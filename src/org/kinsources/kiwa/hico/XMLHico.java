/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.hico;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.Locale;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.util.xml.XMLBadFormatException;
import fr.devinsy.util.xml.XMLReader;
import fr.devinsy.util.xml.XMLTag;
import fr.devinsy.util.xml.XMLTag.TagType;
import fr.devinsy.util.xml.XMLWriter;
import fr.devinsy.util.xml.XMLZipReader;
import fr.devinsy.util.xml.XMLZipWriter;

/**
 * This class represents a Actulog File reader and writer.
 * 
 * @author TIP
 */
public class XMLHico
{
	private static final Logger logger = LoggerFactory.getLogger(XMLHico.class);

	/**
	 * 
	 * @param file
	 * @return
	 */
	public static Hico load(final byte[] source) throws Exception
	{
		Hico result;

		XMLReader in = null;
		try
		{
			//
			in = new XMLZipReader(new ByteArrayInputStream(source));

			//
			in.readXMLHeader();

			//
			result = readHico(in);

		}
		finally
		{
			if (in != null)
			{
				in.close();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param file
	 * @return
	 */
	public static Hico load(final File file) throws Exception
	{
		Hico result;

		XMLReader in = null;
		try
		{
			//
			in = new XMLZipReader(file);

			//
			in.readXMLHeader();

			//
			result = readHico(in);

		}
		finally
		{
			if (in != null)
			{
				in.close();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws Exception
	 */
	public static Article readArticle(final String source) throws XMLStreamException, XMLBadFormatException
	{
		Article result;

		//
		XMLReader in = new XMLReader(new StringReader(source));

		//
		in.readXMLHeader();

		//
		result = readArticle(in);

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static Article readArticle(final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		Article result;

		//
		in.readStartTag("article");

		//
		{
			long id = Long.parseLong(in.readContentTag("id").getContent());

			String name = StringEscapeUtils.unescapeXml(in.readContentTag("name").getContent());

			String author = StringEscapeUtils.unescapeXml(in.readNullableContentTag("author").getContent());

			boolean visibility = Boolean.parseBoolean(in.readContentTag("visible").getContent());

			DateTime creationDate = DateTime.parse(in.readContentTag("creation_date").getContent());

			DateTime editionDate = DateTime.parse(in.readContentTag("edition_date").getContent());

			Locale locale = new Locale(StringEscapeUtils.unescapeXml(in.readContentTag("locale").getContent()));

			String text = StringEscapeUtils.unescapeXml(in.readNullableContentTag("text").getContent());

			//
			result = new Article(id, name, author, visibility, creationDate, editionDate, locale, text);
		}

		//
		in.readEndTag("article");

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static void readArticles(final Articles target, final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{

		//
		XMLTag list = in.readListTag("articles");

		//
		if (list.getType() != TagType.EMPTY)
		{
			while (in.hasNextStartTag("article"))
			{
				//
				Article article = readArticle(in);

				//
				target.add(article);
			}

			//
			in.readEndTag("articles");
		}
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws Exception
	 */
	public static Hico readHico(final String source) throws XMLStreamException, XMLBadFormatException
	{
		Hico result;

		//
		XMLReader in = new XMLReader(new StringReader(source));

		//
		in.readXMLHeader();

		//
		result = readHico(in);

		//
		return result;
	}

	/**
	 * Reads a net from a XMLReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static Hico readHico(final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		Hico result;

		//
		in.readStartTag("hico");

		//
		{
			//
			long lastId = Long.parseLong(in.readContentTag("last_id").getContent());

			//
			result = new Hico(lastId);

			//
			readArticles(result.articles(), in);
		}

		//
		in.readEndTag("hico");

		//
		result.resetLastId();
		result.rebuildIndexes();
		result.resetBubbles();

		//
		return result;
	}

	/**
	 * Saves a net in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 */
	public static void save(final File file, final Hico source, final String generator) throws Exception
	{

		if (file == null)
		{
			throw new IllegalArgumentException("file is null.");
		}
		else if (source == null)
		{
			throw new IllegalArgumentException("source is null.");
		}
		else
		{
			//
			XMLWriter out = null;
			try
			{
				//
				out = new XMLZipWriter(file, generator);

				//
				out.writeXMLHeader((String[]) null);

				//
				write(out, source);

			}
			catch (IOException exception)
			{
				//
				logger.warn(ExceptionUtils.getStackTrace(exception));
				throw new Exception("Error saving file [" + file + "]", exception);

			}
			finally
			{
				//
				if (out != null)
				{
					out.flush();
					out.close();
				}
			}
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String toXMLString(final Article source) throws UnsupportedEncodingException
	{
		String result;

		//
		StringWriter xmlTarget = new StringWriter(1024);

		//
		XMLWriter xmlWriter = new XMLWriter(xmlTarget);

		//
		xmlWriter.writeXMLHeader((String[]) null);

		//
		XMLHico.write(xmlWriter, source);

		//
		result = xmlTarget.toString();

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String toXMLString(final Hico source) throws UnsupportedEncodingException
	{
		String result;

		//
		StringWriter xmlTarget = new StringWriter(1024);

		//
		XMLWriter xmlWriter = new XMLWriter(xmlTarget);

		//
		xmlWriter.writeXMLHeader((String[]) null);

		//
		write(xmlWriter, source);

		//
		result = xmlTarget.toString();

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final Article source)
	{

		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else
		{
			//
			out.writeStartTag("article");

			//
			{
				out.writeTag("id", source.getId());
				out.writeTag("name", StringEscapeUtils.escapeXml(source.getName()));
				out.writeTag("author", StringEscapeUtils.escapeXml(source.getAuthor()));
				out.writeTag("visible", source.isVisible());
				out.writeTag("creation_date", source.getCreationDate().toString());
				out.writeTag("edition_date", source.getEditionDate().toString());
				out.writeTag("locale", source.getLocale().getLanguage());
				out.writeTag("text", StringEscapeUtils.escapeXml(source.getText()));
			}

			//
			out.writeEndTag("article");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final Articles source)
	{

		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if ((source == null) || (source.isEmpty()))
		{
			out.writeEmptyTag("articles");
		}
		else
		{
			//
			out.writeStartTag("articles", "size", String.valueOf(source.size()));

			//
			{
				//
				for (Article log : source)
				{
					write(out, log);
				}
			}

			//
			out.writeEndTag("articles");
		}
	}

	/**
	 * Writes a net in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param source
	 *            Source.
	 */
	public static void write(final XMLWriter out, final Hico source)
	{
		//
		out.writeStartTag("hico");

		//
		{
			//
			out.writeTag("last_id", source.lastArticleId());

			//
			write(out, source.articles());
		}

		//
		out.writeEndTag("hico");
	}
}
