/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.hico;

import java.util.Locale;

import org.joda.time.DateTime;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class Article
{
	private long id;
	private String name;
	private String author;
	private boolean visible;
	private DateTime creationDate;
	private DateTime editionDate;
	private Locale locale;
	private String text;

	/**
	 * 
	 * @param category
	 * @param event
	 * @param comment
	 */
	public Article(final long id, final String name, final Locale locale)
	{
		if (name == null)
		{
			throw new IllegalArgumentException("name is null");
		}
		else if (locale == null)
		{
			throw new IllegalArgumentException("locale is null");
		}
		else
		{
			this.id = id;
			this.name = name;
			this.creationDate = DateTime.now();
			this.editionDate = this.creationDate;
			this.locale = locale;
		}
	}

	/**
	 * 
	 * @param category
	 * @param event
	 * @param comment
	 */
	public Article(final long id, final String name, final String author, final boolean visibility, final DateTime creationDate, final DateTime editionDate, final Locale locale, final String text)
	{
		if (name == null)
		{
			throw new IllegalArgumentException("name is null");
		}
		else if (creationDate == null)
		{
			throw new IllegalArgumentException("creationDate is null");
		}
		else if (editionDate == null)
		{
			throw new IllegalArgumentException("editionDate is null");
		}
		else if (locale == null)
		{
			throw new IllegalArgumentException("locale is null");
		}
		else
		{
			this.id = id;
			this.name = name;
			this.author = author;
			this.visible = visibility;
			this.creationDate = creationDate;
			this.editionDate = editionDate;
			this.locale = locale;
			this.text = text;
		}
	}

	public String getAuthor()
	{
		return this.author;
	}

	public DateTime getCreationDate()
	{
		return this.creationDate;
	}

	public DateTime getEditionDate()
	{
		return this.editionDate;
	}

	public long getId()
	{
		return this.id;
	}

	public Locale getLocale()
	{
		return this.locale;
	}

	public String getName()
	{
		return this.name;
	}

	public String getText()
	{
		return this.text;
	}

	public boolean isVisible()
	{
		return this.visible;
	}

	public void setAuthor(final String author)
	{
		this.author = author;
	}

	public void setCreationDate(final DateTime creationDate)
	{
		this.creationDate = creationDate;
	}

	public void setEditionDate(final DateTime editionDate)
	{
		this.editionDate = editionDate;
	}

	public void setId(final long id)
	{
		this.id = id;
	}

	public void setLocale(final Locale locale)
	{
		this.locale = locale;
	}

	public void setName(final String name)
	{
		if (name == null)
		{
			throw new IllegalArgumentException("name is null.");
		}
		else
		{
			this.name = name;
		}
	}

	public void setText(final String text)
	{
		this.text = text;
	}

	public void setVisible(final boolean visible)
	{
		this.visible = visible;
	}

	/**
	 * 
	 */
	public void touch()
	{
		//
		setEditionDate(DateTime.now());
	}
}
