/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.actilog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import org.joda.time.DateTime;
import org.kinsources.kiwa.actilog.Log.Level;
import org.kinsources.kiwa.actilog.LogComparator.Criteria;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class Logs implements Iterable<Log>
{
	private ArrayList<Log> logs;

	/**
	 * 
	 */
	public Logs()
	{
		this.logs = new ArrayList<Log>();
	}

	/**
	 * 
	 */
	public Logs(final int initialCapacity)
	{
		this.logs = new ArrayList<Log>(initialCapacity);
	}

	/**
	 * 
	 * @param log
	 */
	public void add(final Log log)
	{
		//
		if (log == null)
		{
			throw new IllegalArgumentException("log is null.");
		}
		else if (log.getLevel() == null)
		{
			throw new IllegalArgumentException("level is null.");
		}
		else if (log.getEvent() == null)
		{
			throw new IllegalArgumentException("event is null.");
		}
		else
		{
			this.logs.add(log);
		}
	}

	/**
	 * 
	 */
	public void clear()
	{
		this.logs.clear();
	}

	/**
	 * This methods returns a shallow copy of the current object.
	 * 
	 * @return a shallow copy of the current object.
	 */
	public Logs copy()
	{
		Logs result;

		//
		result = new Logs(this.logs.size());

		//
		for (Log log : this.logs)
		{
			result.add(log);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param level
	 * @return
	 */
	public Logs findAfter(final DateTime date)
	{
		Logs result;

		if (date == null)
		{
			result = copy();
		}
		else
		{
			//
			result = new Logs();

			//
			for (Log log : this.logs)
			{
				if (log.getTime().isAfter(date))
				{
					result.add(log);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param level
	 * @return
	 */
	public Logs findBefore(final DateTime date)
	{
		Logs result;

		if (date == null)
		{
			result = copy();
		}
		else
		{
			//
			result = new Logs();

			//
			for (Log log : this.logs)
			{
				if (log.getTime().isBefore(date))
				{
					result.add(log);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param level
	 * @return
	 */
	public Logs findByDetails(final String pattern)
	{
		Logs result;

		if (pattern == null)
		{
			result = copy();
		}
		else
		{
			//
			result = new Logs();

			//
			for (Log log : this.logs)
			{
				if ((log.getDetails() != null) && (log.getDetails().matches(pattern)))
				{
					result.add(log);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param level
	 * @return
	 */
	public Logs findByEvent(final String pattern)
	{
		Logs result;

		if (pattern == null)
		{
			result = copy();
		}
		else
		{
			//
			result = new Logs();

			//
			for (Log log : this.logs)
			{
				if (log.getEvent().matches(pattern))
				{
					result.add(log);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param level
	 * @return
	 */
	public Logs findByLevel(final Level level)
	{
		Logs result;

		if ((level == null) || (level == Level.ALL))
		{
			result = copy();
		}
		else if (level == Level.OFF)
		{
			result = new Logs();
		}
		else
		{
			//
			result = new Logs();

			//
			for (Log log : this.logs)
			{
				if (log.getLevel().ordinal() >= level.ordinal())
				{
					result.add(log);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetCount
	 * @return
	 */
	public Logs first(final int targetCount)
	{
		Logs result;

		//
		result = new Logs(targetCount);

		//
		boolean ended = false;
		Iterator<Log> iterator = iterator();
		int count = 0;
		while (!ended)
		{
			if ((count > targetCount) || (!iterator.hasNext()))
			{
				ended = true;
			}
			else
			{
				result.add(iterator.next());
				count += 1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param index
	 * @return
	 */
	public Log getByIndex(final int index)
	{
		Log result;

		result = this.logs.get(index);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty()
	{
		boolean result;

		result = this.logs.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty()
	{
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Iterator<Log> iterator()
	{
		Iterator<Log> result;

		result = this.logs.iterator();

		//
		return result;
	}

	/**
	 * 
	 * @param lastCount
	 * @return
	 */
	public Logs lastest(final int targetCount)
	{
		Logs result;

		//
		result = copy().sortByDate().reverse().first(targetCount);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	synchronized public long lastId()
	{
		long result;

		//
		result = 0;
		for (Log log : this.logs)
		{
			if (log.getId() > result)
			{
				result = log.getId();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Logs reverse()
	{
		Logs result;

		//
		Collections.reverse(this.logs);

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int size()
	{
		int result;

		result = this.logs.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Logs sortByDate()
	{
		Logs result;

		//
		Collections.sort(this.logs, new LogComparator(Criteria.DATETIME));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Logs sortById()
	{
		Logs result;

		//
		Collections.sort(this.logs, new LogComparator(Criteria.ID));

		//
		result = this;

		//
		return result;
	}
}
