/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.actilog;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.joda.time.DateTime;
import org.kinsources.kiwa.actilog.Log.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.util.xml.XMLBadFormatException;
import fr.devinsy.util.xml.XMLReader;
import fr.devinsy.util.xml.XMLTag;
import fr.devinsy.util.xml.XMLTag.TagType;
import fr.devinsy.util.xml.XMLWriter;
import fr.devinsy.util.xml.XMLZipReader;
import fr.devinsy.util.xml.XMLZipWriter;

/**
 * This class represents a Actilog File reader and writer.
 * 
 * @author TIP
 */
public class XMLActilog
{
	private static final Logger logger = LoggerFactory.getLogger(XMLActilog.class);

	/**
	 * 
	 * @param file
	 * @return
	 */
	public static Actilog load(final File file) throws Exception
	{
		Actilog result;

		XMLReader in = null;
		try
		{
			//
			in = new XMLZipReader(file);

			//
			in.readXMLHeader();

			//
			result = readActilog(in);
		}
		finally
		{
			if (in != null)
			{
				in.close();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws Exception
	 */
	public static Actilog readActilog(final String source) throws XMLStreamException, XMLBadFormatException
	{
		Actilog result;

		//
		XMLReader in = new XMLReader(new StringReader(source));

		//
		in.readXMLHeader();

		//
		result = readActilog(in);

		//
		return result;
	}

	/**
	 * Reads a net from a XMLReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static Actilog readActilog(final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		Actilog result;

		//
		result = new Actilog();

		//
		in.readStartTag("actilog");

		//
		{
			//
			result.setLogLevel(Level.valueOf(in.readContentTag("log_level").getContent()));

			//
			readLogs(result.logs(), in);
		}

		//
		in.readEndTag("actilog");

		//
		result.resetLastId();

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws Exception
	 */
	public static Log readLog(final String source) throws XMLStreamException, XMLBadFormatException
	{
		Log result;

		//
		XMLReader in = new XMLReader(new StringReader(source));

		//
		in.readXMLHeader();

		//
		result = readLog(in);

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static Log readLog(final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		Log result;

		//
		in.readStartTag("log");

		//
		{
			//
			long id = Long.parseLong(in.readContentTag("id").getContent());
			DateTime time = DateTime.parse(in.readContentTag("time").getContent());
			Level level = Level.valueOf(in.readContentTag("level").getContent());
			String event = StringEscapeUtils.unescapeXml(in.readContentTag("event").getContent());
			String details = StringEscapeUtils.unescapeXml(in.readNullableContentTag("details").getContent());

			//
			result = new Log(id, time, level, event, details);
		}

		//
		in.readEndTag("log");

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static void readLogs(final Logs target, final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{

		//
		XMLTag list = in.readListTag("logs");

		//
		if (list.getType() != TagType.EMPTY)
		{

			while (in.hasNextStartTag("log"))
			{

				//
				Log log = readLog(in);

				//
				target.add(log);
			}

			//
			in.readEndTag("logs");
		}
	}

	/**
	 * Saves a net in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 */
	public static void save(final File file, final Actilog source, final String generator) throws Exception
	{

		if (file == null)
		{
			throw new IllegalArgumentException("file is null.");
		}
		else if (source == null)
		{
			throw new IllegalArgumentException("source is null.");
		}
		else
		{
			XMLWriter out = null;
			try
			{
				//
				out = new XMLZipWriter(file, generator);

				//
				out.writeXMLHeader((String[]) null);

				//
				write(out, source);
			}
			catch (IOException exception)
			{
				logger.warn(ExceptionUtils.getStackTrace(exception));
				throw new Exception("Error saving file [" + file + "]", exception);
			}
			finally
			{
				if (out != null)
				{
					out.flush();
					out.close();
				}
			}
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String toXMLString(final Actilog source) throws UnsupportedEncodingException
	{
		String result;

		//
		StringWriter xmlTarget = new StringWriter(1024);

		//
		XMLWriter xmlWriter = new XMLWriter(xmlTarget);

		//
		xmlWriter.writeXMLHeader((String[]) null);

		//
		XMLActilog.write(xmlWriter, source);

		//
		result = xmlTarget.toString();

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String toXMLString(final Log source) throws UnsupportedEncodingException
	{
		String result;

		//
		StringWriter xmlTarget = new StringWriter(1024);

		//
		XMLWriter xmlWriter = new XMLWriter(xmlTarget);

		//
		xmlWriter.writeXMLHeader((String[]) null);

		//
		XMLActilog.write(xmlWriter, source);

		//
		result = xmlTarget.toString();

		//
		return result;
	}

	/**
	 * Writes a net in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param source
	 *            Source.
	 */
	public static void write(final XMLWriter out, final Actilog source)
	{

		//
		out.writeStartTag("actilog");

		//
		out.writeTag("log_level", source.getLogLevel().toString());

		//
		write(out, source.logs());

		//
		out.writeEndTag("actilog");
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final Log source)
	{

		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else
		{
			//
			out.writeStartTag("log");

			//
			{
				out.writeTag("id", source.getId());
				out.writeTag("time", source.getTime().toString());
				out.writeTag("level", source.getLevel().toString());
				out.writeTag("event", StringEscapeUtils.escapeXml(source.getEvent()));
				String details;
				if (source.getDetails() == null)
				{
					details = null;
				}
				else
				{
					details = StringEscapeUtils.escapeXml(source.getDetails());
				}
				out.writeTag("details", details);
			}

			//
			out.writeEndTag("log");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final Logs source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if ((source == null) || (source.isEmpty()))
		{
			out.writeEmptyTag("logs");
		}
		else
		{
			//
			out.writeStartTag("logs", "size", String.valueOf(source.size()));

			//
			{
				//
				for (Log log : source)
				{
					write(out, log);
				}
			}

			//
			out.writeEndTag("logs");
		}
	}
}
