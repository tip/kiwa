/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.kernel;

/**
 * Kiwa export action offers to export some or all data.
 * 
 * @author Christian P. Momon
 */
public class KiwaExportCriteria
{
	private boolean accounts;
	private boolean actalog;
	private boolean actilog;
	private boolean actulog;
	private boolean agora;
	private boolean hico;
	private boolean kidarep;
	private boolean sciboard;

	public boolean isAccounts()
	{
		return this.accounts;
	}

	public boolean isActalog()
	{
		return this.actalog;
	}

	public boolean isActilog()
	{
		return this.actilog;
	}

	public boolean isActulog()
	{
		return this.actulog;
	}

	public boolean isAgora()
	{
		return this.agora;
	}

	public boolean isHico()
	{
		return this.hico;
	}

	public boolean isKidarep()
	{
		return this.kidarep;
	}

	public boolean isSciboard()
	{
		return this.sciboard;
	}

	public void setAccounts(final boolean accounts)
	{
		this.accounts = accounts;
	}

	public void setActalog(final boolean actalog)
	{
		this.actalog = actalog;
	}

	public void setActilog(final boolean actilog)
	{
		this.actilog = actilog;
	}

	public void setActulog(final boolean actulog)
	{
		this.actulog = actulog;
	}

	public void setAgora(final boolean agora)
	{
		this.agora = agora;
	}

	/**
	 * 
	 * @param value
	 */
	public void setAll(final boolean value)
	{
		this.accounts = value;
		this.actalog = value;
		this.actilog = value;
		this.actulog = value;
		this.hico = value;
		this.kidarep = value;
	}

	public void setHico(final boolean hico)
	{
		this.hico = hico;
	}

	public void setKidarep(final boolean kidarep)
	{
		this.kidarep = kidarep;
	}

	public void setSciboard(final boolean sciboard)
	{
		this.sciboard = sciboard;
	}
}
