/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.actalog;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.joda.time.DateTime;
import org.kinsources.kiwa.actalog.EventLog.Category;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.util.StringList;
import fr.devinsy.util.xml.XMLBadFormatException;
import fr.devinsy.util.xml.XMLReader;
import fr.devinsy.util.xml.XMLTag;
import fr.devinsy.util.xml.XMLTag.TagType;
import fr.devinsy.util.xml.XMLWriter;
import fr.devinsy.util.xml.XMLZipReader;
import fr.devinsy.util.xml.XMLZipWriter;

/**
 * This class represents a Actalog File reader and writer.
 * 
 * @author TIP
 */
public class XMLActalog
{
	private static final Logger logger = LoggerFactory.getLogger(XMLActalog.class);

	private static final String ARGUMENT_SEPARATOR = "£";

	/**
	 * 
	 * @param file
	 * @return
	 */
	public static Actalog load(final File file) throws Exception
	{
		Actalog result;

		XMLReader in = null;
		try
		{
			//
			in = new XMLZipReader(file);

			//
			in.readXMLHeader();

			//
			result = readActalog(in);
		}
		finally
		{
			if (in != null)
			{
				in.close();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws Exception
	 */
	public static Actalog readActalog(final String source) throws XMLStreamException, XMLBadFormatException
	{
		Actalog result;

		//
		XMLReader in = new XMLReader(new StringReader(source));

		//
		in.readXMLHeader();

		//
		result = readActalog(in);

		//
		return result;
	}

	/**
	 * Reads a net from a XMLReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static Actalog readActalog(final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		Actalog result;

		//
		in.readStartTag("actalog");

		//
		{
			//
			long lastId = Long.parseLong(in.readContentTag("last_id").getContent());

			//
			result = new Actalog(lastId);

			//
			readEventLogs(result.eventLogs(), in);
		}

		//
		in.readEndTag("actalog");

		//
		result.resetLastId();

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws Exception
	 */
	public static EventLog readEventLog(final String source) throws XMLStreamException, XMLBadFormatException
	{
		EventLog result;

		//
		XMLReader in = new XMLReader(new StringReader(source));

		//
		in.readXMLHeader();

		//
		result = readEventLog(in);

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static EventLog readEventLog(final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		EventLog result;

		//
		in.readStartTag("event_log");

		//
		{
			//
			long id = Long.parseLong(in.readContentTag("id").getContent());

			DateTime time = DateTime.parse(in.readContentTag("time").getContent());

			Category category = Category.valueOf(StringEscapeUtils.unescapeXml(in.readContentTag("category").getContent()));

			String argumentValue = in.readNullableContentTag("arguments").getContent();
			String[] details;
			if (argumentValue == null)
			{
				details = new String[0];
			}
			else
			{
				details = StringEscapeUtils.unescapeXml(argumentValue).split(ARGUMENT_SEPARATOR);
			}

			//
			result = new EventLog(id, time, category, details);
		}

		//
		in.readEndTag("event_log");

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static void readEventLogs(final EventLogs target, final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		//
		XMLTag list = in.readListTag("event_logs");

		//
		if (list.getType() != TagType.EMPTY)
		{

			while (in.hasNextStartTag("event_log"))
			{
				//
				EventLog event = readEventLog(in);

				//
				target.add(event);
			}

			//
			in.readEndTag("event_logs");
		}
	}

	/**
	 * Saves a net in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 */
	public static void save(final File file, final Actalog source, final String generator) throws Exception
	{

		if (file == null)
		{
			throw new IllegalArgumentException("file is null.");
		}
		else if (source == null)
		{
			throw new IllegalArgumentException("source is null.");
		}
		else
		{
			XMLWriter out = null;
			try
			{
				//
				out = new XMLZipWriter(file, generator);

				//
				out.writeXMLHeader((String[]) null);

				//
				write(out, source);

			}
			catch (IOException exception)
			{
				logger.warn(ExceptionUtils.getStackTrace(exception));
				throw new Exception("Error saving file [" + file + "]", exception);
			}
			finally
			{
				if (out != null)
				{
					out.flush();
					out.close();
				}
			}
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	static public String toXMLString(final Actalog source) throws UnsupportedEncodingException
	{
		String result;

		//
		StringWriter xmlTarget = new StringWriter(1024);

		//
		XMLWriter xmlWriter = new XMLWriter(xmlTarget);

		//
		xmlWriter.writeXMLHeader((String[]) null);

		//
		write(xmlWriter, source);

		//
		result = xmlTarget.toString();

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	static public String toXMLString(final EventLog source) throws UnsupportedEncodingException
	{
		String result;

		//
		StringWriter xmlTarget = new StringWriter(1024);

		//
		XMLWriter xmlWriter = new XMLWriter(xmlTarget);

		//
		xmlWriter.writeXMLHeader((String[]) null);

		//
		XMLActalog.write(xmlWriter, source);

		//
		result = xmlTarget.toString();

		//
		return result;
	}

	/**
	 * Writes a net in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param source
	 *            Source.
	 */
	public static void write(final XMLWriter out, final Actalog source)
	{
		//
		out.writeStartTag("actalog");

		//
		out.writeTag("last_id", source.lastId());

		//
		write(out, source.eventLogs());

		//
		out.writeEndTag("actalog");
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	static public void write(final XMLWriter out, final EventLog source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else
		{
			//
			out.writeStartTag("event_log");

			//
			{
				//
				out.writeTag("id", source.getId());
				out.writeTag("time", source.getTime().toString());
				out.writeTag("category", source.getCategory().toString());

				//
				String arguments;
				if (source.getArguments() == null)
				{
					arguments = null;
				}
				else
				{
					StringList buffer = new StringList(source.getArguments().length);
					for (String string : source.getArguments())
					{
						buffer.append(string).append(ARGUMENT_SEPARATOR);
					}
					buffer.removeLast();
					arguments = StringEscapeUtils.escapeXml(buffer.toString());
				}
				out.writeTag("arguments", arguments);
			}

			//
			out.writeEndTag("event_log");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final EventLogs source)
	{

		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if ((source == null) || (source.isEmpty()))
		{
			out.writeEmptyTag("event_logs");
		}
		else
		{
			//
			out.writeStartTag("event_logs", "size", String.valueOf(source.size()));

			//
			{
				for (EventLog event : source)
				{
					write(out, event);
				}
			}

			//
			out.writeEndTag("event_logs");
		}
	}
}
