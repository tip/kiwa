/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.kidarep;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;

/**
 * The <code>LoadedFiles</code> class represents an account collection.
 * 
 * @author christian.momon@devinsy.fr
 */
public class LoadedFiles implements Iterable<LoadedFile>
{
	private List<LoadedFile> loaddedFiles;

	/**
	 * 
	 */
	public LoadedFiles()
	{
		super();

		this.loaddedFiles = new ArrayList<LoadedFile>();
	}

	/**
	 * 
	 */
	public LoadedFiles(final int initialCapacity)
	{
		this.loaddedFiles = new ArrayList<LoadedFile>(initialCapacity);
	}

	/**
	 * 
	 */
	public LoadedFile add(final int index, final LoadedFile source)
	{
		LoadedFile result;

		//
		if (source != null)
		{
			this.loaddedFiles.add(index, source);
		}

		//
		result = source;

		//
		return result;
	}

	/**
	 * 
	 */
	public LoadedFile add(final LoadedFile source)
	{
		LoadedFile result;

		//
		if (source != null)
		{
			this.loaddedFiles.add(source);
		}

		//
		result = source;

		//
		return result;
	}

	/**
	 * 
	 */
	public void addAll(final LoadedFiles source)
	{
		//
		if (source != null)
		{
			for (LoadedFile file : source)
			{
				this.loaddedFiles.add(file);
			}
		}
	}

	/**
	 * 
	 */
	public void clear()
	{
		this.loaddedFiles.clear();
	}

	/**
	 * This methods returns a shallow copy of the current object.
	 * 
	 * @return a shallow copy of the current object.
	 */
	public LoadedFiles copy()
	{
		LoadedFiles result;

		//
		result = new LoadedFiles(this.loaddedFiles.size());

		//
		for (LoadedFile account : this.loaddedFiles)
		{
			result.add(account);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfLoadedFiles()
	{
		long result;

		result = this.loaddedFiles.size();

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public LoadedFile getByDigest(final String digest)
	{
		LoadedFile result;

		boolean ended = false;
		Iterator<LoadedFile> iterator = this.loaddedFiles.iterator();
		result = null;
		while (!ended)
		{
			if (iterator.hasNext())
			{
				//
				LoadedFile file = iterator.next();

				//
				if (StringUtils.equals(file.header().digest(), digest))
				{
					ended = true;
					result = file;
				}
			}
			else
			{
				ended = true;
				result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public LoadedFile getById(final long id)
	{
		LoadedFile result;

		boolean ended = false;
		Iterator<LoadedFile> iterator = this.loaddedFiles.iterator();
		result = null;
		while (!ended)
		{
			if (iterator.hasNext())
			{
				//
				LoadedFile file = iterator.next();

				//
				if (file.header().id() == id)
				{
					ended = true;
					result = file;
				}
			}
			else
			{
				ended = true;
				result = null;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public LoadedFile getById(final String id)
	{
		LoadedFile result;

		if (NumberUtils.isNumber(id))
		{
			result = getById(Long.parseLong(id));
		}
		else
		{
			result = null;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public LoadedFile getByIndex(final int index)
	{
		LoadedFile result;

		result = this.loaddedFiles.get(index);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty()
	{
		boolean result;

		result = this.loaddedFiles.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty()
	{
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public Iterator<LoadedFile> iterator()
	{
		Iterator<LoadedFile> result;

		result = this.loaddedFiles.iterator();

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 */
	synchronized public void remove(final LoadedFile file)
	{
		this.loaddedFiles.remove(file);
	}

	/**
	 * 
	 * @return
	 */
	public LoadedFiles reverse()
	{
		LoadedFiles result;

		//
		java.util.Collections.reverse(this.loaddedFiles);

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int size()
	{
		int result;

		result = this.loaddedFiles.size();

		//
		return result;
	}
}
