/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.kidarep;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.kinsources.kiwa.kidarep.CollaborationComparator.Criteria;

/**
 * The <code>Collaborators</code> class represents a dataset collaborator.
 * 
 * @author christian.momon@devinsy.fr
 */
public class Collaborations implements Iterable<Collaboration>
{
	private List<Collaboration> collaborations;

	/**
	 * 
	 */
	public Collaborations()
	{
		super();

		this.collaborations = new ArrayList<Collaboration>();
	}

	/**
	 * 
	 */
	public Collaborations(final int initialCapacity)
	{
		this.collaborations = new ArrayList<Collaboration>(initialCapacity);
	}

	/**
	 * 
	 */
	public Collaboration add(final Collaboration source)
	{
		Collaboration result;

		//
		if (source == null)
		{
			throw new IllegalArgumentException("source is null");
		}
		else
		{
			this.collaborations.add(source);
		}

		//
		result = source;

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 */
	public void addAll(final Collaborations source)
	{
		if (source != null)
		{
			for (Collaboration collaboration : source)
			{
				if (!this.collaborations.contains(collaboration))
				{
					add(collaboration);
				}
			}
		}
	}

	/**
	 * 
	 */
	public void clear()
	{
		this.collaborations.clear();
	}

	/**
	 * This methods returns a shallow copy of the current object.
	 * 
	 * @return a shallow copy of the current object.
	 */
	public Collaborations copy()
	{
		Collaborations result;

		result = new Collaborations(this.collaborations.size());

		for (Collaboration collaborator : this.collaborations)
		{
			result.add(collaborator);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Collaboration getByIndex(final int index)
	{
		Collaboration result;

		result = this.collaborations.get(index);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty()
	{
		boolean result;

		result = this.collaborations.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty()
	{
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public Iterator<Collaboration> iterator()
	{
		Iterator<Collaboration> result;

		result = this.collaborations.iterator();

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 */
	synchronized public void remove(final Collaboration collaboration)
	{
		this.collaborations.remove(collaboration);
	}

	/**
	 * 
	 * @return
	 */
	public Collaborations reverse()
	{
		Collaborations result;

		//
		java.util.Collections.reverse(this.collaborations);

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int size()
	{
		int result;

		result = this.collaborations.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Collaborations sortByCollaboratorId()
	{
		Collaborations result;

		//
		java.util.Collections.sort(this.collaborations, new CollaborationComparator(Criteria.COLLABORATOR_ID));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Collaborations sortByCollaboratorName()
	{
		Collaborations result;

		//
		java.util.Collections.sort(this.collaborations, new CollaborationComparator(Criteria.COLLABORATOR_NAME));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Collaborations sortByCreationDate()
	{
		Collaborations result;

		//
		java.util.Collections.sort(this.collaborations, new CollaborationComparator(Criteria.CREATION_DATE));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Collaborations sortByDatasetId()
	{
		Collaborations result;

		//
		java.util.Collections.sort(this.collaborations, new CollaborationComparator(Criteria.DATASET_ID));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Collaborations sortByDatasetName()
	{
		Collaborations result;

		//
		java.util.Collections.sort(this.collaborations, new CollaborationComparator(Criteria.DATASET_NAME));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Collaborations sortByEditionDate()
	{
		Collaborations result;

		//
		java.util.Collections.sort(this.collaborations, new CollaborationComparator(Criteria.EDITION_DATE));

		//
		result = this;

		//
		return result;
	}
}
