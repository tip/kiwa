/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.kidarep;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kidarep.ContributorComparator.Criteria;

/**
 * The <code>Contributors</code> class represents an account collection.
 * 
 * @author christian.momon@devinsy.fr
 */
public class Contributors implements Iterable<Contributor>
{
	private List<Contributor> contributors;

	/**
	 * 
	 */
	public Contributors()
	{
		super();

		this.contributors = new ArrayList<Contributor>();
	}

	/**
	 * 
	 */
	public Contributors(final int initialCapacity)
	{
		this.contributors = new ArrayList<Contributor>(initialCapacity);
	}

	/**
	 * 
	 */
	public Contributor add(final Contributor source)
	{
		Contributor result;

		//
		if (source == null)
		{
			throw new IllegalArgumentException("source is null");
		}
		else
		{
			this.contributors.add(source);
		}

		//
		result = source;

		//
		return result;
	}

	/**
	 * 
	 */
	public void clear()
	{
		this.contributors.clear();
	}

	/**
	 * 
	 * @return
	 */
	public Collections collections()
	{
		Collections result;

		//
		result = new Collections((int) countOfCollections());

		//
		for (Contributor contributor : this.contributors)
		{
			result.addAll(contributor.collections());
		}

		//
		return result;
	}

	/**
	 * This methods returns a shallow copy of the current object.
	 * 
	 * @return a shallow copy of the current object.
	 */
	public Contributors copy()
	{
		Contributors result;

		//
		result = new Contributors(this.contributors.size());

		//
		for (Contributor account : this.contributors)
		{
			result.add(account);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfAttachmentFiles()
	{
		long result;

		result = 0;
		for (Contributor contributor : this.contributors)
		{
			result += contributor.countOfAttachmentFiles();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfCollections()
	{
		long result;

		result = 0;
		for (Contributor contributor : this.contributors)
		{
			result += contributor.countOfCollections();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfContributors()
	{
		long result;

		result = this.contributors.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfDatasetFiles()
	{
		long result;

		result = 0;
		for (Contributor contributor : this.contributors)
		{
			result += contributor.countOfDatasetFiles();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfDatasets()
	{
		long result;

		result = 0;
		for (Contributor contributor : this.contributors)
		{
			result += contributor.countOfDatasets();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfFiles()
	{
		long result;

		result = 0;
		for (Contributor contributor : this.contributors)
		{
			result += contributor.countOfFiles();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfPublishedDatasetFiles()
	{
		long result;

		result = 0;
		for (Contributor contributor : this)
		{
			result += contributor.countOfPublishedDatasetFiles();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfPublishedDatasets()
	{
		long result;

		result = 0;
		for (Contributor contributor : this)
		{
			result += contributor.countOfPublishedDatasets();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Datasets datasets()
	{
		Datasets result;

		//
		result = new Datasets((int) countOfDatasets());

		//
		for (Contributor contributor : this.contributors)
		{
			result.addAll(contributor.personalDatasets());
		}

		//
		return result;
	}

	/**
	 * 
	 * @param status
	 * @return
	 */
	public Contributors findContributors(final Account.Status status)
	{
		Contributors result;

		result = new Contributors();

		if (status != null)
		{
			for (Contributor contributor : this.contributors)
			{
				if (contributor.account().getStatus() == status)
				{
					result.add(contributor);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Contributor getByIndex(final int index)
	{
		Contributor result;

		result = this.contributors.get(index);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty()
	{
		boolean result;

		result = this.contributors.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty()
	{
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	@Override
	public Iterator<Contributor> iterator()
	{
		Iterator<Contributor> result;

		result = this.contributors.iterator();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	synchronized public long lastCollectionId()
	{
		long result;

		//
		result = 0;
		for (Contributor contributor : this.contributors)
		{
			long current = contributor.collections().lastId();
			if (current > result)
			{
				result = current;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	synchronized public long lastId()
	{
		long result;

		//
		result = 0;
		for (Contributor contributor : this.contributors)
		{
			if (contributor.getId() > result)
			{
				result = contributor.getId();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	synchronized public long lastPersonalDatasetId()
	{
		long result;

		//
		result = 0;
		for (Contributor contributor : this.contributors)
		{
			long current = contributor.lastPersonalDatasetId();
			if (current > result)
			{
				result = current;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 */
	synchronized public void remove(final Contributor account)
	{
		this.contributors.remove(account);
	}

	/**
	 * 
	 * @return
	 */
	public Contributors reverse()
	{
		Contributors result;

		//
		java.util.Collections.reverse(this.contributors);

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int size()
	{
		int result;

		result = this.contributors.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Contributors sortByFirstNames()
	{
		Contributors result;

		//
		java.util.Collections.sort(this.contributors, new ContributorComparator(Criteria.FIRST_NAMES));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Contributors sortByFullName()
	{
		Contributors result;

		//
		java.util.Collections.sort(this.contributors, new ContributorComparator(Criteria.FULLNAME));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Contributors sortById()
	{
		Contributors result;

		//
		java.util.Collections.sort(this.contributors, new ContributorComparator(Criteria.ID));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Contributors sortByLastName()
	{
		Contributors result;

		//
		java.util.Collections.sort(this.contributors, new ContributorComparator(Criteria.LAST_NAME));

		//
		result = this;

		//
		return result;
	}
}
