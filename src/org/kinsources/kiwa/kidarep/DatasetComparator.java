/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.kidarep;

import java.util.Comparator;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.kinsources.kiwa.stag.StatisticTag;
import org.kinsources.kiwa.utils.KiwaUtils;

/**
 * 
 * @author TIP
 */
public class DatasetComparator implements Comparator<Dataset>
{
	public enum Criteria
	{
		ATLAS_CODE,
		AUTHOR,
		CODER,
		CONTINENT,
		CONTRIBUTOR,
		COUNTRY,
		DATASET_NAME,
		EDITION_DATE,
		ID,
		LOCATION,
		NAME,
		ORGANIZATION,
		PUBLICATION_DATE,
		REGION,
		STATISTIC,
		SUBMISSION_DATE
	}

	private Criteria criteria;
	private StatisticTag statistic;

	/**
	 * 
	 * @param criteria
	 */
	public DatasetComparator(final Criteria criteria)
	{
		//
		this.criteria = criteria;
		this.statistic = null;
	}

	/**
	 * 
	 * @param criteria
	 */
	public DatasetComparator(final String criteria)
	{
		this.criteria = EnumUtils.getEnum(Criteria.class, StringUtils.upperCase(criteria));

		if (this.criteria == null)
		{
			this.statistic = EnumUtils.getEnum(StatisticTag.class, StringUtils.upperCase(criteria));

			if (this.statistic == null)
			{
				throw new IllegalArgumentException("Invalid criteria [" + criteria + "]");
			}
			else
			{
				this.criteria = Criteria.STATISTIC;
			}
		}
	}

	/**
	 * 
	 */
	@Override
	public int compare(final Dataset alpha, final Dataset bravo)
	{
		int result;

		result = compare(alpha, bravo, this.criteria, this.statistic);

		//
		return result;
	}

	/**
	 * 
	 */
	public static int compare(final Dataset alpha, final Dataset bravo, final Criteria criteria, final StatisticTag statistic)
	{
		int result;

		switch (criteria)
		{
			case ATLAS_CODE:
			{
				//
				String alphaValue;
				if (alpha == null)
				{
					alphaValue = null;
				}
				else
				{
					alphaValue = alpha.getAtlasCode();
				}

				//
				String bravoValue;
				if (bravo == null)
				{
					bravoValue = null;
				}
				else
				{
					bravoValue = bravo.getAtlasCode();
				}

				//
				result = KiwaUtils.compare(alphaValue, bravoValue);
			}
			break;

			case AUTHOR:
			{
				//
				String alphaValue;
				if (alpha == null)
				{
					alphaValue = null;
				}
				else
				{
					alphaValue = alpha.getAuthor();
				}

				//
				String bravoValue;
				if (bravo == null)
				{
					bravoValue = null;
				}
				else
				{
					bravoValue = bravo.getAuthor();
				}

				//
				result = KiwaUtils.compare(alphaValue, bravoValue);
			}
			break;

			case CODER:
			{
				//
				String alphaValue;
				if (alpha == null)
				{
					alphaValue = null;
				}
				else
				{
					alphaValue = alpha.getCoder();
				}

				//
				String bravoValue;
				if (bravo == null)
				{
					bravoValue = null;
				}
				else
				{
					bravoValue = bravo.getCoder();
				}

				//
				result = KiwaUtils.compare(alphaValue, bravoValue);
			}
			break;

			case CONTINENT:
			{
				//
				String alphaValue;
				if (alpha == null)
				{
					alphaValue = null;
				}
				else
				{
					alphaValue = alpha.getContinent();
				}

				//
				String bravoValue;
				if (bravo == null)
				{
					bravoValue = null;
				}
				else
				{
					bravoValue = bravo.getContinent();
				}

				//
				result = KiwaUtils.compare(alphaValue, bravoValue);
			}
			break;

			case CONTRIBUTOR:
			{
				//
				String alphaValue;
				if (alpha == null)
				{
					alphaValue = null;
				}
				else
				{
					alphaValue = alpha.getContributor().getFullName();
				}

				//
				String bravoValue;
				if (bravo == null)
				{
					bravoValue = null;
				}
				else
				{
					bravoValue = bravo.getContributor().getFullName();
				}

				//
				result = KiwaUtils.compare(alphaValue, bravoValue);
			}
			break;

			case COUNTRY:
			{
				//
				String alphaValue;
				if (alpha == null)
				{
					alphaValue = null;
				}
				else
				{
					alphaValue = alpha.getCountry();
				}

				//
				String bravoValue;
				if (bravo == null)
				{
					bravoValue = null;
				}
				else
				{
					bravoValue = bravo.getCountry();
				}

				//
				result = KiwaUtils.compare(alphaValue, bravoValue);
			}
			break;

			case EDITION_DATE:
			{
				DateTime alphaValue;
				if (alpha == null)
				{
					alphaValue = null;
				}
				else
				{
					alphaValue = alpha.getEditionDate();
				}

				//
				DateTime bravoValue;
				if (bravo == null)
				{
					bravoValue = null;
				}
				else
				{
					bravoValue = bravo.getEditionDate();
				}

				//
				result = KiwaUtils.compare(alphaValue, bravoValue);
			}
			break;

			case ID:
			{
				//
				Long alphaValue;
				if (alpha == null)
				{
					alphaValue = null;
				}
				else
				{
					alphaValue = alpha.getId();
				}

				//
				Long bravoValue;
				if (bravo == null)
				{
					bravoValue = null;
				}
				else
				{
					bravoValue = bravo.getId();
				}

				//
				result = KiwaUtils.compare(alphaValue, bravoValue);
			}
			break;

			case LOCATION:
			{
				//
				String alphaValue;
				if (alpha == null)
				{
					alphaValue = null;
				}
				else
				{
					alphaValue = alpha.getLocation();
				}

				//
				String bravoValue;
				if (bravo == null)
				{
					bravoValue = null;
				}
				else
				{
					bravoValue = bravo.getLocation();
				}

				//
				result = KiwaUtils.compare(alphaValue, bravoValue);
			}
			break;

			case DATASET_NAME:
			case NAME:
			{
				//
				String alphaValue;
				if (alpha == null)
				{
					alphaValue = null;
				}
				else
				{
					alphaValue = alpha.getName();
				}

				//
				String bravoValue;
				if (bravo == null)
				{
					bravoValue = null;
				}
				else
				{
					bravoValue = bravo.getName();
				}

				//
				result = KiwaUtils.compare(alphaValue, bravoValue);
			}
			break;

			case ORGANIZATION:
			{
				//
				String alphaValue;
				if (alpha == null)
				{
					alphaValue = null;
				}
				else
				{
					alphaValue = alpha.getContributor().account().getOrganization();
				}

				//
				String bravoValue;
				if (bravo == null)
				{
					bravoValue = null;
				}
				else
				{
					bravoValue = bravo.getContributor().account().getOrganization();
				}

				//
				result = KiwaUtils.compare(alphaValue, bravoValue);
			}
			break;

			case PUBLICATION_DATE:
			{
				DateTime alphaValue;
				if (alpha == null)
				{
					alphaValue = null;
				}
				else
				{
					alphaValue = alpha.getPublicationDate();
				}

				//
				DateTime bravoValue;
				if (bravo == null)
				{
					bravoValue = null;
				}
				else
				{
					bravoValue = bravo.getPublicationDate();
				}

				//
				result = KiwaUtils.compare(alphaValue, bravoValue);
			}
			break;

			case REGION:
			{
				//
				String alphaValue;
				if (alpha == null)
				{
					alphaValue = null;
				}
				else
				{
					alphaValue = alpha.getRegion();
				}

				//
				String bravoValue;
				if (bravo == null)
				{
					bravoValue = null;
				}
				else
				{
					bravoValue = bravo.getRegion();
				}

				//
				result = KiwaUtils.compare(alphaValue, bravoValue);
			}
			break;

			case STATISTIC:
			{
				//
				Object alphaValue;
				if (alpha == null)
				{
					alphaValue = null;
				}
				else if (alpha.getOriginFile() == null)
				{
					alphaValue = null;
				}
				else
				{
					alphaValue = alpha.getOriginFile().statistics().getValue(statistic.name());
				}

				//
				Object bravoValue;
				if (bravo == null)
				{
					bravoValue = null;
				}
				else if (bravo.getOriginFile() == null)
				{
					bravoValue = null;
				}
				else
				{
					bravoValue = bravo.getOriginFile().statistics().getValue(statistic.name());
				}

				//
				result = compareObjects(alphaValue, bravoValue);
			}
			break;

			case SUBMISSION_DATE:
			{
				DateTime alphaValue;
				if (alpha == null)
				{
					alphaValue = null;
				}
				else
				{
					alphaValue = alpha.getSubmissionDate();
				}

				//
				DateTime bravoValue;
				if (bravo == null)
				{
					bravoValue = null;
				}
				else
				{
					bravoValue = bravo.getSubmissionDate();
				}

				//
				result = KiwaUtils.compare(alphaValue, bravoValue);
			}
			break;

			default:
				throw new IllegalStateException("Unknown criteria [" + criteria + "]");
		}

		//
		return result;
	}

	/**
	 * 
	 * @param alpha
	 * @param bravo
	 * @return
	 */
	public static int compareObjects(final Object alpha, final Object bravo)
	{
		int result;

		if ((alpha instanceof Double) || (bravo instanceof Double))
		{
			result = KiwaUtils.compare((Double) alpha, (Double) bravo);
		}
		else if ((alpha instanceof Integer) || (bravo instanceof Integer))
		{
			result = KiwaUtils.compare((Integer) alpha, (Integer) bravo);
		}
		else if ((alpha instanceof Long) || (bravo instanceof Long))
		{
			result = KiwaUtils.compare((Long) alpha, (Long) bravo);
		}
		else
		{
			//
			String alphaValue;
			if (alpha == null)
			{
				alphaValue = null;
			}
			else
			{
				alphaValue = alpha.toString();
			}

			//
			String bravoValue;
			if (bravo == null)
			{
				bravoValue = null;
			}
			else
			{
				bravoValue = bravo.toString();
			}

			//
			result = KiwaUtils.compare(alphaValue, bravoValue);
		}

		//
		return result;
	}
}
