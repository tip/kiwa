/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.sciboard;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.xml.stream.XMLStreamException;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.devinsy.util.xml.XMLBadFormatException;
import fr.devinsy.util.xml.XMLReader;
import fr.devinsy.util.xml.XMLTag;
import fr.devinsy.util.xml.XMLTag.TagType;
import fr.devinsy.util.xml.XMLWriter;
import fr.devinsy.util.xml.XMLZipReader;
import fr.devinsy.util.xml.XMLZipWriter;

/**
 * This class represents a Sciboard File reader and writer.
 * 
 * @author TIP
 */
public class XMLSciboard
{
	private static final Logger logger = LoggerFactory.getLogger(XMLSciboard.class);

	/**
	 * 
	 * @param file
	 * @return
	 */
	public static Sciboard load(final File file) throws Exception
	{
		Sciboard result;

		XMLReader in = null;
		try
		{
			//
			in = new XMLZipReader(file);

			//
			in.readXMLHeader();

			//
			result = readSciboard(in);

		}
		finally
		{
			if (in != null)
			{
				in.close();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param in
	 * @return
	 * @throws XMLStreamException
	 * @throws XMLBadFormatException
	 */
	public static Comment readComment(final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		Comment result;

		//
		XMLTag tag = in.readStartTag("comment");

		//
		{
			//
			long id = Long.parseLong(in.readContentTag("id").getContent());
			long authorId = Long.parseLong(in.readContentTag("author_id").getContent());
			DateTime creationDate = DateTime.parse(in.readContentTag("creation_date").getContent());
			DateTime editionDate = DateTime.parse(in.readContentTag("edition_date").getContent());
			String text = StringEscapeUtils.unescapeXml(in.readNullableContentTag("text").getContent());

			result = new Comment(id, authorId, creationDate, editionDate, text);
		}

		//
		in.readEndTag("comment");

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static void readComments(final Comments target, final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{

		//
		XMLTag list = in.readListTag("comments");

		//
		if (list.getType() != TagType.EMPTY)
		{
			//
			while (in.hasNextStartTag("comment"))
			{

				//
				Comment comment = readComment(in);

				//
				target.add(comment);
			}

			//
			in.readEndTag("comments");
		}
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static void readProperties(final Properties target, final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		//
		XMLTag list = in.readListTag("properties");

		//
		if (list.getType() != TagType.EMPTY)
		{
			//
			while (in.hasNextStartTag("property"))
			{

				//
				in.readStartTag("property");

				//
				{
					//
					String key = StringEscapeUtils.unescapeXml(in.readContentTag("key").getContent());
					String value = StringEscapeUtils.unescapeXml(in.readNullableContentTag("value").getContent());
					if (value == null)
					{
						value = "";
					}

					//
					target.setProperty(key, value);
				}

				//
				in.readEndTag("property");
			}

			//
			in.readEndTag("properties");
		}
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws Exception
	 */
	public static Request readRequest(final String source) throws XMLStreamException, XMLBadFormatException
	{
		Request result;

		//
		XMLReader in = new XMLReader(new StringReader(source));

		//
		in.readXMLHeader();

		//
		result = readRequest(in);

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws Exception
	 */
	public static Request readRequest(final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		Request result;

		//
		XMLTag tag = in.readStartTag("request");

		//
		{
			//
			long id = Long.parseLong(tag.attributes().getByLabel("id").getValue());
			long contributorId = Long.parseLong(in.readContentTag("contributor_id").getContent());
			result = new Request(id, contributorId, null, null, null);

			result.setStatus(Request.Status.valueOf(in.readContentTag("status").getContent()));

			result.setCreationDate(DateTime.parse(in.readContentTag("creation_date").getContent()));
			result.setContributorComment(StringEscapeUtils.unescapeXml(in.readNullableContentTag("contributor_comment").getContent()));

			result.setType(Request.Type.valueOf(in.readContentTag("type").getContent()));
			result.setDecisionComment(StringEscapeUtils.unescapeXml(in.readNullableContentTag("decision_comment").getContent()));

			String decisionDate = in.readNullableContentTag("decision_date").getContent();
			if (decisionDate != null)
			{
				result.setDecisionDate(DateTime.parse(decisionDate));
			}

			String decisionMakerId = in.readNullableContentTag("decision_maker_id").getContent();
			if (decisionMakerId != null)
			{
				result.setDecisionMakerId(new Long(decisionMakerId));
			}

			readProperties(result.properties(), in);
			readVotes(result.votes(), in);
			readComments(result.comments(), in);
		}

		//
		in.readEndTag("request");

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static void readRequests(final Requests target, final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		//
		XMLTag list = in.readListTag("requests");

		//
		if (list.getType() != TagType.EMPTY)
		{
			//
			while (in.hasNextStartTag("request"))
			{

				//
				Request request = readRequest(in);

				//
				target.add(request);
			}

			//
			in.readEndTag("requests");
		}
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * @throws Exception
	 */
	public static Sciboard readSciboard(final String source) throws XMLStreamException, XMLBadFormatException
	{
		Sciboard result;

		//
		XMLReader in = new XMLReader(new StringReader(source));

		//
		in.readXMLHeader();

		//
		result = readSciboard(in);

		//
		return result;
	}

	/**
	 * Reads a net from a XMLReader object.
	 * 
	 * @param in
	 *            the source of reading.
	 * 
	 * @return the read net.
	 * 
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static Sciboard readSciboard(final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		Sciboard result;

		//
		result = new Sciboard();

		//
		in.readStartTag("sciboard");

		//
		{
			//
			readRequests(result.requests(), in);
		}

		//
		in.readEndTag("sciboard");

		//
		result.resetLastRequestId();
		result.rebuildIndexes();

		//
		return result;
	}

	/**
	 * 
	 * @param in
	 * @return
	 * @throws XMLStreamException
	 * @throws XMLBadFormatException
	 */
	public static Vote readVote(final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		Vote result;

		//
		in.readStartTag("vote");

		//
		{
			//
			long ownerId = Long.parseLong(in.readContentTag("owner_id").getContent());
			DateTime creationDate = DateTime.parse(in.readContentTag("creation_date").getContent());
			DateTime editionDate = DateTime.parse(in.readContentTag("edition_date").getContent());
			Vote.Value value = Vote.Value.valueOf(in.readContentTag("value").getContent());

			//
			result = new Vote(ownerId, creationDate, editionDate, value);
		}

		//
		in.readEndTag("vote");

		//
		return result;
	}

	/**
	 * 
	 * @param target
	 * @param in
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 */
	public static void readVotes(final Votes target, final XMLReader in) throws XMLStreamException, XMLBadFormatException
	{
		//
		XMLTag list = in.readListTag("votes");

		//
		if (list.getType() != TagType.EMPTY)
		{
			//
			while (in.hasNextStartTag("vote"))
			{

				//
				Vote vote = readVote(in);

				//
				target.add(vote);
			}

			//
			in.readEndTag("votes");
		}
	}

	/**
	 * Saves a net in a file.
	 * 
	 * @param file
	 *            Target.
	 * @param source
	 *            Source.
	 * 
	 */
	public static void save(final File file, final Sciboard source, final String generator) throws Exception
	{

		if (file == null)
		{
			throw new IllegalArgumentException("file is null.");
		}
		else if (source == null)
		{
			throw new IllegalArgumentException("source is null.");
		}
		else
		{
			XMLWriter out = null;
			try
			{
				//
				out = new XMLZipWriter(file, generator);

				//
				out.writeXMLHeader((String[]) null);

				//
				write(out, source);

			}
			catch (IOException exception)
			{
				logger.error(ExceptionUtils.getStackTrace(exception));
				throw new Exception("Error saving file [" + file + "]", exception);
			}
			finally
			{
				if (out != null)
				{
					out.flush();
					out.close();
				}
			}
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String toXMLString(final Request source) throws UnsupportedEncodingException
	{
		String result;

		//
		StringWriter xmlTarget = new StringWriter(1024);

		//
		XMLWriter xmlWriter = new XMLWriter(xmlTarget);

		//
		xmlWriter.writeXMLHeader((String[]) null);

		//
		XMLSciboard.write(xmlWriter, source);

		//
		result = xmlTarget.toString();

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 * @throws UnsupportedEncodingException
	 */
	public static String toXMLString(final Sciboard source) throws UnsupportedEncodingException
	{
		String result;

		//
		StringWriter xmlTarget = new StringWriter(1024);

		//
		XMLWriter xmlWriter = new XMLWriter(xmlTarget);

		//
		xmlWriter.writeXMLHeader((String[]) null);

		//
		XMLSciboard.write(xmlWriter, source);

		//
		result = xmlTarget.toString();

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final Comment source)
	{

		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if (source == null)
		{
			throw new IllegalArgumentException("source is null.");
		}
		else
		{
			//
			out.writeStartTag("comment");

			//
			{
				out.writeTag("id", source.getId());
				out.writeTag("author_id", source.getAuthorId());
				out.writeTag("creation_date", source.getCreationDate().toString());
				out.writeTag("edition_date", source.getEditionDate().toString());
				out.writeTag("text", StringEscapeUtils.escapeXml(source.getText()));
			}

			//
			out.writeEndTag("comment");
		}
	}

	/**
	 * 
	 * @param out
	 * @param source
	 */
	public static void write(final XMLWriter out, final Comments source)
	{

		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if ((source == null) || (source.isEmpty()))
		{
			out.writeEmptyTag("comments");
		}
		else
		{
			//
			out.writeStartTag("comments", "size", String.valueOf(source.size()));

			{
				//
				for (Comment comment : source)
				{
					write(out, comment);
				}
			}

			//
			out.writeEndTag("comments");
		}
	}

	/**
	 * 
	 * @param out
	 * @param source
	 */
	public static void write(final XMLWriter out, final Properties source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if ((source == null) || (source.isEmpty()))
		{
			out.writeEmptyTag("properties");
		}
		else
		{
			//
			out.writeStartTag("properties", "size", String.valueOf(source.size()));

			//
			for (Object key : source.keySet())
			{
				//
				out.writeStartTag("property");
				{
					//
					out.writeTag("key", StringEscapeUtils.escapeXml((String) key));
					out.writeTag("value", StringEscapeUtils.escapeXml(source.getProperty((String) key)));
				}

				//
				out.writeEndTag("property");
			}

			//
			out.writeEndTag("properties");
		}
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static void write(final XMLWriter out, final Request source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if (source == null)
		{
			throw new IllegalArgumentException("request is null.");
		}
		else
		{
			//
			out.writeStartTag("request", "id", String.valueOf(source.getId()));

			//
			{
				out.writeTag("contributor_id", source.getContributorId());
				out.writeTag("status", source.getStatus().toString());
				out.writeTag("creation_date", source.getCreationDate().toString());
				out.writeTag("contributor_comment", StringEscapeUtils.escapeXml(source.getContributorComment()));
				out.writeTag("type", source.getType().toString());
				out.writeTag("decision_comment", StringEscapeUtils.escapeXml(source.getDecisionComment()));
				if (source.getDecisionDate() == null)
				{
					out.writeTag("decision_date", null);
				}
				else
				{
					out.writeTag("decision_date", source.getDecisionDate().toString());
				}
				if (source.getDecisionMakerId() == null)
				{
					out.writeTag("decision_maker_id", null);
				}
				else
				{
					out.writeTag("decision_maker_id", source.getDecisionMakerId());
				}

				write(out, source.properties());
				write(out, source.votes());
				write(out, source.comments());
			}

			//
			out.writeEndTag("request");
		}
	}

	/**
	 * 
	 * @param out
	 * @param source
	 */
	public static void write(final XMLWriter out, final Requests source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if ((source == null) || (source.isEmpty()))
		{
			out.writeEmptyTag("requests");
		}
		else
		{
			//
			out.writeStartTag("requests", "size", String.valueOf(source.size()));

			{
				//
				for (Request request : source)
				{
					write(out, request);
				}
			}

			//
			out.writeEndTag("requests");
		}
	}

	/**
	 * Writes a net in an stream.
	 * 
	 * @param out
	 *            Target.
	 * 
	 * @param source
	 *            Source.
	 */
	public static void write(final XMLWriter out, final Sciboard source)
	{
		//
		out.writeStartTag("sciboard");

		//
		write(out, source.requests());

		//
		out.writeEndTag("sciboard");
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	static public void write(final XMLWriter out, final Vote source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if (source == null)
		{
			throw new IllegalArgumentException("source is null.");
		}
		else
		{
			//
			out.writeStartTag("vote");

			//
			{
				out.writeTag("owner_id", source.getOwnerId());
				out.writeTag("creation_date", source.getCreationDate().toString());
				out.writeTag("edition_date", source.getEditionDate().toString());
				out.writeTag("value", source.getValue().toString());
			}

			//
			out.writeEndTag("vote");
		}
	}

	/**
	 * 
	 * @param out
	 * @param source
	 */
	public static void write(final XMLWriter out, final Votes source)
	{
		if (out == null)
		{
			throw new IllegalArgumentException("out is null.");
		}
		else if ((source == null) || (source.isEmpty()))
		{
			out.writeEmptyTag("votes");
		}
		else
		{
			//
			out.writeStartTag("votes", "size", String.valueOf(source.size()));

			{
				//
				for (Vote vote : source)
				{
					write(out, vote);
				}
			}

			//
			out.writeEndTag("votes");
		}
	}
}
