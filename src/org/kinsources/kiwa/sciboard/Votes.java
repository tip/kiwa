/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.sciboard;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class Votes implements Iterable<Vote>
{
	private HashMap<Long, Vote> votes;

	/**
	 * 
	 */
	public Votes()
	{
		this.votes = new HashMap<Long, Vote>();
	}

	/**
	 * 
	 */
	public Votes(final int initialCapacity)
	{
		this.votes = new HashMap<Long, Vote>(initialCapacity);
	}

	/**
	 * 
	 * @param log
	 */
	public void add(final Vote source)
	{
		//
		if (source == null)
		{
			throw new IllegalArgumentException("source is null.");
		}
		else
		{
			this.votes.put(source.getOwnerId(), source);
		}
	}

	/**
	 * 
	 */
	public void clear()
	{
		this.votes.clear();
	}

	/**
	 * This methods returns a shallow copy of the current object.
	 * 
	 * @return a shallow copy of the current object.
	 */
	public Votes copy()
	{
		Votes result;

		//
		result = new Votes(this.votes.size());

		//
		for (Vote vote : this.votes.values())
		{
			result.add(vote);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param index
	 * @return
	 */
	public Vote getByOwnerId(final long id)
	{
		Vote result;

		result = this.votes.get(id);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty()
	{
		boolean result;

		result = this.votes.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty()
	{
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Iterator<Vote> iterator()
	{
		Iterator<Vote> result;

		result = this.votes.values().iterator();

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 */
	synchronized public void remove(final Vote vote)
	{
		this.votes.remove(vote);
	}

	/**
	 * 
	 * @return
	 */
	public int size()
	{
		int result;

		result = this.votes.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public List<Vote> toList()
	{
		List<Vote> result;

		result = new ArrayList<Vote>(this.votes.size());
		for (Vote vote : this.votes.values())
		{
			result.add(vote);
		}

		//
		return result;
	}
}
