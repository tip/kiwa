/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.actulog;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Locale;

import org.kinsources.kiwa.actulog.WireComparator.Criteria;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class Wires implements Iterable<Wire>
{
	private ArrayList<Wire> wires;

	/**
	 * 
	 */
	public Wires()
	{
		this.wires = new ArrayList<Wire>();
	}

	/**
	 * 
	 */
	public Wires(final int initialCapacity)
	{
		this.wires = new ArrayList<Wire>(initialCapacity);
	}

	/**
	 * 
	 * @param eventLog
	 */
	public void add(final Wire wire)
	{
		//
		if (wire == null)
		{
			throw new IllegalArgumentException("eventLog is null.");
		}
		else if (wire.getTitle() == null)
		{
			throw new IllegalArgumentException("title is null.");
		}
		else
		{
			this.wires.add(wire);
		}
	}

	/**
	 * 
	 */
	public void clear()
	{
		this.wires.clear();
	}

	/**
	 * This methods returns a shallow copy of the current object.
	 * 
	 * @return a shallow copy of the current object.
	 */
	public Wires copy()
	{
		Wires result;

		//
		result = new Wires(this.wires.size());

		//
		for (Wire wire : this.wires)
		{
			result.add(wire);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetCount
	 * @return
	 */
	public Wire first()
	{
		Wire result;

		//
		if (this.wires.isEmpty())
		{
			result = null;
		}
		else
		{
			result = getByIndex(0);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetCount
	 * @return
	 */
	public Wires first(final int targetCount)
	{
		Wires result;

		//
		result = new Wires(targetCount);

		//
		boolean ended = false;
		Iterator<Wire> iterator = iterator();
		int count = 0;
		while (!ended)
		{
			if ((count > targetCount) || (!iterator.hasNext()))
			{
				ended = true;
			}
			else
			{
				result.add(iterator.next());
				count += 1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param index
	 * @return
	 */
	public Wire getByIndex(final int index)
	{
		Wire result;

		result = this.wires.get(index);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty()
	{
		boolean result;

		result = this.wires.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty()
	{
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Iterator<Wire> iterator()
	{
		Iterator<Wire> result;

		result = this.wires.iterator();

		//
		return result;
	}

	/**
	 * 
	 * @param targetCount
	 * @return
	 */
	public Wire last()
	{
		Wire result;

		//
		if (this.wires.isEmpty())
		{
			result = null;
		}
		else
		{
			result = getByIndex(this.wires.size() - 1);
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	synchronized public long lastId()
	{
		long result;

		//
		result = 0;
		for (Wire wire : this.wires)
		{
			if (wire.getId() > result)
			{
				result = wire.getId();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetCount
	 * @return
	 */
	public Wire latestPublishedWire(final Locale locale)
	{
		Wire result;

		//
		result = publishedWires(locale).sortByPublicationDate().last();

		//
		return result;
	}

	/**
	 * 
	 * @param targetCount
	 * @return
	 */
	public Wires latestPublishedWires(final int targetCount)
	{
		Wires result;

		//
		result = publishedWires().sortByPublicationDate().reverse().first(targetCount);

		//
		return result;
	}

	/**
	 * 
	 * @param targetCount
	 * @return
	 */
	public Wires latestPublishedWires(final int targetCount, final Locale locale)
	{
		Wires result;

		//
		result = publishedWires(locale).sortByPublicationDate().reverse().first(targetCount);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Wires publishedWires()
	{
		Wires result;

		//
		result = new Wires(this.wires.size());

		//
		for (Wire wire : this.wires)
		{
			if (wire.isPublished())
			{
				result.add(wire);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Wires publishedWires(final Locale locale)
	{
		Wires result;

		if (locale == null)
		{
			result = publishedWires();
		}
		else
		{
			//
			Wires publishableWires = publishedWires();

			//
			result = new Wires(publishableWires.size());

			//
			for (Wire wire : publishableWires)
			{
				if (locale.equals(wire.getLocale()))
				{
					result.add(wire);
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 */
	synchronized public void remove(final Wire wire)
	{
		this.wires.remove(wire);
	}

	/**
	 * 
	 * @return
	 */
	public Wires reverse()
	{
		Wires result;

		//
		Collections.reverse(this.wires);

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int size()
	{
		int result;

		result = this.wires.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Wires sortById()
	{
		Wires result;

		//
		Collections.sort(this.wires, new WireComparator(Criteria.ID));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Wires sortByPublicationDate()
	{
		Wires result;

		//
		Collections.sort(this.wires, new WireComparator(Criteria.PUBLICATION_DATE));

		//
		result = this;

		//
		return result;
	}
}
