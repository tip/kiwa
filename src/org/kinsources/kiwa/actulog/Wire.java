/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.actulog;

import java.util.Locale;

import org.joda.time.DateTime;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class Wire
{
	public enum Status
	{
		DRAFT,
		PUBLISHABLE,
		PUBLISHED
	}

	private long id;
	private String title;
	private String author;
	private DateTime creationDate;
	private DateTime editionDate;
	private DateTime publicationDate;
	private Locale locale;
	private String leadParagraph;
	private String body;

	/**
	 * 
	 * @param category
	 * @param event
	 * @param comment
	 */
	public Wire(final long id, final String title, final Locale locale)
	{
		//
		if (title == null)
		{
			throw new IllegalArgumentException("title is null");
		}
		else if (locale == null)
		{
			throw new IllegalArgumentException("locale is null");
		}
		else
		{
			this.id = id;
			this.title = title;
			this.creationDate = DateTime.now();
			this.editionDate = this.creationDate;
			this.publicationDate = null;
			this.locale = locale;
		}
	}

	/**
	 * 
	 * @param category
	 * @param event
	 * @param comment
	 */
	public Wire(final long id, final String title, final String author, final DateTime creationDate, final DateTime editionDate, final DateTime publicationDate, final Locale locale,
			final String leadParagraph, final String text)
	{
		//
		if (title == null)
		{
			throw new IllegalArgumentException("title is null");
		}
		else if (creationDate == null)
		{
			throw new IllegalArgumentException("creationDate is null");
		}
		else if (editionDate == null)
		{
			throw new IllegalArgumentException("editionDate is null");
		}
		else if (locale == null)
		{
			throw new IllegalArgumentException("locale is null");
		}
		else
		{
			this.id = id;
			this.title = title;
			this.author = author;
			this.creationDate = creationDate;
			this.editionDate = editionDate;
			this.publicationDate = publicationDate;
			this.locale = locale;
			this.leadParagraph = leadParagraph;
			this.body = text;
		}
	}

	public String getAuthor()
	{
		return this.author;
	}

	public String getBody()
	{
		return this.body;
	}

	public DateTime getCreationDate()
	{
		return this.creationDate;
	}

	public DateTime getEditionDate()
	{
		return this.editionDate;
	}

	public long getId()
	{
		return this.id;
	}

	public String getLeadParagraph()
	{
		return this.leadParagraph;
	}

	public Locale getLocale()
	{
		return this.locale;
	}

	public DateTime getPublicationDate()
	{
		return this.publicationDate;
	}

	public String getTitle()
	{
		return this.title;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotPublished()
	{
		boolean result;

		result = !isPublished();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isPublished()
	{
		boolean result;

		if ((this.publicationDate != null) && (this.publicationDate.isBeforeNow()))
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public void publish()
	{
		//
		this.publicationDate = DateTime.now();
	}

	public void setAuthor(final String author)
	{
		this.author = author;
	}

	public void setBody(final String body)
	{
		this.body = body;
	}

	public void setCreationDate(final DateTime creationDate)
	{
		this.creationDate = creationDate;
	}

	public void setEditionDate(final DateTime editionDate)
	{
		this.editionDate = editionDate;
	}

	public void setId(final long id)
	{
		this.id = id;
	}

	public void setLeadParagraph(final String leadParagraph)
	{
		this.leadParagraph = leadParagraph;
	}

	public void setLocale(final Locale locale)
	{
		this.locale = locale;
	}

	/**
	 * 
	 * @param publicationDate
	 */
	public void setPublicationDate(final DateTime publicationDate)
	{
		//
		this.publicationDate = publicationDate;
	}

	public void setTitle(final String title)
	{
		if (title == null)
		{
			throw new IllegalArgumentException("title is null.");
		}
		else
		{
			this.title = title;
		}
	}

	/**
	 * 
	 * @return
	 */
	public Status status()
	{
		Status result;

		if (this.publicationDate == null)
		{
			result = Status.DRAFT;
		}
		else if (isPublished())
		{
			result = Status.PUBLISHED;
		}
		else
		{
			result = Status.PUBLISHABLE;
		}

		//
		return result;
	}

	/**
	 * 
	 */
	public void unpublish()
	{
		//
		this.publicationDate = null;
	}
}
