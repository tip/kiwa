/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.seligo;

import org.apache.commons.lang3.StringUtils;
import org.kinsources.kiwa.kidarep.Dataset;

import fr.devinsy.util.StringList;

/**
 * 
 * 
 * @author Christian P. Momon
 */
public class SearchCriteria
{
	public enum AttributeDescriptorFilter
	{
		NONE,
		OPTIONAL,
		REQUIRED
	}

	public enum DatasetFileScope
	{
		NONE,
		OPTIONAL,
		REQUIRED
	}

	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(SearchCriteria.class);

	//
	private Dataset.Type datasetType;
	private String atlasCode;
	private String author;
	private String coder;
	private String continent;
	private String contributor;
	private String datasetName;
	private String country;
	private String location;
	private String organization;
	private String region;
	private DatasetFileScope datasetFileScope;
	private AttributeDescriptorFilter attributeDescriptor;
	private Intervals intervals;

	private String termsSelfTerm;
	private String termsRecursive;
	private String termsAscendantClassification;
	private String termsCousinClassification;

	/**
	 * 
	 */
	public SearchCriteria()
	{
		this.datasetFileScope = DatasetFileScope.REQUIRED;
		this.attributeDescriptor = AttributeDescriptorFilter.OPTIONAL;
		this.intervals = new Intervals();
	}

	public String getAtlasCode()
	{
		return this.atlasCode;
	}

	public AttributeDescriptorFilter getAttributeDescriptorFilter()
	{
		return this.attributeDescriptor;
	}

	public String getAuthor()
	{
		return this.author;
	}

	public String getCoder()
	{
		return this.coder;
	}

	public String getContinent()
	{
		return this.continent;
	}

	public String getContributor()
	{
		return this.contributor;
	}

	public String getCountry()
	{
		return this.country;
	}

	public DatasetFileScope getDatasetFileScope()
	{
		return this.datasetFileScope;
	}

	public String getDatasetName()
	{
		return this.datasetName;
	}

	public Dataset.Type getDatasetType()
	{
		return this.datasetType;
	}

	public String getLocation()
	{
		return this.location;
	}

	public String getOrganization()
	{
		return this.organization;
	}

	public String getRegion()
	{
		return this.region;
	}

	public String getsTermsSelfTerm()
	{
		return this.termsSelfTerm;
	}

	public String getTermsAscendantClassification()
	{
		return this.termsAscendantClassification;
	}

	public String getTermsCousinClassification()
	{
		return this.termsCousinClassification;
	}

	public String getTermsRecursive()
	{
		return this.termsRecursive;
	}

	public Intervals intervals()
	{
		return this.intervals;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty()
	{
		boolean result;

		if (StringUtils.isNotBlank(this.atlasCode))
		{
			result = false;
		}
		else if (StringUtils.isNotBlank(this.author))
		{
			result = false;
		}
		else if (StringUtils.isNotBlank(this.coder))
		{
			result = false;
		}
		else if (StringUtils.isNotBlank(this.continent))
		{
			result = false;
		}
		else if (StringUtils.isNotBlank(this.contributor))
		{
			result = false;
		}
		else if (StringUtils.isNotBlank(this.datasetName))
		{
			result = false;
		}
		else if (StringUtils.isNotBlank(this.country))
		{
			result = false;
		}
		else if (StringUtils.isNotBlank(this.location))
		{
			result = false;
		}
		else if (StringUtils.isNotBlank(this.organization))
		{
			result = false;
		}
		else if (StringUtils.isNotBlank(this.region))
		{
			result = false;
		}
		else
		{
			result = this.intervals.isEmpty();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty()
	{
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	public void setAtlasCode(final String atlasCode)
	{
		this.atlasCode = atlasCode;
	}

	public void setAttributeDescriptor(final AttributeDescriptorFilter filter)
	{
		if (filter != null)
		{
			this.attributeDescriptor = filter;
		}
	}

	public void setAuthor(final String author)
	{
		this.author = author;
	}

	public void setCoder(final String coder)
	{
		this.coder = coder;
	}

	public void setContinent(final String continent)
	{
		this.continent = continent;
	}

	public void setContributor(final String contributor)
	{
		this.contributor = contributor;
	}

	public void setCountry(final String country)
	{
		this.country = country;
	}

	public void setDatasetFileScope(final DatasetFileScope value)
	{
		if (value == null)
		{
			this.datasetFileScope = DatasetFileScope.REQUIRED;
		}
		else
		{
			this.datasetFileScope = value;
		}
	}

	public void setDatasetName(final String datasetName)
	{
		this.datasetName = datasetName;
	}

	public void setDatasetType(final Dataset.Type type)
	{
		if (type != null)
		{
			this.datasetType = type;
		}
	}

	public void setLocation(final String location)
	{
		this.location = location;
	}

	public void setOrganization(final String organization)
	{
		this.organization = organization;
	}

	public void setRegion(final String region)
	{
		this.region = region;
	}

	public void setTermsAscendantClassification(final String termsAscendantClassification)
	{
		this.termsAscendantClassification = termsAscendantClassification;
	}

	public void setTermsCousinClassification(final String termsCousinClassification)
	{
		this.termsCousinClassification = termsCousinClassification;
	}

	public void setTermsRecursive(final String termsRecursive)
	{
		this.termsRecursive = termsRecursive;
	}

	public void setTermsSelfTerm(final String termsSelfTerm)
	{
		this.termsSelfTerm = termsSelfTerm;
	}

	/**
	 * 
	 */
	@Override
	public String toString()
	{
		String result;

		//
		StringList buffer = new StringList();

		//
		toString(buffer, "dataset_type", this.datasetFileScope.toString());
		toString(buffer, "dataset_file", this.datasetFileScope.toString());
		toString(buffer, "attributes_descriptors", this.attributeDescriptor.toString());
		toString(buffer, "organization", this.organization);
		toString(buffer, "atlasCode", this.atlasCode);
		toString(buffer, "author", this.author);
		toString(buffer, "coder", this.coder);
		toString(buffer, "continent", this.continent);
		toString(buffer, "dataset_name", this.datasetName);
		toString(buffer, "country", this.country);
		toString(buffer, "contributor", this.contributor);
		toString(buffer, "location", this.location);
		toString(buffer, "region", this.region);
		toString(buffer, "self_name", this.termsSelfTerm);
		toString(buffer, "recursive_terms", this.termsRecursive);
		toString(buffer, "terms_ascendant_classification", this.termsAscendantClassification);
		toString(buffer, "terms_cousin_classification", this.termsCousinClassification);

		//
		if (this.intervals.isNotEmpty())
		{
			for (Interval interval : this.intervals)
			{
				if ((interval.isNotBiggest() && (interval.isNotBiggestPositive())) && (interval.isNotBiggestPercentage()))
				{
					buffer.append(interval.toStringWithName());
					buffer.append(";");
				}
			}
		}

		//
		buffer.removeLast();

		//
		result = buffer.toString();

		//
		return result;
	}

	/**
	 * 
	 * @param key
	 * @return
	 */
	private static void toString(final StringList buffer, final String key, final String value)
	{
		//
		if (StringUtils.isNotBlank(value))
		{
			buffer.append(key).append("=").append(value).append(";");
		}
	}
}
