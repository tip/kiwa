/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.seligo;

import java.text.Normalizer;

import org.apache.commons.lang3.StringUtils;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.kidarep.Datasets;
import org.kinsources.kiwa.kidarep.Statistics;
import org.kinsources.kiwa.stag.StatisticTag;

import fr.devinsy.util.StringList;

/**
 * 
 * 
 * @author Christian P. Momon
 */
public class Seligo
{
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Seligo.class);
	private static final Kiwa kiwa = Kiwa.instance();

	/**
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
	public static boolean match(final String source, final String pattern)
	{
		boolean result;

		String target = normalizeField(source);

		if (target == null)
		{
			result = false;
		}
		else if (pattern == null)
		{
			result = false;
		}
		else
		{
			result = target.matches(pattern);
		}

		logger.debug("[{}]->[{}]o[{}] = [{}]", source, target, pattern, result);

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
	public static boolean matchCaseSensitive(final String source, final String pattern)
	{
		boolean result;

		String target = normalizeCaseSensitiveField(source);

		if (target == null)
		{
			result = false;
		}
		else if (pattern == null)
		{
			result = false;
		}
		else
		{
			result = target.matches(pattern);
		}

		logger.debug("[{}]->[{}]o[{}] = [{}]", source, target, pattern, result);

		//
		return result;
	}

	/**
	 * 
	 * @param criteria
	 * @param dataset
	 * @return
	 */
	public static String matching(final SearchCriteria criteria, final Dataset dataset)
	{
		String result;

		if (dataset.getOriginFile() == null)
		{
			//
			result = null;

		}
		else
		{
			//
			StringList buffer = new StringList();

			if (StringUtils.isNotBlank(criteria.getContributor()))
			{
				buffer.append("contributor").append("=").append(dataset.getContributor().getFullName()).append("; ");
			}

			if (StringUtils.isNotBlank(criteria.getOrganization()))
			{
				buffer.append("contributor_organization").append("=").append(dataset.getContributor().account().getOrganization()).append("; ");
			}

			//
			Statistics statistics = dataset.getOriginFile().statistics();
			for (Interval interval : criteria.intervals())
			{
				if (StatisticTag.isNotBasic(interval.getName()))
				{
					buffer.append(interval.getName()).append("=").append(statistics.getStringValue(interval.getName().toUpperCase())).append("; ");
				}
			}
			buffer.removeLast();

			//
			result = buffer.toString();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String normalizeCaseSensitiveCriteria(final String source)
	{
		String result;

		result = Seligo.toRegex(StringUtils.trim(source));

		logger.debug("[{}]->[{}]", source, result);

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String normalizeCaseSensitiveField(final String source)
	{
		String result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			result = Normalizer.normalize(source, Normalizer.Form.NFD);
			result = result.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String normalizeCriteria(final String source)
	{
		String result;

		result = Seligo.toRegex(toLowerCase(StringUtils.trim(source)));

		logger.debug("[{}]->[{}]", source, result);

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String normalizeField(final String source)
	{
		String result;

		result = toLowerCase(source);

		// logger.debug("[{}]->[{}]", source, result);

		//
		return result;
	}

	/**
	 * 
	 * @param name
	 * @return
	 */
	public static Datasets search(final Long requesterId, final SearchCriteria criteria)
	{
		Datasets result;

		//
		result = kiwa.kidarep().datasetsFromIndex();
		result = result.findVisible(requesterId, Dataset.Status.VALIDATED);

		// Dataset type.
		{
			if (criteria.getDatasetType() != null)
			{
				result = result.find(criteria.getDatasetType());
			}
		}

		// Dataset file scope.
		{
			//
			switch (criteria.getDatasetFileScope())
			{
				case REQUIRED:
				{
					Datasets source = result;
					result = new Datasets();
					for (Dataset dataset : source)
					{
						if (dataset.getOriginFile() != null)
						{
							result.add(dataset);
						}
					}
				}
				break;

				case NONE:
				{
					Datasets source = result;
					result = new Datasets();
					for (Dataset dataset : source)
					{
						if (dataset.getOriginFile() == null)
						{
							result.add(dataset);
						}
					}
				}
				break;

				default:
			}
		}

		// Attribute filter.
		if (criteria.getAttributeDescriptorFilter() != null)
		{
			logger.debug("Attribute desriptor filter=[{}]", criteria.getAttributeDescriptorFilter());
			switch (criteria.getAttributeDescriptorFilter())
			{
				case REQUIRED:
				{
					Datasets source = result;
					result = new Datasets();
					for (Dataset dataset : source)
					{
						if (dataset.hasAttributesDescriptors())
						{
							logger.debug("[{}][{}][{}][{}]", dataset.hasAttributesDescriptors(), dataset.getName(), dataset.getOriginFile().statistics().attributeDescriptors().size(), dataset
									.getOriginFile().statistics().attributeDescriptors().getByIndex(0).getLabel());
							result.add(dataset);
						}
					}
				}
				break;

				case NONE:
				{
					Datasets source = result;
					result = new Datasets();
					for (Dataset dataset : source)
					{
						if (!dataset.hasAttributesDescriptors())
						{
							result.add(dataset);
						}
					}
				}
				break;

				default:
			}
		}

		// Atlas code.
		if (StringUtils.isNotBlank(criteria.getAtlasCode()))
		{
			//
			Datasets source = result;
			result = new Datasets();

			//
			String pattern = normalizeCaseSensitiveCriteria(criteria.getAtlasCode());
			for (Dataset dataset : source)
			{
				if (matchCaseSensitive(dataset.getAtlasCode(), pattern))
				{
					result.add(dataset);
				}
			}
		}

		// Author.
		if (StringUtils.isNotBlank(criteria.getAuthor()))
		{
			//
			Datasets source = result;
			result = new Datasets();

			//
			String pattern = normalizeCriteria(criteria.getAuthor());
			for (Dataset dataset : source)
			{
				if (match(dataset.getAuthor(), pattern))
				{
					result.add(dataset);
				}
			}
		}

		// Coder.
		if (StringUtils.isNotBlank(criteria.getCoder()))
		{
			//
			Datasets source = result;
			result = new Datasets();

			//
			String pattern = normalizeCriteria(criteria.getCoder());
			for (Dataset dataset : source)
			{
				if (match(dataset.getCoder(), pattern))
				{
					result.add(dataset);
				}
			}
		}

		// Dataset name.
		if (StringUtils.isNotBlank(criteria.getDatasetName()))
		{
			//
			Datasets source = result;
			result = new Datasets();

			//
			String pattern = normalizeCriteria(criteria.getDatasetName());
			for (Dataset dataset : source)
			{
				if (match(dataset.getName(), pattern))
				{
					result.add(dataset);
				}
			}
		}

		// Country.
		if (StringUtils.isNotBlank(criteria.getCountry()))
		{
			//
			Datasets source = result;
			result = new Datasets();

			//
			String pattern = normalizeCriteria(criteria.getCountry());
			for (Dataset dataset : source)
			{
				if (match(dataset.getCountry(), pattern))
				{
					result.add(dataset);
				}
			}
		}

		// Contributor.
		if (StringUtils.isNotBlank(criteria.getContributor()))
		{
			//
			Datasets source = result;
			result = new Datasets();

			//
			String pattern = normalizeCriteria(criteria.getContributor());
			for (Dataset dataset : source)
			{
				if (match(dataset.getContributor().getLastName(), pattern))
				{
					result.add(dataset);
				}
			}
		}

		// Organization.
		if (StringUtils.isNotBlank(criteria.getOrganization()))
		{
			//
			Datasets source = result;
			result = new Datasets();

			//
			String pattern = normalizeCriteria(criteria.getOrganization());
			for (Dataset dataset : source)
			{
				if (match(dataset.getContributor().account().getOrganization(), pattern))
				{
					result.add(dataset);
				}
			}
		}

		// Region.
		if (StringUtils.isNotBlank(criteria.getRegion()))
		{
			//
			Datasets source = result;
			result = new Datasets();

			//
			String pattern = normalizeCriteria(criteria.getRegion());
			for (Dataset dataset : source)
			{
				if (match(dataset.getRegion(), pattern))
				{
					result.add(dataset);
				}
			}
		}

		// Continent.
		if (StringUtils.isNotBlank(criteria.getContinent()))
		{
			//
			Datasets source = result;
			result = new Datasets();

			//
			String pattern = normalizeCriteria(criteria.getContinent());
			for (Dataset dataset : source)
			{
				if (match(dataset.getContinent(), pattern))
				{
					result.add(dataset);
				}
			}
		}

		// Location.
		if (StringUtils.isNotBlank(criteria.getLocation()))
		{
			//
			Datasets source = result;
			result = new Datasets();

			//
			String pattern = normalizeCriteria(criteria.getLocation());
			for (Dataset dataset : source)
			{
				if (match(dataset.getLocation(), pattern))
				{
					result.add(dataset);
				}
			}
		}

		//
		for (Interval interval : criteria.intervals())
		{
			//
			if ((interval.isNotBiggest() && (interval.isNotBiggestPositive())) && (interval.isNotBiggestPercentage()))
			{
				//
				Datasets source = result;
				result = new Datasets();

				//
				String criteriaName = interval.getName().toUpperCase();

				//
				for (Dataset dataset : source)
				{
					if ((dataset.getOriginFile() != null) && (interval.matches(dataset.getDoubleStat(criteriaName))))
					{
						result.add(dataset);
					}
				}
			}
		}

		// Terms Self Term.
		if (StringUtils.isNotBlank(criteria.getsTermsSelfTerm()))
		{
			//
			Datasets source = result;
			result = new Datasets();

			//
			for (Dataset dataset : source)
			{
				if ((dataset.getOriginFile() != null) && (StringUtils.equals(dataset.getStringStat(StatisticTag.SELF_NAME.name()), criteria.getsTermsSelfTerm())))
				{
					result.add(dataset);
				}
			}
		}

		// Terms Self Term.
		if (StringUtils.isNotBlank(criteria.getTermsRecursive()))
		{
			//
			Datasets source = result;
			result = new Datasets();

			//
			String pattern = normalizeCriteria(criteria.getTermsRecursive());
			for (Dataset dataset : source)
			{
				if ((dataset.getOriginFile() != null) && (match(dataset.getStringStat(StatisticTag.RECURSIVE_TERMS.name()), pattern)))
				{
					result.add(dataset);
				}
			}
		}

		// Terms Ascendant Classification.
		if (StringUtils.isNotBlank(criteria.getTermsAscendantClassification()))
		{
			//
			Datasets source = result;
			result = new Datasets();

			//
			for (Dataset dataset : source)
			{
				if (dataset.getOriginFile() != null)
				{
					String value = dataset.getStringStat(StatisticTag.ASCENDANT_CLASSIFICATION.name());
					String searched = criteria.getTermsAscendantClassification();
					logger.debug("Terms Ascendant Classification: [searched={}][statistics={}]", searched, value);

					if (StringUtils.equals(value, searched))
					{
						result.add(dataset);
					}
				}
			}
		}

		// Terms Cousin Classification.
		if (StringUtils.isNotBlank(criteria.getTermsCousinClassification()))
		{
			//
			Datasets source = result;
			result = new Datasets();

			//
			for (Dataset dataset : source)
			{
				if (dataset.getOriginFile() != null)
				{
					String value = dataset.getStringStat(StatisticTag.COUSIN_CLASSIFICATION.name());
					String searched = criteria.getTermsCousinClassification();

					logger.debug("Terms Cousin Classification: [searched={}][statistics={}]", searched, value);
					if (StringUtils.equals(value, searched))
					{
						result.add(dataset);
					}
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param name
	 * @return
	 */
	public static Datasets search(final Long requesterId, final String datasetNameCriteria)
	{
		Datasets result;

		//
		String pattern = normalizeCriteria(datasetNameCriteria);

		//
		Datasets source = kiwa.kidarep().datasetsFromIndex();
		source = source.findVisible(requesterId, Dataset.Status.VALIDATED);

		//
		result = new Datasets();
		for (Dataset dataset : source)
		{
			if (match(dataset.getName(), pattern))
			{
				result.add(dataset);
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String toLowerCase(final String source)
	{
		String result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			result = Normalizer.normalize(source, Normalizer.Form.NFD);
			result = result.replaceAll("\\p{InCombiningDiacriticalMarks}+", "");
			result = result.toLowerCase();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static String toRegex(final String source)
	{
		String result;

		if (source == null)
		{
			result = null;
		}
		else
		{
			//
			result = source.replace(".", "\\.");

			//
			result = result.replace("*", ".*");

			//
			result = result.replace("%", ".");

			//
			result = result.replace("#", "\\b");
		}

		//
		return result;
	}
}
