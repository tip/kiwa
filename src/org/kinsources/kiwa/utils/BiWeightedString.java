/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.utils;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class BiWeightedString
{
	private String string;
	private Double weight1;
	private Double weight2;

	/**
	 * 
	 * @param string
	 * @param weight1
	 * @param weight2
	 */
	public BiWeightedString(final String string, final Double weight1, final Double weight2)
	{
		this.string = string;
		this.weight1 = weight1;
		this.weight2 = weight2;
	}

	public String getString()
	{
		return this.string;
	}

	public Double getWeight1()
	{
		return this.weight1;
	}

	public Double getWeight2()
	{
		return this.weight2;
	}

	public void setString(final String string)
	{
		this.string = string;
	}

	public void setWeight1(final Double weight1)
	{
		this.weight1 = weight1;
	}

	public void setWeight2(final Double weight2)
	{
		this.weight2 = weight2;
	}
}
