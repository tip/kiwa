/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.utils;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

/**
 * This code comes from http://en.wikipedia.org/wiki/JavaMail
 * 
 */
public class SendMailUsage
{
	/*
	 * Static nested class to act as a JAF datasource to send HTML e-mail content
	 */
	static class HTMLDataSource implements DataSource
	{
		private String html;

		public HTMLDataSource(final String htmlString)
		{
			this.html = htmlString;
		}

		/**
         * 
         */
		@Override
		public String getContentType()
		{
			return "text/html";
		}

		//
		//
		/**
		 * Return HTML string in an InputStream.
		 * 
		 * A new stream must be returned each time.
		 */
		@Override
		public InputStream getInputStream() throws IOException
		{
			if (this.html == null)
			{
				throw new IOException("Null HTML");
			}
			return new ByteArrayInputStream(this.html.getBytes());
		}

		/**
         * 
         */
		@Override
		public String getName()
		{
			return "JAF text/html dataSource to send e-mail only";
		}

		/**
         * 
         */
		@Override
		public OutputStream getOutputStream() throws IOException
		{
			throw new IOException("This DataHandler cannot write HTML");
		}
	}

	/**
	 * 
	 * @param args
	 */
	public static void main(final String[] args)
	{

		// SUBSTITUTE YOUR EMAIL ADDRESSES HERE!!!
		String to = "sendToMailAddress";
		String from = "sendFromMailAddress";
		// SUBSTITUTE YOUR ISP'S MAIL SERVER HERE!!!
		String host = "smtpserver.yourisp.invalid";

		// Create properties for the Session
		Properties props = new Properties();

		// If using static Transport.send(),
		// need to specify the mail server here
		props.put("mail.smtp.host", host);
		// To see what is going on behind the scene
		props.put("mail.debug", "true");

		// Get a session
		Session session = Session.getInstance(props);

		try
		{
			// Get a Transport object to send e-mail
			Transport bus = session.getTransport("smtp");

			// Connect only once here
			// Transport.send() disconnects after each send
			// Usually, no username and password is required for SMTP
			bus.connect();
			// bus.connect("smtpserver.yourisp.net", "username", "password");

			// Instantiate a message
			Message msg = new MimeMessage(session);

			// Set message attributes
			msg.setFrom(new InternetAddress(from));
			InternetAddress[] address = { new InternetAddress(to) };
			msg.setRecipients(Message.RecipientType.TO, address);
			// Parse a comma-separated list of email addresses. Be strict.
			msg.setRecipients(Message.RecipientType.CC, InternetAddress.parse(to, true));
			// Parse comma/space-separated list. Cut some slack.
			msg.setRecipients(Message.RecipientType.BCC, InternetAddress.parse(to, false));

			msg.setSubject("Test E-Mail through Java");
			msg.setSentDate(new Date());

			// Set message content and send
			setTextContent(msg);
			msg.saveChanges();
			bus.sendMessage(msg, address);

			setMultipartContent(msg);
			msg.saveChanges();
			bus.sendMessage(msg, address);

			setFileAsAttachment(msg, "C:/WINDOWS/CLOUD.GIF");
			msg.saveChanges();
			bus.sendMessage(msg, address);

			setHTMLContent(msg);
			msg.saveChanges();
			bus.sendMessage(msg, address);

			bus.close();

		}
		catch (MessagingException mex)
		{
			// Prints all nested (chained) exceptions as well
			mex.printStackTrace();
			// How to access nested exceptions
			while (mex.getNextException() != null)
			{
				// Get next exception in chain
				Exception ex = mex.getNextException();
				ex.printStackTrace();
				if (!(ex instanceof MessagingException))
				{
					break;
				}
				else
				{
					mex = (MessagingException) ex;
				}
			}
		}
	}

	// Set a file as an attachment. Uses JAF FileDataSource.
	public static void setFileAsAttachment(final Message msg, final String filename) throws MessagingException
	{

		// Create and fill first part
		MimeBodyPart p1 = new MimeBodyPart();
		p1.setText("This is part one of a test multipart e-mail." + "The second part is file as an attachment");

		// Create second part
		MimeBodyPart p2 = new MimeBodyPart();

		// Put a file in the second part
		FileDataSource fds = new FileDataSource(filename);
		p2.setDataHandler(new DataHandler(fds));
		p2.setFileName(fds.getName());

		// Create the Multipart. Add BodyParts to it.
		Multipart mp = new MimeMultipart();
		mp.addBodyPart(p1);
		mp.addBodyPart(p2);

		// Set Multipart as the message's content
		msg.setContent(mp);
	}

	// Set a single part html content.
	// Sending data of any type is similar.
	public static void setHTMLContent(final Message msg) throws MessagingException
	{

		String html = "<html><head><title>" + msg.getSubject() + "</title></head><body><h1>" + msg.getSubject() + "</h1><p>This is a test of sending an HTML e-mail" + " through Java.</body></html>";

		// HTMLDataSource is a static nested class
		msg.setDataHandler(new DataHandler(new HTMLDataSource(html)));
	}

	// A simple multipart/mixed e-mail. Both body parts are text/plain.
	public static void setMultipartContent(final Message msg) throws MessagingException
	{
		// Create and fill first part
		MimeBodyPart p1 = new MimeBodyPart();
		p1.setText("This is part one of a test multipart e-mail.");

		// Create and fill second part
		MimeBodyPart p2 = new MimeBodyPart();
		// Here is how to set a charset on textual content
		p2.setText("This is the second part", "us-ascii");

		// Create the Multipart. Add BodyParts to it.
		Multipart mp = new MimeMultipart();
		mp.addBodyPart(p1);
		mp.addBodyPart(p2);

		// Set Multipart as the message's content
		msg.setContent(mp);
	}

	// A simple, single-part text/plain e-mail.
	public static void setTextContent(final Message msg) throws MessagingException
	{
		// Set message content
		String mytxt = "This is a test of sending a " + "plain text e-mail through Java.\n" + "Here is line 2.";
		msg.setText(mytxt);

		// Alternate form
		msg.setContent(mytxt, "text/plain");

	}
}
