/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.accounts;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

/**
 * 
 * SHA-256 encryption.
 * 
 * @author christian.momon@devinsy.fr
 */
public class Password
{
	private String digest;
	private DateTime editionDate;

	/**
	 * 
	 */
	public Password()
	{
		this.digest = null;
		this.editionDate = DateTime.now();
	}

	/**
	 * 
	 */
	public Password(final String password)
	{
		set(password);
	}

	/**
	 * 
	 */
	public Password(final String digest, final DateTime editionDate)
	{
		this.digest = digest;
		this.editionDate = editionDate;
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public boolean check(final String value)
	{
		boolean result;

		result = StringUtils.equals(digest(value, this.editionDate.getMillis()), this.digest);

		//
		return result;
	}

	public String digest()
	{
		return this.digest;
	}

	public DateTime editionDate()
	{
		return this.editionDate;
	}

	/**
	 * 
	 * @param passwordValue
	 * @return
	 */
	public boolean matches(final String passwordValue)
	{
		boolean result;

		if ((this.digest == null) || (passwordValue == null))
		{
			result = false;
		}
		else
		{
			result = StringUtils.equals(this.digest, digest(passwordValue, this.editionDate.getMillis()));
		}

		//
		return result;
	}

	/**
	 * 
	 * @param password
	 */
	public void set(final String password)
	{
		this.editionDate = DateTime.now();
		this.digest = digest(password, this.editionDate.getMillis());
	}

	/**
	 * 
	 */
	public void touch()
	{
		this.editionDate = DateTime.now();
	}

	/**
	 * 
	 * @return
	 */
	public static String digest(final String value, final long time)
	{
		String result;

		if (value == null)
		{
			result = null;
		}
		else
		{
			result = DigestUtils.sha512Hex(time + value);
		}

		//
		return result;
	}
}
