/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.accounts;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class IndexedRoles implements Iterable<Role>
{
	private List<Role> roles;
	private HashMap<Integer, Role> idIndex;
	private HashMap<String, Role> nameIndex;

	/**
	 *
	 */
	public IndexedRoles()
	{
		this.roles = new ArrayList<Role>();
		this.idIndex = new HashMap<Integer, Role>();
		this.nameIndex = new HashMap<String, Role>();
	}

	/**
	 *
	 */
	public IndexedRoles(final int capacity)
	{
		this.roles = new ArrayList<Role>(capacity);
		this.idIndex = new HashMap<Integer, Role>(capacity);
		this.nameIndex = new HashMap<String, Role>(capacity);
	}

	/**
	 * 
	 * @param attribute
	 */
	public void add(final Role role)
	{
		if (role != null)
		{
			this.roles.add(role);
			this.idIndex.put(role.getId(), role);
			this.nameIndex.put(role.getName(), role);
		}
	}

	/**
	 * 
	 * @param source
	 */
	public void addAll(final IndexedRoles source)
	{
		for (Role role : source)
		{
			// this.put(role.getLabel(), role.getValue());
		}
	}

	/**
	 * This method returns a deep copy of the current object.
	 * 
	 * @see java.util.HashMap#clone()
	 */
	@Override
	public IndexedRoles clone()
	{
		IndexedRoles result;

		result = new IndexedRoles();
		// for (Attribute attribute : this.values()) {
		// this.put(attribute.getLabel(), new Attribute(attribute.getLabel(),
		// attribute.getValue()));
		// }

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Iterator<Role> iterator()
	{
		Iterator<Role> result;

		result = this.roles.iterator();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Set<String> labels()
	{
		Set<String> result;

		result = null;

		//
		return result;
	}

	/**
	 * 
	 */
	public void put(final Role role)
	{
	}

	/**
	 * 
	 * @param label
	 * @param value
	 */
	public Attribute put(final String label, final String value)
	{
		Attribute result;

		// result = this.get(label);
		// if (result == null) {
		// result = new Attribute(label, value);
		// this.put(label, result);
		// } else {
		// result.setValue(value);
		// }

		result = null;

		//
		return result;
	}

	/**
	 * 
	 * @param attribute
	 * @param newLabel
	 */
	public void rename(final Role role, final String newName)
	{
	}

	/**
	 * 
	 * @return
	 */
	public List<Role> toList()
	{
		List<Role> result;

		result = new ArrayList<Role>();

		//
		return result;
	}
}
