/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * Copyright 2017      Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.sciboard;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.actilog.Log.Level;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.sciboard.Request;
import org.kinsources.kiwa.website.charter.ErrorView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.util.StringList;
import fr.devinsy.util.xml.XMLTools;

/**
 *
 */
public class Cancel_request_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8988256363597317696L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Cancel_request_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.sciboard.cancel_request", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);

			// Get parameters.
			// ===============
			String requestId = request.getParameter("request_id");
			logger.info("requestId=[{}]", requestId);

			String comment = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("comment")));
			logger.info("comment=[{}]", comment);

			// Use parameters.
			// ===============
			Request query = kiwa.sciboard().getRequestById(requestId);

			//
			if (query == null)
			{
				throw new IllegalArgumentException("Unknown request.");
			}
			else if (query.getStatus() != Request.Status.OPENED)
			{
				throw new IllegalArgumentException("Invalid status.");
			}
			else if (account == null)
			{
				throw new IllegalArgumentException("Permission denied.");
			}
			else if ((account.getId() != query.getContributorId()) && (!kiwa.isWebmaster(account)))
			{
				throw new IllegalArgumentException("Permission denied.");
			}
			else if (StringUtils.isBlank(comment))
			{
				throw new IllegalArgumentException("Motivation is blank.");
			}
			else
			{
				//
				logger.info("CANCEL REQUEST [{}][{}]", requestId);
				kiwa.log(Level.INFO, "events.cancel_request", "[accountId={}][requestId={}]", account.getId(), requestId);

				//
				kiwa.cancelRequest(query, comment);

				//
				notifySciboarders(query, account, locale);

				//
				Redirector.redirect(response, "/sciboard/my_requests.xhtml");
			}
		}
		catch (final IllegalArgumentException exception)
		{
			StringList message = new StringList();

			message.appendln("<p>An error occurre during your dataset submission.</p>");
			message.appendln("<pre>" + exception.getMessage() + "</pre>");

			ErrorView.show(request, response, "Submit dataset error", message);

		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}

	/**
	 * 
	 * @param account
	 * @throws Exception
	 */
	private void notifySciboarders(final Request request, final Account canceller, final Locale locale) throws Exception
	{
		//
		StringList directLink = new StringList();
		directLink.append(kiwa.getWebsiteUrl());
		directLink.append("sciboard/request.xhtml");
		directLink.append("?request_id=").append(request.getId());

		//
		StringList html = new StringList();
		html.appendln("<p>Request cancelled by contributor.</p>");
		html.appendln("<p>");
		html.append("Request id: ").append(request.getId()).appendln("<br/>");
		html.append("Canceller: ").append(StringEscapeUtils.escapeXml(canceller.getFullName())).appendln("<br/>");
		html.append("<br/>");
		html.append("<a href=\"").append(directLink.toString()).appendln("\" class=\"button_second\">Direct link</a><br/>");
		html.appendln("</p>");

		String subject = String.format("Sciboard event: request cancelled (Request ID: %d)", request.getId());
		kiwa.notifySciboarders(canceller, subject, html.toString(), locale);
	}
}

// ////////////////////////////////////////////////////////////////////////
