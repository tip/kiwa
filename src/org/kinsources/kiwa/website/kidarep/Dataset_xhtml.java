/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.kidarep;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Collaborator;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.kidarep.LoadedFileHeader;
import org.kinsources.kiwa.utils.KiwaUtils;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.charter.KiwaCharterView;
import org.kinsources.kiwa.website.kidarep.statisticsviews.DatasetStatisticsView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.kiss4web.SimpleServletDispatcher;
import fr.devinsy.util.StacktraceWriter;
import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Dataset_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8903536311059605390L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Dataset_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/kidarep/dataset.html");
	private static DatasetStatisticsView datasetStatisticsView = new DatasetStatisticsView();
	public static final String EMPTY_FIELD = "-";

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.kidarep.dataset", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);
			Long accountId = Account.getId(account);

			// Get parameters.
			// ===============
			String datasetIdParameter = SimpleServletDispatcher.shortRewritedUrlParameter(request);
			logger.info("datasetId=[{}]", datasetIdParameter);
			Long datasetId;
			if (NumberUtils.isNumber(datasetIdParameter))
			{
				datasetId = Long.parseLong(datasetIdParameter);
			}
			else
			{
				datasetId = null;
			}

			// Use parameters.
			// ===============
			if (datasetId == null)
			{
				throw new IllegalArgumentException("Invalid dataset id");
			}
			else
			{
				Dataset dataset = kiwa.kidarep().getDatasetById(datasetId);

				if (dataset == null)
				{
					throw new IllegalArgumentException("Invalid dataset.");
				}
				else
				{
					//
					TagDataManager data = new TagDataManager();

					if (isNotGrantedAccess(dataset, account))
					{
						data.setContent("dataset", "Permission denied.");
					}
					else
					{
						//
						data.setContent("title_name", XMLTools.escapeXmlBlank(dataset.getName()));

						//
						data.setContent("label_lead_author", "Author:");
						if (StringUtils.isBlank(dataset.getAuthor()))
						{
							data.setContent("lead_author", EMPTY_FIELD);
						}
						else
						{
							data.setContent("lead_author_link", XMLTools.escapeXmlBlank(dataset.getAuthor()));
							data.setAttribute("lead_author_link", "href", "/browser/field_value.xhtml?name=AUTHOR&amp;value=" + XMLTools.escapeXmlBlank(dataset.getAuthor()));
						}

						//
						data.setContent("id", dataset.getId());

						if (dataset.isGenealogy())
						{
							data.setAttribute("dataset_type_icon", "src", "/charter/commons/dataset-genealogy.png");
							data.setAttribute("dataset_type_icon", "title", "Genealogy");
							data.setAttribute("dataset_type_icon", "alt", "Genealogy Type");
							data.setAttribute("dataset_type", "sorttable_customkey", "Genealogy");
						}
						else
						{
							data.setAttribute("dataset_type_icon", "src", "/charter/commons/dataset-terminology.png");
							data.setAttribute("dataset_type_icon", "title", "Terminology");
							data.setAttribute("dataset_type_icon", "alt", "Terminology Type");
							data.setAttribute("dataset_type", "sorttable_customkey", "Terminology");
						}

						//
						data.setContent("label_status", "Status:");
						String statusIcon;
						switch (dataset.getStatus())
						{
							case CREATED:
								statusIcon = "/charter/commons/created.png";
							break;
							case SUBMITTED:
								statusIcon = "/charter/commons/submitted.png";
							break;
							case VALIDATED:
								statusIcon = "/charter/commons/validated.png";
							break;
							default:
								statusIcon = "";
						}
						data.setAttribute("status", "src", statusIcon);
						data.setAttribute("status", "title", dataset.getStatus().toString());

						//
						data.setContent("label_language_group", "Language group:");
						if (StringUtils.isBlank(dataset.getLanguageGroup()))
						{
							data.setContent("language_group", EMPTY_FIELD);
						}
						else
						{
							data.setContent("language_group_link", XMLTools.escapeXmlBlank(dataset.getLanguageGroup()));
							data.setAttribute("language_group_link", "href", "/browser/field_value.xhtml?name=LANGUAGE_GROUP&amp;value=" + XMLTools.escapeXmlBlank(dataset.getLanguageGroup()));
						}

						//
						data.setContent("label_lead_license", "License:");
						if (StringUtils.isBlank(dataset.getLicense()))
						{
							data.setContent("lead_license", EMPTY_FIELD);
						}
						else
						{
							data.setContent("lead_license", KiwaUtils.htmlize(XMLTools.escapeXmlBlank(dataset.getLicense())));
						}

						//
						data.setContent("label_license", "Conditions of use:");
						if (StringUtils.isBlank(dataset.getLicense()))
						{
							data.setContent("license", EMPTY_FIELD);
						}
						else
						{
							data.setContent("license", StringUtils.defaultIfBlank(KiwaUtils.htmlize(XMLTools.escapeXmlBlank(dataset.getLicense())), EMPTY_FIELD));
						}

						//
						data.setContent("label_permanent_link", "Permanent link:");
						if (StringUtils.isBlank(dataset.getLicense()))
						{
							data.setContent("permanent_link", EMPTY_FIELD);
						}
						else
						{
							data.setContent("permanent_link", KiwaUtils.htmlize(XMLTools.escapeXmlBlank(kiwa.permanentURI(dataset))));
						}

						//
						data.setContent("label_name", "Name:");
						data.setContent("name", XMLTools.escapeXmlBlank(dataset.getName()));
						data.setAttribute("name", "title", "Technical name: " + dataset.simplifiedName());

						//
						data.setContent("label_author", "Author:");
						if (StringUtils.isBlank(dataset.getAuthor()))
						{
							data.setContent("author", EMPTY_FIELD);
						}
						else
						{
							data.setContent("author_link", XMLTools.escapeXmlBlank(dataset.getAuthor()));
							data.setAttribute("author_link", "href", "/browser/field_value.xhtml?name=AUTHOR&amp;value=" + XMLTools.escapeXmlBlank(dataset.getAuthor()));
						}

						data.setContent("label_simplified_name", "Simplified name:");
						data.setContent("simplified_name", dataset.simplifiedName());

						data.setContent("label_file_base_name", "Base file name:");
						data.setContent("file_base_name", dataset.baseFileName());

						data.setContent("label_contributor", "Contributor:");
						data.setContent("contributor", XMLTools.escapeXmlBlank(dataset.getContributor().getFullName()));
						data.setAttribute("contributor", "href", "/browser/contributor.xhtml?contributor_id=" + dataset.getContributor().getId());

						data.setContent("label_creation_date", "Creation date:");
						data.setContent("creation_date", dataset.getCreationDate().toString("dd/MM/YYYY"));

						data.setContent("label_edition_date", "Edition date:");
						data.setContent("edition_date", dataset.getEditionDate().toString("dd/MM/YYYY"));

						if (dataset.getStatus() == Dataset.Status.CREATED)
						{
							data.setContent("label_lead_date", "Creation date:");
							if (dataset.getCreationDate() == null)
							{
								data.setContent("lead_date", EMPTY_FIELD);
							}
							else
							{
								data.setContent("lead_date", dataset.getCreationDate().toString("dd/MM/YYYY"));
							}
						}
						else if (dataset.getStatus() == Dataset.Status.SUBMITTED)
						{
							data.setContent("label_lead_date", "Submission date:");
							if (dataset.getSubmissionDate() == null)
							{
								data.setContent("lead_date", EMPTY_FIELD);
							}
							else
							{
								data.setContent("lead_date", dataset.getSubmissionDate().toString("dd/MM/YYYY"));
							}
						}
						else
						{
							data.setContent("label_lead_date", "Publication date:");
							if (dataset.getPublicationDate() == null)
							{
								data.setContent("lead_date", EMPTY_FIELD);
							}
							else
							{
								data.setContent("lead_date", dataset.getPublicationDate().toString("dd/MM/YYYY"));
							}
						}

						data.setContent("label_atlas_code", "Atlas code:");
						if (StringUtils.isBlank(dataset.getAtlasCode()))
						{
							data.setContent("atlas_code", EMPTY_FIELD);
						}
						else
						{
							data.setContent("atlas_code_link", XMLTools.escapeXmlBlank(dataset.getAtlasCode()));
							data.setAttribute("atlas_code_link", "href", "/browser/field_value.xhtml?name=ATLAS_CODE&amp;value=" + XMLTools.escapeXmlBlank(dataset.getAtlasCode()));
						}

						data.setContent("label_bibliography", "Bibliography:");
						data.setContent("bibliography", StringUtils.defaultIfBlank(KiwaUtils.htmlize(XMLTools.escapeXmlBlank(dataset.getBibliography())), EMPTY_FIELD));

						data.setContent("label_citation", "Citation:");
						data.setContent("citation", StringUtils.defaultIfBlank(KiwaUtils.htmlize(XMLTools.escapeXmlBlank(dataset.getCitation())), EMPTY_FIELD));

						//
						data.setContent("label_how_to_cite", "How to cite:");
						data.setContent("how_to_cite", StringUtils.defaultIfBlank(KiwaUtils.htmlize(XMLTools.escapeXmlBlank(kiwa.howToCite(dataset))), EMPTY_FIELD));

						//
						data.setContent("label_coder", "Coder:");
						if (StringUtils.isBlank(dataset.getCoder()))
						{
							data.setContent("coder", EMPTY_FIELD);
						}
						else
						{
							data.setContent("coder_link", XMLTools.escapeXmlBlank(dataset.getCoder()));
							data.setAttribute("coder_link", "href", "/browser/field_value.xhtml?name=CODER&amp;value=" + XMLTools.escapeXmlBlank(dataset.getCoder()));
						}

						data.setContent("label_collection_notes", "Collection note:<br/>(circumstances, sources, etc.):");
						data.setContent("label_collection_notes", "Collection notes<br/>(circumstances, sources, methods, funding, coverage…):");
						if (dataset.getCollectionNotes() == null)
						{
							data.setContent("collection_notes", EMPTY_FIELD);
						}
						else
						{
							data.setContent("collection_notes", KiwaUtils.htmlize(XMLTools.escapeXmlBlank(dataset.getCollectionNotes()).replace("\n", "<br/>")));
						}

						data.setContent("label_contact", "Contact:");
						data.setContent("contact", StringUtils.defaultIfBlank(KiwaUtils.htmlize(XMLTools.escapeXmlBlank(dataset.getContact())), EMPTY_FIELD));

						data.setContent("label_country", "Country:");
						if (StringUtils.isBlank(dataset.getCountry()))
						{
							data.setContent("country", EMPTY_FIELD);
						}
						else
						{
							data.setContent("country_link", XMLTools.escapeXmlBlank(dataset.getCountry()));
							data.setAttribute("country_link", "href", "/browser/field_value.xhtml?name=COUNTRY&amp;value=" + XMLTools.escapeXmlBlank(dataset.getCountry()));
						}

						data.setContent("label_description", "Description:");
						data.setContent("description", StringUtils.defaultIfBlank(KiwaUtils.htmlize(XMLTools.escapeXmlBlank(dataset.getDescription())), EMPTY_FIELD));

						data.setContent("label_ethnic_or_cultural_group", "Ethnic or cultural group:");
						data.setContent("ethnic_or_cultural_group", StringUtils.defaultIfBlank(XMLTools.escapeXmlBlank(dataset.getEthnicOrCulturalGroup()), EMPTY_FIELD));

						data.setContent("label_geographic_coordinate", "Coordinate:");
						data.setAttribute("label_geographic_coordinate", "title", " (Latitude/Longitude of centre)");
						data.setContent("geographic_coordinate", StringUtils.defaultIfBlank(XMLTools.escapeXmlBlank(dataset.getGeographicCoordinate()), EMPTY_FIELD));

						data.setContent("label_history", "Dataset history:");
						data.setContent("history", StringUtils.defaultIfBlank(KiwaUtils.htmlize(XMLTools.escapeXmlBlank(dataset.getHistory())), EMPTY_FIELD));

						data.setContent("label_other_repositories", "Other repositories:");
						data.setContent("other_repositories", StringUtils.defaultIfBlank(KiwaUtils.htmlize(XMLTools.escapeXmlBlank(dataset.getOtherRepositories())), EMPTY_FIELD));

						data.setContent("label_period", "Period covered by the dataset:");
						data.setContent("period", StringUtils.defaultIfBlank(XMLTools.escapeXmlBlank(dataset.getPeriod()), EMPTY_FIELD));

						data.setContent("label_period_note", "Period note:");
						data.setContent("period_note", StringUtils.defaultIfBlank(XMLTools.escapeXmlBlank(dataset.getPeriodNote()), EMPTY_FIELD));

						data.setContent("label_coverage", "Period of data collection:");
						data.setContent("coverage", StringUtils.defaultIfBlank(XMLTools.escapeXmlBlank(dataset.getCoverage()), EMPTY_FIELD));

						data.setContent("label_radius_from_center", "Radius from center (km):");
						if (dataset.getRadiusFromCenter() == null)
						{
							data.setContent("radius_from_center", EMPTY_FIELD);
						}
						else
						{
							data.setContent("radius_from_center", dataset.getRadiusFromCenter());
						}

						data.setContent("label_reference", "Reference:");
						data.setContent("reference", StringUtils.defaultIfBlank(KiwaUtils.htmlize(XMLTools.escapeXmlBlank(dataset.getReference())), EMPTY_FIELD));

						data.setContent("label_continent", "Continent:");
						if (StringUtils.isBlank(dataset.getContinent()))
						{
							data.setContent("continent", EMPTY_FIELD);
						}
						else
						{
							data.setContent("continent_link", XMLTools.escapeXmlBlank(dataset.getContinent()));
							data.setAttribute("continent_link", "href", "/browser/field_value.xhtml?name=CONTINENT&amp;value=" + XMLTools.escapeXmlBlank(dataset.getContinent()));
						}

						data.setContent("label_region", "Region:");
						if (StringUtils.isBlank(dataset.getRegion()))
						{
							data.setContent("region", EMPTY_FIELD);
						}
						else
						{
							data.setContent("region_link", XMLTools.escapeXmlBlank(dataset.getRegion()));
							data.setAttribute("region_link", "href", "/browser/field_value.xhtml?name=REGION&amp;value=" + XMLTools.escapeXmlBlank(dataset.getRegion()));
						}

						data.setContent("label_location", "Location:");
						if (StringUtils.isBlank(dataset.getLocation()))
						{
							data.setContent("location", EMPTY_FIELD);
						}
						else
						{
							data.setContent("location_link", XMLTools.escapeXmlBlank(dataset.getLocation()));
							data.setAttribute("location_link", "href", "/browser/field_value.xhtml?name=LOCATION&amp;value=" + XMLTools.escapeXmlBlank(dataset.getLocation()));
						}

						data.setContent("label_short_description", "Short description:");
						data.setContent("short_description", StringUtils.defaultIfBlank(XMLTools.escapeXmlBlank(dataset.getShortDescription()), EMPTY_FIELD));

						// Bubbles.
						data.setContent("bubble.dataset.submit_button", kiwa.hico().getBubbleText("bubble.dataset.submit_button", locale));
						data.setContent("bubble.dataset.ead_button", kiwa.hico().getBubbleText("bubble.dataset.ead_button", locale));
						data.setContent("bubble.dataset.dublin_core_button", kiwa.hico().getBubbleText("bubble.dataset.dublin_core_button", locale));
						data.setContent("bubble.dataset.upload_file_button", kiwa.hico().getBubbleText("bubble.dataset.upload_file_button", locale));
						data.setContent("bubble.dataset.upload_attachment_button", kiwa.hico().getBubbleText("bubble.dataset.upload_attachment_button", locale));

						// Dataset edition button.
						if ((dataset.isContributor(account)) || ((dataset.getStatus() == Dataset.Status.SUBMITTED) && (kiwa.isSciboarder(account))))
						{
							data.setContent("dataset_edition_button", "Edit");
							data.setAttribute("dataset_edition_button", "href", "dataset_edition.xhtml?dataset_id=" + dataset.getId());
						}
						else if (kiwa.isWebmaster(account))
						{
							data.setContent("dataset_edition_button", "Edit");
							data.setAttribute("dataset_edition_button", "href", "dataset_edition.xhtml?dataset_id=" + dataset.getId());
							data.setAttribute("dataset_edition_button", "class", "button_webmaster");
						}
						else
						{
							data.setContent("dataset_edition", "");
						}

						// Remove dataset button.
						if ((dataset.getStatus() == Dataset.Status.CREATED) && (dataset.isContributor(account)))
						{
							data.setContent("remove_dataset_button", "Remove");
							data.setAttribute("remove_dataset_button", "href", "dataset_remove.xhtml?dataset_id=" + dataset.getId());
						}
						else if ((dataset.getStatus() == Dataset.Status.CREATED) && (kiwa.isWebmaster(account)))
						{
							data.setContent("remove_dataset_button", "Remove");
							data.setAttribute("remove_dataset_button", "href", "dataset_remove.xhtml?dataset_id=" + dataset.getId());
							data.setAttribute("remove_dataset_button", "class", "button_webmaster");
						}
						else
						{
							data.setContent("remove_dataset", "");
						}

						// Download EAD button.
						if (kiwa.isWebmaster(account))
						{
							data.setContent("download_ead_button", "Download EAD");
							data.setAttribute("download_ead_button", "href", "download_ead.xhtml?dataset_id=" + dataset.getId());
						}
						else
						{
							data.setContent("download_ead", "");
						}

						// Download Dublin Core button.
						if (kiwa.isWebmaster(account))
						{
							data.setContent("download_dublin_core_button", "Download Dublin Core");
							data.setAttribute("download_dublin_core_button", "href", "download_dublin_core.xhtml?dataset_id=" + dataset.getId());
						}
						else
						{
							data.setContent("download_dublin_core", "");
						}

						// Transfer button.
						if ((dataset.getStatus() == Dataset.Status.VALIDATED) && (dataset.isContributor(account)))
						{
							data.setContent("transfer_dataset_button", "Transfer");
							data.setAttribute("transfer_dataset_button", "href", "dataset_transfer.xhtml?dataset_id=" + dataset.getId());
						}
						else if ((dataset.getStatus() == Dataset.Status.VALIDATED) && (kiwa.isWebmaster(account)))
						{
							data.setContent("transfer_dataset_button", "Transfer");
							data.setAttribute("transfer_dataset_button", "href", "dataset_transfer.xhtml?dataset_id=" + dataset.getId());
							data.setAttribute("transfer_dataset_button", "class", "button_webmaster");
						}
						else
						{
							data.setContent("transfer_dataset", "");
						}

						// Submit button.
						if ((dataset.getStatus() == Dataset.Status.CREATED) && (dataset.isContributor(account)))
						{
							data.setContent("submit_dataset_button", "Submit to publish");
							data.setAttribute("submit_dataset_button", "href", "/sciboard/dataset_submit.xhtml?dataset_id=" + dataset.getId());
						}
						else if ((dataset.getStatus() == Dataset.Status.CREATED) && (kiwa.isWebmaster(account)))
						{
							data.setContent("submit_dataset_button", "Submit to publish");
							data.setAttribute("submit_dataset_button", "href", "/sciboard/dataset_submit.xhtml?dataset_id=" + dataset.getId());
							data.setAttribute("submit_dataset_button", "class", "button_webmaster");
						}
						else
						{
							data.setContent("submit_dataset", "");
						}

						// Unpublish button.
						if ((dataset.getStatus() == Dataset.Status.VALIDATED) && (Account.isActivated(account)))
						{
							data.setContent("unpublish_dataset_button", "Unpublish");
							data.setAttribute("unpublish_dataset_button", "href", "dataset_unpublish.xhtml?dataset_id=" + dataset.getId());
							data.setContent("bubble.dataset.unpublish_dataset", "Send an unpublication request to the scientific board.");
						}
						else
						{
							data.setContent("unpublish_dataset", "");
						}

						// Origin file.
						if (dataset.isContributor(account))
						{
							if (dataset.getOriginFile() == null)
							{
								data.setContent("origin_file", "No dataset file.");
							}
							else
							{
								data.setContent("origin_file_link", dataset.getOriginFile().name());
								data.setAttribute("origin_file_link", "href",
										SimpleServletDispatcher.rewriteLongUrl("dataset_origin_file", String.valueOf(dataset.getId()), dataset.getOriginFile().name()));
								data.setContent("origin_file_length", KiwaUtils.formatByteSize(dataset.getOriginFile().length()));
							}
						}
						else if ((kiwa.isWebmaster(account)))
						{
							if (dataset.getOriginFile() == null)
							{
								data.setContent("origin_file", "No dataset file.");
							}
							else
							{
								data.setContent("origin_file_name", dataset.getOriginFile().name());
								data.setContent("origin_file_length", KiwaUtils.formatByteSize(dataset.getOriginFile().length()));
							}
						}
						else
						{
							data.setContent("origin_file", "");
							data.setAttribute("origin_file", "style", "display: none;");
						}

						// Public download count.
						if ((dataset.getPublicFile() == null) || ((!dataset.isContributor(account)) && (!kiwa.isWebmaster(account)) && (!kiwa.isSciboarder(account))))
						{
							data.setAttribute("public_download_count_section", "style", "display: none;");
						}
						else
						{
							data.setContent("public_download_count_label", "Public download count:");
							data.setContent("public_download_count", dataset.getPublicFile().getDownloadCount());
						}

						// Public file download.
						if ((dataset.getPublicFile() == null) || ((!dataset.isContributor(account)) && (!kiwa.isWebmaster(account))))
						{
							data.setContent("public_file", "");
							data.setAttribute("public_file", "style", "display: none;");
						}
						else
						{
							data.setAttribute("public_file_download", "href",
									SimpleServletDispatcher.rewriteLongUrl("/kidarep/dataset_public_file", String.valueOf(dataset.getId()), dataset.publicFileName()));
							data.setAttribute("public_file_download", "title", "Download the public file");
						}

						// Remove dataset file button.
						if ((dataset.getStatus() == Dataset.Status.CREATED) && (dataset.getOriginFile() != null) && ((dataset.isContributor(account)) || (kiwa.isWebmaster(account))))
						{
							data.setAttribute("remove_dataset_file_button", "title", "Remove");
							data.setAttribute("remove_dataset_file_button", "href", "remove_dataset_file.xhtml?dataset_id=" + dataset.getId());
						}
						else
						{
							data.setContent("remove_dataset_file", "");
						}

						// Upload dataset file button.
						if ((dataset.getStatus() == Dataset.Status.CREATED) && ((dataset.isContributor(account)) || (kiwa.isWebmaster(account))))
						{
							if (dataset.getOriginFile() == null)
							{
								data.setContent("upload_dataset_file_button", "Upload dataset file");
							}
							else
							{
								data.setContent("upload_dataset_file_button", "Replace dataset file");
							}
							data.setAttribute("upload_dataset_file_button", "href", "dataset_file_upload.xhtml?dataset_id=" + dataset.getId());
						}
						else
						{
							data.setContent("upload_dataset_file", "");
						}

						// Download file button.
						if (dataset.getOriginFile() == null)
						{
							data.setContent("download_dataset_file", "");
						}
						else
						{
							data.setContent("download_dataset_file_button", "Download");

							if (dataset.isGenealogy())
							{
								data.setAttribute("download_dataset_file_button", "href", "dataset_file_download.xhtml?dataset_id=" + dataset.getId());
							}
							else
							{
								data.setAttribute("download_dataset_file_button", "href",
										SimpleServletDispatcher.rewriteLongUrl("/kidarep/dataset_public_file", String.valueOf(dataset.getId()), dataset.publicFileName()));
							}
						}

						// Attachment files.
						data.setContent("attachments_title", "Attachments (" + dataset.attachments().countOfLoadedFiles() + "/5)");
						if (dataset.attachments().isEmpty())
						{
							data.setContent("attachments", "No attachment.");
						}
						else
						{
							int attachmentIndex = 0;
							for (LoadedFileHeader attachment : dataset.attachments())
							{
								//
								data.setContent("attachment", attachmentIndex, "attachment_link", attachment.name());
								data.setAttribute("attachment", attachmentIndex, "attachment_link", "href",
										SimpleServletDispatcher.rewriteLongUrl("/kidarep/dataset_attachment", String.valueOf(dataset.getId()), String.valueOf(attachment.id()), attachment.name()));
								data.setContent("attachment", attachmentIndex, "attachment_length", KiwaUtils.formatByteSize(attachment.length()));

								//
								if (((dataset.getStatus() == Dataset.Status.CREATED) || (dataset.getStatus() == Dataset.Status.SUBMITTED))
										&& ((dataset.isContributor(account)) || (kiwa.isWebmaster(account))))
								{
									data.setAttribute("attachment", attachmentIndex, "attachment_remove_link", "title", "Remove");
									data.setAttribute("attachment", attachmentIndex, "attachment_remove_link", "href", "remove_dataset_attachment.xhtml?dataset_id=" + dataset.getId()
											+ "&amp;attachment_id=" + attachment.id());
								}
								else
								{
									data.setAttribute("attachment", attachmentIndex, "attachment_remove", "class", "xid:nodisplay");
								}

								//
								attachmentIndex += 1;
							}
						}

						if ((dataset.attachments().size() < 5) && ((dataset.getStatus() == Dataset.Status.CREATED) || (dataset.getStatus() == Dataset.Status.SUBMITTED))
								&& ((dataset.isContributor(account)) || (kiwa.isWebmaster(account))))
						{
							data.setAttribute("upload_attachment_dataset_id", "value", dataset.getId());
						}
						else
						{
							data.setAttribute("upload_attachment", "class", "xid:nodisplay");
						}

						// Collaborators
						// =============
						if ((dataset.getStatus() != Dataset.Status.VALIDATED) || ((!dataset.isContributor(account)) && (!kiwa.isWebmaster(account))))
						{
							data.setContent("collaborators_section", "");
						}
						else
						{
							//
							data.setContent("collaborators_title", String.format("Collaborators (%d)", dataset.collaborators().size()));

							//
							if (dataset.isGenealogy())
							{
								data.setContent("add_collaborator", "ADD…");
								data.setAttribute("add_collaborator", "href", "/kidarep/collaborator_creation.xhtml?dataset_id=" + dataset.getId());
							}
							else
							{
								data.setContent("collaborator_add", "");
							}

							//
							if (dataset.collaborators().isEmpty())
							{
								data.setContent("collaborator_line", "<td colspan=\"10\">No collaborator.</td>");
							}
							else
							{
								int index = 0;
								for (Collaborator collaborator : dataset.collaborators().sortByLastName())
								{
									//
									data.setContent("collaborator_line", index, "collaborator_name", XMLTools.escapeXmlBlank(collaborator.getAccount().getFullName()));
									data.setAttribute("collaborator_line", index, "collaborator_name", "href", "/kidarep/collaborator_edition.xhtml?dataset_id=" + dataset.getId()
											+ "&amp;collaborator_id=" + collaborator.getId());
									data.setContent("collaborator_line", index, "collaborator_description", XMLTools.escapeXmlBlank(collaborator.getDescription()));
									data.setAttribute("collaborator_line", index, "collaborator_description", "title", collaborator.filters().toString());
									data.setAttribute("collaborator_line", index, "collaborator_remove", "href", "/kidarep/remove_collaborator.xhtml?dataset_id=" + dataset.getId()
											+ "&amp;collaborator_id=" + collaborator.getId());
									data.setAttribute("collaborator_line", index, "collaborator_remove", "title", "Remove this collaborator");
									data.setAttribute("collaborator_line", index, "collaborator_download", "href", SimpleServletDispatcher.rewriteLongUrl("/kidarep/dataset_collaborator_file",
											String.valueOf(dataset.getId()), String.valueOf(collaborator.getId()), collaborator.getFile().name()));
									data.setAttribute("collaborator_line", index, "collaborator_download", "title", "Download the collaborator file");

									//
									index += 1;
								}
							}
						}

						// Statistics values
						// =================
						data.setContent("statistics_view", datasetStatisticsView.getHtmlBody(dataset, account, locale));
					}

					// Send response.
					// ==============
					StringBuffer content = xidyn.dynamize(data);

					//
					StringBuffer page = kiwa.getCharterView().getHtml(KiwaCharterView.Menu.BROWSER, accountId, locale, content);

					//
					response.setContentType("application/xhtml+xml; charset=UTF-8");
					response.getWriter().println(page);
				}
			}

		}
		catch (final IllegalArgumentException exception)
		{
			logger.error(StacktraceWriter.toString(exception));

			Redirector.redirect(response, "/");
		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}

	/**
	 * 
	 * @param dataset
	 * @param account
	 * @return
	 */
	public static boolean isGrantedAccess(final Dataset dataset, final Account account)
	{
		boolean result;

		if ((dataset.getStatus() == Dataset.Status.VALIDATED) || (dataset.isContributor(account) || (Kiwa.instance().isWebmaster(account))))
		{
			result = true;
		}
		else if ((Kiwa.instance().isSciboarder(account)) && (dataset.getStatus() == Dataset.Status.SUBMITTED))
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @param dataset
	 * @param account
	 * @return
	 */
	public static boolean isNotGrantedAccess(final Dataset dataset, final Account account)
	{
		boolean result;

		result = !isGrantedAccess(dataset, account);

		//
		return result;
	}
}

// ////////////////////////////////////////////////////////////////////////
