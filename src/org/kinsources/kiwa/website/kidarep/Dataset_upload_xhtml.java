/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.kidarep;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.utils.ISO_3166_1_alpha_2;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.charter.KiwaCharterView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Dataset_upload_xhtml extends HttpServlet
{
	private static final long serialVersionUID = 3523141827401383261L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Dataset_upload_xhtml.class);
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/kidarep/dataset_upload.html");
	private static final Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.my_space.dataset_upload", request);

			// Get parameters.
			// ===============
			Locale locale = kiwa.getUserLocale(request);
			Long accountId = kiwa.getAuthentifiedAccountId(request, response);

			// Use parameters.
			// ===============

			// Send response.
			// ==============
			if (accountId == null)
			{
				Redirector.redirect(response, "not_logged_upload.xhtml");
			}
			else
			{
				//
				TagDataManager data = new TagDataManager();

				//
				data.setContent("label_name", "Name:");
				data.setContent("label_atlas_code", "Atlas code:");
				data.setContent("label_author", "Author (for citation):");
				data.setContent("label_bibliography", "Bibliography:");
				data.setContent("label_citation", "Citation<br/>(How to cite in publication):");
				data.setContent("label_coder", "Coder:");
				data.setContent("label_collection_notes", "Collection note<br/>(circumstances, sources, etc.):");
				data.setContent("label_contact", "Contact:");
				data.setContent("label_continent", "Continent:");
				data.setContent("label_country", "Country:");
				data.setContent("label_coverage", "Coverage:");
				data.setContent("label_description", "Description:");
				data.setContent("label_ethnic_or_cultural_group", "Ethnic or cultural group:");
				data.setContent("label_geographic_coordinate", "Geographic coordinate: <br/>(Latitude/Longitude of centre)");
				data.setContent("label_history", "Dataset history:");
				data.setContent("label_license", "Conditions of use and license:");
				data.setContent("label_location", "Location<br/> (maximal precision):");
				data.setContent("label_owner", "Owner:");
				data.setContent("label_other_repositories", "Other repositories:");
				data.setContent("label_period", "Period (from/to):");
				data.setContent("label_period_note", "Period note:");
				data.setContent("label_radius_from_center", "Radius from center (km):");
				data.setContent("label_reference", "Reference:");
				data.setContent("label_region", "Region:");
				data.setContent("label_short_description", "Short description:");

				//
				kiwa.hico().generateBubble(data, "bubble.dataset.atlas_code", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.author", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.bibliography", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.citation", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.coder", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.collection_notes", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.contact", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.continent", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.country", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.coverage", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.description", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.ethnic_or_cultural_group", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.geographic_coordinate", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.history", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.license", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.location", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.name", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.owner", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.other_repositories", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.period", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.period_note", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.radius_from_center", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.reference", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.region", locale);
				kiwa.hico().generateBubble(data, "bubble.dataset.short_description", locale);

				//
				int countryIndex = 0;
				for (String code : ISO_3166_1_alpha_2.codes())
				{
					//
					data.setContent("country_option", countryIndex, ISO_3166_1_alpha_2.getLabel(code));
					data.setAttribute("country_option", countryIndex, "value", ISO_3166_1_alpha_2.getLabel(code));

					//
					countryIndex += 1;
				}

				//
				int continentIndex = 0;
				for (String continent : kiwa.stag().getContinentNames().sort())
				{
					//
					data.setContent("continent_option", continentIndex, continent);
					data.setAttribute("continent_option", continentIndex, "value", continent);

					//
					continentIndex += 1;
				}

				//
				StringBuffer content = xidyn.dynamize(data);

				//
				StringBuffer html = kiwa.getCharterView().getHtml(KiwaCharterView.Menu.MY_SPACE, accountId, locale, content);

				// Display page.
				response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.getWriter().println(html);
			}
		}
		catch (Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
