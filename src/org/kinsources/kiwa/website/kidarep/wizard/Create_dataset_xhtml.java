/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.kidarep.wizard;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.actilog.Log.Level;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.website.charter.ErrorView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.util.StringList;
import fr.devinsy.util.xml.XMLTools;

/**
 *
 */
public class Create_dataset_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8988256363597317696L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Create_dataset_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			// Get parameters.
			// ===============
			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);

			//
			kiwa.logPageHit("pages.my_space.wizard.create_dataset", request);

			//
			if (account == null)
			{
				Redirector.redirect(response, "not_logged_upload.xhtml");
			}
			else
			{
				String datasetId = request.getParameter("dataset_id");
				logger.info("datasetId=[{}]", datasetId);

				String type = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("type")));
				logger.info("type=[{}]", type);

				String name = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("name")));
				logger.info("name=[{}]", name);

				String author = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("author")));
				logger.info("author=[{}]", author);

				String shortDescription = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("short_description")));
				logger.info("shortDescription=[{}]", shortDescription);

				// Use parameters.
				// ===============
				if (StringUtils.isBlank(name) || StringUtils.isBlank(author) || StringUtils.isBlank(shortDescription))
				{
					//
					String message = new String("");
					if (StringUtils.isBlank(name))
					{
						message = message.concat("Name is blank.<br />");
					}

					if (StringUtils.isBlank(author))
					{
						message = message.concat("Author is blank.<br />");
					}

					if (StringUtils.isBlank(shortDescription))
					{
						message = message.concat("Short description is blank.<br />");
					}

					//
					throw new IllegalArgumentException(message);

				}
				else if (kiwa.kidarep().isNotAvailableDatasetName(name, null))
				{
					throw new IllegalArgumentException("The technical name generated from this name is already in use.");
				}
				else
				{
					//
					logger.info("CREATE DATASET [{}]", name);
					kiwa.log(Level.INFO, "events.wizard.upload_dataset", "[accountId={}][name={}][name={}]", account.getId(), name, type);

					//
					Dataset dataset = kiwa.createDataset(kiwa.kidarep().getContributorById(account.getId()), name);

					if (StringUtils.equals(type, "GENEALOGY"))
					{
						dataset.setType(Dataset.Type.GENEALOGY);
					}
					else
					{
						dataset.setType(Dataset.Type.TERMINOLOGY);
					}
					dataset.setAuthor(author);
					if (shortDescription == null)
					{
						dataset.setShortDescription(null);
					}
					else
					{
						dataset.setShortDescription(shortDescription.replace("\n", " "));
					}

					//
					kiwa.updateDataset(dataset);

					//
					Redirector.redirect(response, "dataset_creation.xhtml?dataset_id=" + dataset.getId());
				}
			}
		}
		catch (final IllegalArgumentException exception)
		{
			StringList message = new StringList();

			message.appendln("<p>Your dataset form is incomplete.</p>");
			message.appendln("<p>Please, fix the following items:</p>");
			message.appendln("<pre>" + exception.getMessage() + "</pre>");

			ErrorView.show(request, response, "Dataset upload error", message, "/kidarep/dataset_upload.xhtml");

		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
