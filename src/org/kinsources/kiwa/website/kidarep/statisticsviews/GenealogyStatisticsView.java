/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.kidarep.statisticsviews;

import java.util.Locale;

import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.kidarep.DatasetFile;
import org.kinsources.kiwa.kidarep.Statistics;
import org.kinsources.kiwa.stag.Stag;
import org.kinsources.kiwa.stag.StatisticTag;

import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;
import fr.devinsy.xidyn.utils.XidynUtils;

/**
 *
 */
public class GenealogyStatisticsView
{
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(GenealogyStatisticsView.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static URLPresenter view = new URLPresenter("/org/kinsources/kiwa/website/kidarep/statisticsviews/genealogy_statistics_view.html");
	private static AttributesView attributesView = new AttributesView();

	/**
	 * 
	 */
	public GenealogyStatisticsView()
	{
	}

	/**
	 * {@inheritDoc}
	 */
	public StringBuffer getHtml(final Dataset dataset, final Account account, final Locale locale) throws Exception
	{
		StringBuffer result;

		//
		TagDataManager data = new TagDataManager();

		//
		logger.debug("locale=[{}]", locale);

		// Statistics values
		// =================
		DatasetFile sourceFile = kiwa.getDatasetFile(dataset, account);

		if ((sourceFile == null) || (sourceFile.statistics().isEmpty()))
		{
			data.setAttribute("statistics_iurg", "style", "display: none");
			data.setContent("statistics_data", "No statistics.");
			data.setAttribute("raw_statistics_button", "class", "xid:nodisplay");
		}
		else
		{
			//
			Statistics statistics = sourceFile.statistics();

			//
			data.setContent("iurg_individual_count", statistics.getStringValue(StatisticTag.NUM_OF_INDIVIDUALS.name()));
			data.setContent("iurg_union_count", statistics.getStringValue(StatisticTag.NUM_OF_UNIONS.name()));
			data.setContent("iurg_relation_count", statistics.getStringValue(StatisticTag.NUM_OF_RELATIONS.name()));
			data.setContent("iurg_generation_count", statistics.getStringValue(StatisticTag.DEPTH.name()));

			data.setContent("individual_count", statistics.getStringValue(StatisticTag.NUM_OF_INDIVIDUALS.name()));

			data.setContent("men_count", statistics.getStringValue(StatisticTag.NUM_OF_MEN.name()));
			data.setContent("men_rate", statistics.getStringValue(StatisticTag.RATE_OF_MEN.name()));

			data.setContent("women_count", statistics.getStringValue(StatisticTag.NUM_OF_WOMEN.name()));
			data.setContent("women_rate", statistics.getStringValue(StatisticTag.RATE_OF_WOMEN.name()));

			data.setContent("unknown_count", statistics.getStringValue(StatisticTag.NUM_OF_UNKNOWN.name()));
			data.setContent("unknown_rate", statistics.getStringValue(StatisticTag.RATE_OF_UNKNOWN.name()));

			data.setContent("non_single_men_count", statistics.getStringValue(StatisticTag.NUM_OF_NON_SINGLE_MEN.name()));

			data.setContent("non_single_women_count", statistics.getStringValue(StatisticTag.NUM_OF_NON_SINGLE_WOMEN.name()));

			data.setContent("union_count", statistics.getStringValue(StatisticTag.NUM_OF_UNIONS.name()));

			data.setContent("marriage_count", statistics.getStringValue(StatisticTag.NUM_OF_MARRIAGES.name()));
			data.setContent("marriage_density", statistics.getStringValue(StatisticTag.DENSITY_OF_MARRIAGES.name()));

			data.setContent("fertile_marriage_count", statistics.getStringValue(StatisticTag.NUM_OF_FERTILE_MARRIAGES.name()));
			data.setContent("fertile_marriage_rate", statistics.getStringValue(StatisticTag.RATE_OF_FERTILE_MARRIAGES.name()));

			data.setAttribute("cross_first_cousin_marriage_count_label", "title", Stag.CROSS_FIRST_COUSIN_MARRIAGE_PATTERN.toString());
			data.setContent("cross_first_cousin_marriage_count", statistics.getStringValue(StatisticTag.NUM_OF_CROSS_FIRST_COUSIN_MARRIAGES.name()));
			data.setContent("cross_first_cousin_marriage_rate", statistics.getStringValue(StatisticTag.RATE_OF_CROSS_FIRST_COUSIN_MARRIAGES.name()));

			data.setAttribute("levirate_marriage_count_label", "title", Stag.LEVIRATE_MARRIAGE_PATTERN.toString());
			data.setContent("levirate_marriage_count", statistics.getStringValue(StatisticTag.NUM_OF_LEVIRATE_MARRIAGES.name()));
			data.setContent("levirate_marriage_rate", statistics.getStringValue(StatisticTag.RATE_OF_LEVIRATE_MARRIAGES.name()));

			data.setAttribute("sororate_marriage_count_label", "title", Stag.SORORATE_MARRIAGE_PATTERN.toString());
			data.setContent("sororate_marriage_count", statistics.getStringValue(StatisticTag.NUM_OF_SORORATE_MARRIAGES.name()));
			data.setContent("sororate_marriage_rate", statistics.getStringValue(StatisticTag.RATE_OF_SORORATE_MARRIAGES.name()));

			data.setAttribute("double_or_exchange_marriage_count_label", "title", Stag.DOUBLE_OR_EXCHANGE_MARRIAGE_PATTERN.toString());
			data.setContent("double_or_exchange_marriage_count", statistics.getStringValue(StatisticTag.NUM_OF_DOUBLE_OR_EXCHANGE_MARRIAGES.name()));
			data.setContent("double_or_exchange_marriage_rate", statistics.getStringValue(StatisticTag.RATE_OF_DOUBLE_OR_EXCHANGE_MARRIAGES.name()));

			data.setAttribute("niece_nephew_marriage_count_label", "title", Stag.NIECE_NEPHEW_MARRIAGE_PATTERN.toString());
			data.setContent("niece_nephew_marriage_count", statistics.getStringValue(StatisticTag.NUM_OF_NIECE_NEPHEW_MARRIAGES.name()));
			data.setContent("niece_nephew_marriage_rate", statistics.getStringValue(StatisticTag.RATE_OF_NIECE_NEPHEW_MARRIAGES.name()));

			data.setAttribute("parallel_first_cousin_marriage_count_label", "title", Stag.PARALLEL_FIRST_COUSIN_MARRIAGE_PATTERN.toString());
			data.setContent("parallel_first_cousin_marriage_count", statistics.getStringValue(StatisticTag.NUM_OF_PARALLEL_FIRST_COUSIN_MARRIAGES.name()));
			data.setContent("parallel_first_cousin_marriage_rate", statistics.getStringValue(StatisticTag.RATE_OF_PARALLEL_FIRST_COUSIN_MARRIAGES.name()));

			data.setAttribute("first_cousin_marriage_count_label", "title", Stag.FIRST_COUSIN_MARRIAGE_PATTERN.toString());
			data.setContent("first_cousin_marriage_count", statistics.getStringValue(StatisticTag.NUM_OF_FIRST_COUSIN_MARRIAGES.name()));
			data.setContent("first_cousin_marriage_rate", statistics.getStringValue(StatisticTag.RATE_OF_FIRST_COUSIN_MARRIAGES.name()));

			data.setAttribute("double_marriage_count_label", "title", Stag.DOUBLE_MARRIAGE_PATTERN.toString());
			data.setContent("double_marriage_count", statistics.getStringValue(StatisticTag.NUM_OF_DOUBLE_MARRIAGES.name()));
			data.setContent("double_marriage_rate", statistics.getStringValue(StatisticTag.RATE_OF_DOUBLE_MARRIAGES.name()));

			data.setAttribute("exchange_marriage_count_label", "title", Stag.EXCHANGE_MARRIAGE_PATTERN.toString());
			data.setContent("exchange_marriage_count", statistics.getStringValue(StatisticTag.NUM_OF_EXCHANGE_MARRIAGES.name()));
			data.setContent("exchange_marriage_rate", statistics.getStringValue(StatisticTag.RATE_OF_EXCHANGE_MARRIAGES.name()));

			data.setContent("parent_child_ties_count", statistics.getStringValue(StatisticTag.NUM_OF_PARENT_CHILD_TIES.name()));

			data.setContent("filiation_density", statistics.getStringValue(StatisticTag.DENSITY_OF_FILIATION.name()));

			data.setContent("spouse_of_men_mean", statistics.getStringValue(StatisticTag.MEAN_SPOUSE_OF_MEN.name()));
			data.setContent("spouse_of_women_mean", statistics.getStringValue(StatisticTag.MEAN_SPOUSE_OF_WOMEN.name()));

			data.setContent("children_per_fertile_couple_mean", statistics.getStringValue(StatisticTag.MEAN_CHILDREN_PER_FERTILE_COUPLE.name()));

			data.setContent("cohusband_relations_count", statistics.getStringValue(StatisticTag.NUM_OF_COHUSBAND_RELATIONS.name()));
			data.setContent("cowife_relations_count", statistics.getStringValue(StatisticTag.NUM_OF_COWIFE_RELATIONS.name()));

			data.setContent("depth", statistics.getStringValue(StatisticTag.DEPTH.name()));
			data.setContent("depth_mean", statistics.getStringValue(StatisticTag.DEPTH_MEAN.name()));

			data.setContent("agnatic_fratry_size_mean", statistics.getStringValue(StatisticTag.MEAN_AGNATIC_FRATRY_SIZE.name()));
			data.setContent("uterine_fratry_size_mean", statistics.getStringValue(StatisticTag.MEAN_UTERINE_FRATRY_SIZE.name()));

			data.setContent("elementary_cycles_count", statistics.getStringValue(StatisticTag.NUM_OF_ELEMENTARY_CYCLES.name()));

			data.setContent("component_count", statistics.getStringValue(StatisticTag.NUM_OF_COMPONENTS.name()));

			Double componentValue = statistics.getDoubleValue(StatisticTag.MEAN_OF_COMPONENTS_SHARE_AGNATIC.name());
			if (componentValue != null)
			{
				data.setContent("component_share_agnatic_mean", String.format("%.2f", componentValue));
			}

			componentValue = statistics.getDoubleValue(StatisticTag.MEAN_OF_COMPONENTS_SHARE_AGNATIC_WO_SINGLETON.name());
			if (componentValue != null)
			{
				data.setContent("component_share_agnatic_wo_singleton_mean", String.format("%.2g", componentValue));
			}

			componentValue = statistics.getDoubleValue(StatisticTag.MEAN_OF_COMPONENTS_SHARE_UTERINE.name());
			data.setContent("component_share_uterine_mean", String.format("%.2f", componentValue));

			componentValue = statistics.getDoubleValue(StatisticTag.MEAN_OF_COMPONENTS_SHARE_UTERINE_WO_SINGLETON.name());
			if (componentValue != null)
			{
				data.setContent("component_share_uterine_wo_singleton_mean", String.format("%.2g", componentValue));
			}

			componentValue = statistics.getDoubleValue(StatisticTag.MAX_OF_COMPONENTS_SHARE_AGNATIC.name());
			if (componentValue != null)
			{
				data.setContent("component_share_agnatic_max", String.format("%.2f", componentValue));
			}

			componentValue = statistics.getDoubleValue(StatisticTag.MAX_OF_COMPONENTS_SHARE_UTERINE.name());
			if (componentValue != null)
			{
				data.setContent("component_share_uterine_max", String.format("%.2f", componentValue));
			}

			// Watch raw statistics button.
			data.setAttribute("raw_statistics_button", "href", "/kidarep/dataset_raw_statistics.xhtml?dataset_id=" + dataset.getId());
		}

		// Rebuild statistics button.
		if (kiwa.isWebmaster(account))
		{
			data.setContent("clear_statistics_button", "Clear statistics");
			data.setAttribute("clear_statistics_button", "href", "/stag/admin/clear_dataset_statistics.xhtml?dataset_id=" + dataset.getId());

			data.setContent("rebuild_statistics_button", "Rebuild statistics");
			data.setAttribute("rebuild_statistics_button", "href", "/stag/admin/rebuild_dataset_statistics.xhtml?dataset_id=" + dataset.getId());
		}
		else
		{
			data.setAttribute("clear_statistics_button", "class", "xid:nodisplay");
			data.setAttribute("rebuild_statistics_button", "class", "xid:nodisplay");
		}

		//
		result = view.dynamize(data);

		//
		return result;
	}

	/**
	 * 
	 * @param dataset
	 * @param account
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	public String getHtmlBody(final Dataset dataset, final Account account, final Locale locale) throws Exception
	{
		String result;

		result = XidynUtils.extractBodyContent(getHtml(dataset, account, locale));

		//
		return result;
	}
}

// ////////////////////////////////////////////////////////////////////////
