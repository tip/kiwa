/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.kidarep.statisticsviews;

import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.kidarep.DatasetFile;
import org.kinsources.kiwa.kidarep.Statistics;
import org.kinsources.kiwa.stag.StatisticTag;

import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;
import fr.devinsy.xidyn.utils.XidynUtils;

/**
 *
 */
public class TerminologyStatisticsView
{
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(TerminologyStatisticsView.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static URLPresenter view = new URLPresenter("/org/kinsources/kiwa/website/kidarep/statisticsviews/terminology_statistics_view.html");

	/**
	 * 
	 */
	public TerminologyStatisticsView()
	{
	}

	/**
	 * {@inheritDoc}
	 */
	public StringBuffer getHtml(final Dataset dataset, final Account account, final Locale locale) throws Exception
	{
		StringBuffer result;

		//
		TagDataManager data = new TagDataManager();

		//
		logger.debug("locale=[{}]", locale);

		// Statistics
		// ==========
		DatasetFile sourceFile = kiwa.getDatasetFile(dataset, account);

		if ((sourceFile == null) || (sourceFile.statistics().isEmpty()))
		{
			data.setAttribute("statistics_iurg", "style", "display: none");
			data.setContent("statistics_data", "No statistics.");
		}
		else
		{
			//
			Statistics statistics = sourceFile.statistics();

			//
			data.setContent("self_term_value", statistics.getStringValue(StatisticTag.SELF_NAME.name()));
			data.setContent("terms_count", statistics.getStringValue(StatisticTag.NUM_OF_TERMS.name()));
			data.setContent("male_count", statistics.getStringValue(StatisticTag.NUM_OF_MALES.name()));
			data.setContent("male_exclusive_count", statistics.getStringValue(StatisticTag.NUM_OF_MALES_EXCLUSIVE.name()));
			data.setContent("female_count", statistics.getStringValue(StatisticTag.NUM_OF_FEMALES.name()));
			data.setContent("female_exclusive_count", statistics.getStringValue(StatisticTag.NUM_OF_FEMALES_EXCLUSIVE.name()));

			data.setContent("male_speaker_count", statistics.getStringValue(StatisticTag.NUM_OF_MALES_SPEAKER.name()));
			data.setContent("male_speaker_exclusive_count", statistics.getStringValue(StatisticTag.NUM_OF_MALES_SPEAKER_EXCLUSIVE.name()));
			data.setContent("female_speaker_count", statistics.getStringValue(StatisticTag.NUM_OF_FEMALES_SPEAKER.name()));
			data.setContent("female_speaker_exclusive_count", statistics.getStringValue(StatisticTag.NUM_OF_FEMALES_SPEAKER_EXCLUSIVE.name()));

			data.setContent("generation_pattern_total_value", statistics.getStringValue(StatisticTag.GENERATION_PATTERN_TOTAL.name()));
			data.setContent("generation_pattern_male_value", statistics.getStringValue(StatisticTag.GENERATION_PATTERN_MALE.name()));
			data.setContent("generation_pattern_female_value", statistics.getStringValue(StatisticTag.GENERATION_PATTERN_FEMALE.name()));

			data.setContent("autoreciprocal_count", statistics.getStringValue(StatisticTag.NUM_OF_AUTORECIPROCAL_TERMS.name()));
			data.setContent("recursive_value", statistics.getStringValue(StatisticTag.RECURSIVE_TERMS.name()));

			{
				String value = statistics.getStringValue(StatisticTag.ASCENDANT_CLASSIFICATION.name());
				if (value != null)
				{
					value = StringUtils.capitalize(value.replace('_', ' ').toLowerCase());
				}
				data.setContent("ascendant_classification_value", value);
			}

			{
				String value = statistics.getStringValue(StatisticTag.COUSIN_CLASSIFICATION.name());
				if (value != null)
				{
					value = StringUtils.capitalize(value.replace('_', ' ').toLowerCase());
				}
				data.setContent("cousin_classification_value", value);
			}

			{
				Double value = statistics.getDoubleValue(StatisticTag.KIN_TERM_NETWORK_MALE_SPEAKER_DENSITY.name());
				if (value != null)
				{
					data.setContent("male_density_value", String.format("%.2f", value));
				}
			}
			{
				Double value = statistics.getDoubleValue(StatisticTag.KIN_TERM_NETWORK_FEMALE_SPEAKER_DENSITY.name());
				if (value != null)
				{
					data.setContent("female_density_value", String.format("%.2f", value));
				}
			}
			{
				Double value = statistics.getDoubleValue(StatisticTag.KIN_TERM_NETWORK_MALE_SPEAKER_MEAN_DEGREE.name());
				if (value != null)
				{
					data.setContent("male_mean_degree_value", String.format("%.2f", value));
				}
			}
			{
				Double value = statistics.getDoubleValue(StatisticTag.KIN_TERM_NETWORK_FEMALE_SPEAKER_MEAN_DEGREE.name());
				if (value != null)
				{
					data.setContent("female_mean_degree_value", String.format("%.2f", value));
				}
			}
			{
				Double value = statistics.getDoubleValue(StatisticTag.KIN_TERM_NETWORK_MALE_SPEAKER_MEAN_CLUSTERING_COEFFICIENT.name());
				if (value != null)
				{
					data.setContent("male_mean_clustering_value", String.format("%.2f", value));
				}
			}
			{
				Double value = statistics.getDoubleValue(StatisticTag.KIN_TERM_NETWORK_FEMALE_SPEAKER_MEAN_CLUSTERING_COEFFICIENT.name());
				if (value != null)
				{
					data.setContent("female_mean_clustering_value", String.format("%.2f", value));
				}
			}
			data.setContent("male_components_value", statistics.getStringValue(StatisticTag.KIN_TERM_NETWORK_MALE_COMPONENTS.name()));
			data.setContent("female_components_value", statistics.getStringValue(StatisticTag.KIN_TERM_NETWORK_FEMALE_COMPONENTS.name()));
			{
				Double value = statistics.getDoubleValue(StatisticTag.KIN_TERM_NETWORK_MALE_CONCENTRATION.name());
				data.setContent("male_concentration_value", String.format("%.2f", value));
			}
			{
				Double value = statistics.getDoubleValue(StatisticTag.KIN_TERM_NETWORK_FEMALE_CONCENTRATION.name());
				data.setContent("female_concentration_value", String.format("%.2f", value));
			}
		}

		// Watch raw statistics button.
		data.setAttribute("raw_statistics_button", "href", "/kidarep/dataset_raw_statistics.xhtml?dataset_id=" + dataset.getId());

		// Rebuild statistics button.
		if (kiwa.isWebmaster(account))
		{
			data.setContent("clear_statistics_button", "Clear statistics");
			data.setAttribute("clear_statistics_button", "href", "/stag/admin/clear_dataset_statistics.xhtml?dataset_id=" + dataset.getId());

			data.setContent("rebuild_statistics_button", "Rebuild statistics");
			data.setAttribute("rebuild_statistics_button", "href", "/stag/admin/rebuild_dataset_statistics.xhtml?dataset_id=" + dataset.getId());
		}
		else
		{
			data.setAttribute("clear_statistics_button", "class", "xid:nodisplay");
			data.setAttribute("rebuild_statistics_button", "class", "xid:nodisplay");
		}

		//
		result = view.dynamize(data);

		//
		return result;
	}

	/**
	 * 
	 * @param dataset
	 * @param account
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	public String getHtmlBody(final Dataset dataset, final Account account, final Locale locale) throws Exception
	{
		String result;

		result = XidynUtils.extractBodyContent(getHtml(dataset, account, locale));

		//
		return result;
	}
}

// ////////////////////////////////////////////////////////////////////////
