/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.kidarep;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.actilog.Log.Level;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.FormatIssue;
import org.kinsources.kiwa.website.charter.ErrorView;

import fr.devinsy.util.StringList;

/**
 *
 */
public class Cancel_format_issue_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8988256363597317696L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Cancel_format_issue_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.kidarep.cancel_format_issue", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);

			// Get parameters.
			// ===============
			String formatIssueId = request.getParameter("format_issue_id");
			logger.info("formatIssueId=[{}]", formatIssueId);

			// Use parameters.
			// ===============
			FormatIssue formatIssue = kiwa.kidarep().getFormatIssueById(formatIssueId);

			if (formatIssue == null)
			{
				throw new IllegalArgumentException("Unknown dataset.");
			}
			else if (formatIssue.getStatus() != FormatIssue.Status.WAITING)
			{
				throw new IllegalArgumentException("Bad status.");
			}
			else if (account == null)
			{
				throw new IllegalArgumentException("Permission denied.");
			}
			else if ((formatIssue.getContributorId() != account.getId()) && (!kiwa.isWebmaster(account)))
			{
				throw new IllegalArgumentException("Permission denied.");
			}
			else
			{
				logger.info("CANCEL FORMAT ISSUE[{}]", formatIssue.getId());
				kiwa.log(Level.INFO, "events.kidarep.cancel_format_issue", "[formatIssueId={}][accountId={}]", formatIssue.getId(), account.getId());

				// Use parameters.
				// ===============
				kiwa.cancelFormatIssue(formatIssue);

				//
				StringList page = new StringList(10);
				page.appendln("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>");
				page.appendln("<!DOCTYPE html>");
				page.appendln("<html xmlns=\"http://www.w3.org/1999/xhtml\"><head><title>Redirection</title></head><body>");
				page.appendln("<script type=\"text/javascript\">window.history.go(-2);</script>");
				page.appendln("</body></html>");

				//
				response.reset();
				response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.getWriter().println(page);

				//
				// Redirector.redirect(response,
				// "/kidarep/dataset_file_upload.xhtml?dataset_id=" +
				// formatIssue.getDatasetId());
			}
		}
		catch (final IllegalArgumentException exception)
		{
			//
			ErrorView.show(request, response, "Cancel format issue", exception.getMessage(), "/");

		}
		catch (final Exception exception)
		{
			//
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
