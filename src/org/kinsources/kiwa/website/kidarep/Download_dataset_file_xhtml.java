/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.kidarep;

import java.io.IOException;
import java.util.Locale;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.actilog.Log.Level;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.kidarep.DatasetFile;
import org.kinsources.kiwa.kidarep.LoadedFile;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.tip.puck.net.Net;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.kiss4web.SimpleServletDispatcher;
import fr.devinsy.kiss4web.SimpleServletDispatcher.ContentDispositionType;
import fr.devinsy.util.StringList;

/**
 *
 */
public class Download_dataset_file_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8988256363597317696L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Download_dataset_file_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.kidarep.download_dataset_file", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);
			Long accountId = kiwa.getAuthentifiedAccountId(request, response);

			// Get parameters.
			// ===============
			String datasetId = request.getParameter("dataset_id");
			logger.info("datasetId=[{}]", datasetId);

			String gedFormat = request.getParameter("ged");
			logger.info("gedFormat=[{}]", gedFormat);

			String odsFormat = request.getParameter("ods");
			logger.info("odsFormat=[{}]", odsFormat);

			String pajOreGraphFormat = request.getParameter("paj_ore_graph");
			logger.info("pajOreGraphFormat=[{}]", pajOreGraphFormat);

			String pajTipGraphFormat = request.getParameter("paj_tip_graph");
			logger.info("pajTipGraphFormat=[{}]", pajTipGraphFormat);

			String pajPGraphFormat = request.getParameter("paj_p_graph");
			logger.info("pajPGraphFormat=[{}]", pajPGraphFormat);

			String plFormat = request.getParameter("pl");
			logger.info("plFormat=[{}]", plFormat);

			String pucFormat = request.getParameter("puc");
			logger.info("pucFormat=[{}]", pucFormat);

			String tipFormat = request.getParameter("tip");
			logger.info("tipFormat=[{}]", tipFormat);

			String txtFormat = request.getParameter("txt");
			logger.info("txtFormat=[{}]", txtFormat);

			String xlsFormat = request.getParameter("xls");
			logger.info("xlsFormat=[{}]", xlsFormat);

			// Use parameters.
			// ===============
			Dataset dataset = kiwa.kidarep().getDatasetById(datasetId);

			//
			StringList formats = new StringList(10);
			if (gedFormat != null)
			{
				formats.append("ged");
			}
			if (odsFormat != null)
			{
				formats.append("iur.ods");
			}
			if (pajOreGraphFormat != null)
			{
				formats.append("opaj");
			}
			if (pajTipGraphFormat != null)
			{
				formats.append("tpaj");
			}
			if (pajPGraphFormat != null)
			{
				formats.append("ppaj");
			}
			if (plFormat != null)
			{
				formats.append("pl");
			}
			if (pucFormat != null)
			{
				formats.append("puc");
			}
			if (tipFormat != null)
			{
				formats.append("tip");
			}
			if (txtFormat != null)
			{
				formats.append("iur.txt");
			}
			if (xlsFormat != null)
			{
				formats.append("iur.xls");
			}

			//
			if (dataset == null)
			{
				throw new IllegalArgumentException("Unknown dataset.");
			}
			else if (dataset.getOriginFile() == null)
			{
				throw new IllegalArgumentException("Dataset file unavailable.");
			}
			else if ((dataset.getStatus() == Dataset.Status.CREATED) && (!dataset.isContributor(account)) && (!kiwa.isWebmaster(account)))
			{
				throw new IllegalArgumentException("Permission denied (1).");
			}
			else if ((dataset.getStatus() == Dataset.Status.SUBMITTED) && (!dataset.isContributor(account)) && (!kiwa.isSciboarder(account)) && (!kiwa.isWebmaster(account)))
			{
				throw new IllegalArgumentException("Permission denied (2).");
			}
			else if ((dataset.getStatus() != Dataset.Status.CREATED) && (dataset.getStatus() != Dataset.Status.SUBMITTED) && (dataset.getStatus() != Dataset.Status.VALIDATED))
			{
				throw new IllegalArgumentException("Permission denied (3).");
			}
			else
			{
				//
				logger.info("DOWNLOAD DATASET FILE [datasetId={}][name={}]", datasetId, dataset.getName());
				kiwa.log(Level.INFO, "events.download_dataset_file", "[accountId={}][datasetId={}][name={}][size={}]", accountId, dataset.getId());

				//
				if (formats.isEmpty())
				{
					Redirector.redirect(response, "dataset_file_download.xhtml?dataset_id=" + dataset.getId());
				}
				else if (formats.size() == 1)
				{
					//
					DatasetFile datasetFile = kiwa.getDatasetFile(dataset, account);

					//
					LoadedFile sourceFile = kiwa.getLoadedFileDetached(datasetFile);

					//
					LoadedFile targetFile = Kiwa.convert(formats.get(0), dataset.baseFileName(), sourceFile);

					//
					if (dataset.getStatus() == Dataset.Status.VALIDATED)
					{
						datasetFile.incDownloadCount();
						kiwa.updateDataset(dataset);
					}

					//
					logger.debug("MIME TYPE=" + getServletContext().getMimeType(targetFile.name()));
					SimpleServletDispatcher.returnFile(response, targetFile.name(), targetFile.data(), getServletContext().getMimeType(targetFile.name()), ContentDispositionType.ATTACHMENT, 0);
				}
				else
				{
					//
					DatasetFile datasetFile = kiwa.getDatasetFile(dataset, account);

					//
					LoadedFile sourceFile = kiwa.getLoadedFileDetached(datasetFile);

					//
					Net net = Kiwa.loadNet(sourceFile);

					//
					String targetFileName = dataset.baseFileName() + ".zip";

					//
					response.reset();
					response.setContentType(getServletContext().getMimeType(targetFileName));
					response.setHeader("Content-Disposition", "attachment; filename=\"" + targetFileName + "\"");

					response.setDateHeader("Expires", 0);
					response.setHeader("Cache-Control", "max-age=0");

					response.flushBuffer();

					ServletOutputStream out = response.getOutputStream();

					try
					{
						//
						ZipOutputStream zos = new ZipOutputStream(out);
						zos.setLevel(Deflater.BEST_COMPRESSION);
						zos.setMethod(ZipOutputStream.DEFLATED);
						zos.setComment("Kinsources");

						//
						for (String format : formats)
						{
							//
							LoadedFile targetFile = Kiwa.convertNet(format, dataset.baseFileName(), net);

							logger.debug("name=" + sourceFile.name());

							zos.putNextEntry(new ZipEntry(targetFile.name()));
							zos.write(targetFile.data());

							zos.flush();
							zos.closeEntry();
						}

						//
						zos.close();
					}
					catch (IOException exception)
					{
						logger.error(ExceptionUtils.getStackTrace(exception));
						response.sendError(HttpServletResponse.SC_PARTIAL_CONTENT);
					}
					finally
					{
						try
						{
							out.flush();
							out.close();
						}
						catch (IOException exception)
						{
							logger.error(ExceptionUtils.getStackTrace(exception));
						}
					}

					//
					if (dataset.getStatus() == Dataset.Status.VALIDATED)
					{
						datasetFile.incDownloadCount();
						kiwa.updateDataset(dataset);
					}
				}
			}

		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
