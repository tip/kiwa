/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.kidarep.admin;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kernel.KiwaRoles;
import org.kinsources.kiwa.kidarep.Contributor;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.kidarep.FormatIssue;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.webmaster.WebmasterView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Format_issue_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8903536311059605390L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Format_issue_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/kidarep/admin/format_issue.html");
	private static WebmasterView view = new WebmasterView();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.kidarep.admin.format_issue", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Account userAccount = kiwa.getAuthentifiedAccount(request, response);

			// Get parameters
			// ==============
			String formatIssueId = request.getParameter("format_issue_id");
			logger.info("formatIssueId=[{}]", formatIssueId);

			// Use parameters
			// ==============
			FormatIssue formatIssue = kiwa.kidarep().getFormatIssueById(formatIssueId);

			if ((userAccount == null) || ((!userAccount.isRole(KiwaRoles.WEBMASTER)) && (!userAccount.isRole(KiwaRoles.FORMAT_INSPECTOR))))
			{
				Redirector.redirect(response, "/");
			}
			else if (formatIssue == null)
			{
				throw new IllegalArgumentException("Unknown format issue.");
			}
			else
			{
				//
				TagDataManager data = new TagDataManager();

				//
				data.setContent("format_issue_count", kiwa.kidarep().countOfFormatIssues());
				data.setContent("format_issue_ready_count", kiwa.kidarep().countOfReadyFormatIssues());

				//
				data.setContent("id_label", "ID:");
				data.setContent("id", formatIssue.getId());

				//
				data.setContent("file_name_label", "File name:");
				data.setContent("file_name", StringEscapeUtils.escapeXml(formatIssue.getFile().name()));

				//
				data.setContent("contributor_name_label", "Contributor:");
				Contributor contributor = kiwa.kidarep().getContributorById(formatIssue.getContributorId());
				if (contributor == null)
				{
					data.setContent("contributor_name", "n/a");
				}
				else
				{
					data.setContent("contributor_link", StringEscapeUtils.escapeXml(contributor.getFullName()));
					data.setAttribute("contributor_link", "href", "/browser/contributor.xhtml?contributor_id=" + contributor.getId());
				}

				//
				data.setContent("dataset_name_label", "Dataset:");
				Dataset dataset = kiwa.kidarep().getDatasetById(formatIssue.getDatasetId());
				if (dataset == null)
				{
					data.setContent("dataset_name", "n/a");
				}
				else
				{
					data.setContent("dataset_link", StringEscapeUtils.escapeXml(dataset.getName()) + " (ID=" + dataset.getId() + ")");
					data.setAttribute("dataset_link", "href", kiwa.permanentURI(dataset));
				}

				//
				data.setContent("file_length_label", "File length:");
				data.setContent("file_length", formatIssue.getFile().header().length() + " bytes");

				//
				data.setContent("creation_date_label", "Creation date:");
				data.setContent("creation_date", formatIssue.getCreationDate().toString("dd/MM/yyyy") + " " + formatIssue.getCreationDate().toString("HH'h'mm"));

				//
				data.setContent("status_label", "Status:");
				data.setContent("status", formatIssue.getStatus().toString());

				//
				data.setContent("download_label", "Download:");
				data.setAttribute("download", "href", "download_format_issue_file.xhtml?format_issue_id=" + formatIssue.getId());

				data.setAttribute("remove_format_issue", "href", "remove_format_issue.xhtml?format_issue_id=" + formatIssue.getId());
				data.setContent("remove_format_issue", "Remove");

				//
				String content = xidyn.dynamize(data).toString();

				// Send response
				// ============
				StringBuffer html = kiwa.getCharterView().getHtml(userAccount.getId(), locale, view.getHtml(content));

				//
				response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.getWriter().println(html);
			}
		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		//
		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}
}

// ////////////////////////////////////////////////////////////////////////
