/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.kidarep;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.DatasetFile;
import org.tip.puck.net.workers.AttributeDescriptor;
import org.tip.puck.net.workers.AttributeDescriptors;
import org.tip.puck.net.workers.AttributeFilter;
import org.tip.puck.net.workers.AttributeFilters;

import fr.devinsy.util.StringList;
import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;
import fr.devinsy.xidyn.utils.XidynUtils;

/**
 * Convention: if filter list is null or empty then it is undefined.
 */
public class DatasetFiltersView
{
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(DatasetFiltersView.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static URLPresenter view = new URLPresenter("/org/kinsources/kiwa/website/kidarep/dataset_filters_view.html");

	/**
	 * 
	 */
	public DatasetFiltersView()
	{
	}

	/**
	 * {@inheritDoc}
	 */
	public StringBuffer getHtml(final DatasetFile file, final AttributeFilters filters, final Locale locale) throws Exception
	{
		StringBuffer result;

		//
		TagDataManager data = new TagDataManager();

		//
		logger.debug("locale=[{}]", locale);

		data.setContent("data_filtering_label", "Data filtering:");
		if (file == null)
		{
			//
			data.setContent("data_filtering", "Irrelevant.");
		}
		else
		{
			//
			data.setContent("anonymize_none_label", "No filtering");
			data.setContent("anonymize_by_gender_and_id_label", "Anonymize by gender and id (default settings, check if this is useful to you)");
			data.setContent("anonymize_by_first_name_label", "Anonymize by first name");
			data.setContent("anonymize_by_last_name_label", "Anonymize by last name");

			//
			if ((filters == null) || (filters.isEmpty()))
			{
				//
				data.setAttribute("anonymize_by_gender_and_id", "checked", "checked");
			}
			else
			{
				//
				AttributeFilter nameFilter = filters.getByIndex(0);
				switch (nameFilter.getMode())
				{
					case NONE:
						data.setAttribute("anonymize_none", "checked", "checked");
					break;
					case ANONYMIZE_BY_FIRST_NAME:
						data.setAttribute("anonymize_by_first_name", "checked", "checked");
					break;
					case ANONYMIZE_BY_GENDER_AND_ID:
						data.setAttribute("anonymize_by_gender_and_id", "checked", "checked");
					break;
					case ANONYMIZE_BY_LAST_NAME:
						data.setAttribute("anonymize_by_last_name", "checked", "checked");
					break;
					default:
				}
			}

			//
			if (file.statistics().attributeDescriptors().isEmpty())
			{
				data.setContent("other_attributes", "Your upload dataset file contains none attribute.");
			}
			else
			{
				int index = 0;
				for (AttributeDescriptor descriptor : file.statistics().attributeDescriptors())
				{
					//
					data.setContent("attribute_filter_line", index, "attribute_filter_scope", XMLTools.escapeXmlBlank(StringUtils.capitalize(StringUtils.lowerCase(descriptor.getRelationName()))));
					data.setContent("attribute_filter_line", index, "attribute_filter_label", XMLTools.escapeXmlBlank(descriptor.getLabel()));
					data.setAttribute("attribute_filter_line", index, "attribute_filter_radio_none", "name", "attribute_filter_" + index);
					data.setAttribute("attribute_filter_line", index, "attribute_filter_radio_anonymize", "name", "attribute_filter_" + index);
					data.setAttribute("attribute_filter_line", index, "attribute_filter_radio_reduce_date", "name", "attribute_filter_" + index);
					data.setAttribute("attribute_filter_line", index, "attribute_filter_radio_remove", "name", "attribute_filter_" + index);

					//
					if ((filters == null) || (filters.isEmpty()))
					{
						data.setAttribute("attribute_filter_line", index, "attribute_filter_radio_remove", "checked", "checked");
					}
					else
					{
						AttributeFilters matchingfilters;
						switch (descriptor.getScope())
						{
							case CORPUS:
								matchingfilters = filters.findBy(AttributeFilter.Scope.CORPUS, descriptor.getLabel());
							break;
							case INDIVIDUALS:
								matchingfilters = filters.findBy(AttributeFilter.Scope.INDIVIDUALS, descriptor.getLabel());
							break;
							case FAMILIES:
								matchingfilters = filters.findBy(AttributeFilter.Scope.FAMILIES, descriptor.getLabel());
							break;
							case RELATION:
								matchingfilters = filters.findBy(descriptor.getOptionalRelationName(), descriptor.getLabel());
							break;
							case ACTORS:
								matchingfilters = filters.findBy(AttributeFilter.Scope.ACTORS, descriptor.getLabel());
							break;
							default:
								matchingfilters = null;
						}

						//
						if ((matchingfilters == null) || (matchingfilters.isEmpty()))
						{
							data.setAttribute("attribute_filter_line", index, "attribute_filter_radio_none", "checked", "checked");
						}
						else
						{
							//
							AttributeFilter filter = matchingfilters.getByIndex(0);

							//
							switch (filter.getMode())
							{
								case NONE:
									data.setAttribute("attribute_filter_line", index, "attribute_filter_radio_none", "checked", "checked");
								break;
								case ANONYMIZE_BY_NUMBERING:
									data.setAttribute("attribute_filter_line", index, "attribute_filter_radio_anonymize", "checked", "checked");
								break;
								case REDUCE_DATE:
									data.setAttribute("attribute_filter_line", index, "attribute_filter_radio_reduce_date", "checked", "checked");
								break;
								case REMOVE:
									data.setAttribute("attribute_filter_line", index, "attribute_filter_radio_remove", "checked", "checked");
								break;
								default:
							}
						}
					}

					//
					index += 1;
				}
			}
		}

		//
		result = view.dynamize(data);

		//
		return result;
	}

	/**
	 * 
	 * @param file
	 * @param filters
	 * @param locale
	 * @return
	 * @throws Exception
	 */
	public String getHtmlBody(final DatasetFile file, final AttributeFilters filters, final Locale locale) throws Exception
	{
		String result;

		result = XidynUtils.extractBodyContent(getHtml(file, filters, locale));

		//
		return result;
	}

	/**
	 * 
	 * @param request
	 * @return
	 */
	public static AttributeFilters getCriteria(final HttpServletRequest request, final AttributeDescriptors descriptors)
	{
		AttributeFilters result;

		//
		result = new AttributeFilters();

		//
		String individualNameAnonymize = request.getParameter("individual_name_anonymize");
		logger.info("individualNameAnonymize=[{}]", individualNameAnonymize);

		//
		if (individualNameAnonymize == null)
		{
			result.add(new AttributeFilter(AttributeFilter.Scope.INDIVIDUALS, "name", AttributeFilter.Mode.ANONYMIZE_BY_GENDER_AND_ID));
		}
		else
		{
			result.add(new AttributeFilter(AttributeFilter.Scope.INDIVIDUALS, "name", AttributeFilter.Mode.valueOf(individualNameAnonymize)));
		}

		//
		StringList filterParameters = new StringList(20);
		int index = 0;
		boolean ended = false;
		while (!ended)
		{
			//
			String filterParameter = request.getParameter("attribute_filter_" + index);

			//
			if (filterParameter == null)
			{
				ended = true;
			}
			else
			{
				//
				filterParameters.add(filterParameter);

				//
				index += 1;
			}
		}
		logger.info("filtersParameters=[{}]", filterParameters.toStringWithFrenchCommas());

		//
		DatasetFiltersView.merge(result, filterParameters, descriptors);

		//
		return result;
	}

	/**
	 * 
	 * @param filterParameters
	 * @param descriptors
	 * @return
	 */
	public static void merge(final AttributeFilters target, final StringList filterParameters, final AttributeDescriptors descriptors)
	{
		//
		int index = 0;
		for (String filterParameter : filterParameters)
		{
			//
			AttributeFilter.Mode filterMode;
			if (StringUtils.equals(filterParameter, "ANONYMIZE"))
			{
				filterMode = AttributeFilter.Mode.ANONYMIZE_BY_NUMBERING;
			}
			else if (StringUtils.equals(filterParameter, "REDUCE_DATE"))
			{
				filterMode = AttributeFilter.Mode.REDUCE_DATE;
			}
			else if (StringUtils.equals(filterParameter, "REMOVE"))
			{
				filterMode = AttributeFilter.Mode.REMOVE;
			}
			else
			{
				filterMode = AttributeFilter.Mode.NONE;
			}

			//
			if (filterMode != AttributeFilter.Mode.NONE)
			{
				//
				AttributeDescriptor descriptor = descriptors.getByIndex(index);

				//
				AttributeFilter.Scope filterScope;
				switch (descriptor.getScope())
				{
					case CORPUS:
						filterScope = AttributeFilter.Scope.CORPUS;
					break;
					case INDIVIDUALS:
						filterScope = AttributeFilter.Scope.INDIVIDUALS;
					break;
					case FAMILIES:
						filterScope = AttributeFilter.Scope.FAMILIES;
					break;
					case RELATION:
						filterScope = AttributeFilter.Scope.RELATION;
					break;
					case ACTORS:
						filterScope = AttributeFilter.Scope.ACTORS;
					break;
					default:
						filterScope = AttributeFilter.Scope.NONE;
				}

				//
				AttributeFilter filter;
				if (descriptor.getScope() == AttributeDescriptor.Scope.RELATION)
				{
					filter = new AttributeFilter(descriptor.getOptionalRelationName(), descriptor.getLabel(), filterMode);
				}
				else
				{
					filter = new AttributeFilter(filterScope, descriptor.getLabel(), filterMode);
				}

				//
				target.add(filter);
			}

			//
			index += 1;
		}
	}
}

// ////////////////////////////////////////////////////////////////////////
