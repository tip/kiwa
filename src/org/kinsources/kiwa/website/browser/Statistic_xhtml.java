/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.browser;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.kidarep.Dataset.Type;
import org.kinsources.kiwa.kidarep.Datasets;
import org.kinsources.kiwa.stag.StatisticTag;
import org.kinsources.kiwa.utils.WeightedDataset;
import org.kinsources.kiwa.utils.WeightedDatasetComparator.Criteria;
import org.kinsources.kiwa.utils.WeightedDatasetList;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.charter.KiwaCharterView;

import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Statistic_xhtml extends HttpServlet
{
	private static final long serialVersionUID = 3523141827401383261L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Statistic_xhtml.class);
	private static BrowserView browserView = new BrowserView();
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/browser/statistic.html");
	private static final Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		logger.debug("doGet starting...");

		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.browser.statistic", request);

			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);
			Long accountId = Account.getId(account);

			// Get parameters.
			// ===============
			String tagName = request.getParameter("name");
			logger.info("tagName=[{}]", tagName);

			// Use parameters.
			// ===============
			if (tagName == null)
			{
				throw new IllegalArgumentException("Invalid name.");
			}
			else
			{
				tagName = tagName.toUpperCase();
				StatisticTag tag = StatisticTag.valueOf(tagName);

				if (tag == null)
				{
					throw new IllegalArgumentException("Unknown name.");
				}
				else
				{
					//
					Datasets source = kiwa.kidarep().findPublishedDatasets();

					switch (tag.scope())
					{
						case GENEALOGY:
							source = source.find(Type.GENEALOGY);
						break;

						case TERMINOLOGY:
							source = source.find(Type.TERMINOLOGY);
						break;

						default:
					}

					//
					WeightedDatasetList list = new WeightedDatasetList();
					switch (tag.type())
					{
						case DOUBLE:
							for (Dataset dataset : source)
							{
								Double value = dataset.getDoubleStat(tagName);
								list.add(new WeightedDataset(dataset, value));
							}
							list.sort(Criteria.WEIGHT_AS_DOUBLE).reverse();
						break;

						case LONG:
							for (Dataset dataset : source)
							{
								Long value = dataset.getLongStat(tagName);
								list.add(new WeightedDataset(dataset, value));
							}
							list.sort(Criteria.WEIGHT_AS_LONG).reverse();
						break;

						case BOOLEAN:
							for (Dataset dataset : source)
							{
								Boolean value = dataset.getBooleanStat(tagName);
								list.add(new WeightedDataset(dataset, value));
							}
						break;

						case STRING:
							for (Dataset dataset : source)
							{
								String value = dataset.getStringStat(tagName);
								list.add(new WeightedDataset(dataset, value));
							}
							list.sort(Criteria.WEIGHT_AS_STRING).reverse();
						break;
					}

					// Send response.
					// ==============
					TagDataManager data = new TagDataManager();

					//
					data.setContent("title_statistic_name", tagName);
					data.setContent("statistic_scope", StringUtils.capitalize(String.valueOf(tag.scope()).toLowerCase()));

					//
					if (list.isEmpty())
					{
						data.setContent("dataset", "<td colspan=\"10\">No data.</td>");
					}
					else
					{
						int lineIndex = 0;
						for (WeightedDataset weighting : list)
						{
							//
							data.setContent("dataset", lineIndex, "dataset_index", lineIndex + 1);

							switch (weighting.getDataset().getType())
							{
								case GENEALOGY:
									data.setContent("dataset", lineIndex, "dataset_type", "<img src=\"/charter/commons/dataset-genealogy.png\" title=\"Genealogy\" alt=\"Genealogy Type\" />");
									data.setAttribute("dataset", lineIndex, "dataset_type", "sorttable_customkey", "Genealogy");
									data.setAttribute("dataset", lineIndex, "dataset_type", "title", "Genealogy");
								break;

								case TERMINOLOGY:
									data.setContent("dataset", lineIndex, "dataset_type", "<img src=\"/charter/commons/dataset-terminology.png\" title=\"Terminology\" alt=\"Terminology Type\" />");
									data.setAttribute("dataset", lineIndex, "dataset_type", "sorttable_customkey", "Terminology");
									data.setAttribute("dataset", lineIndex, "dataset_type", "title", "Terminology");
								break;

								default:
									data.setContent("dataset", lineIndex, "dataset_type", "?");
									data.setAttribute("dataset", lineIndex, "dataset_type", "sorttable_customkey", "Unknown");
									data.setAttribute("dataset", lineIndex, "dataset_type", "title", "Other");
							}

							data.setContent("dataset", lineIndex, "dataset_name", XMLTools.escapeXmlBlank(weighting.getDataset().getName()));
							data.setAttribute("dataset", lineIndex, "dataset_name", "title", "ID " + weighting.getDataset().getId());
							data.setAttribute("dataset", lineIndex, "dataset_name", "href", kiwa.permanentURI(weighting.getDataset()));

							if (weighting.getWeight() == null)
							{
								data.setContent("dataset", lineIndex, "dataset_value", "n/a");
							}
							else
							{
								data.setContent("dataset", lineIndex, "dataset_value", String.valueOf(weighting.getWeight()));
							}

							//
							lineIndex += 1;
						}
					}

					//
					StringBuffer content = xidyn.dynamize(data);

					//
					StringBuffer html = kiwa.getCharterView().getHtml(KiwaCharterView.Menu.BROWSER, accountId, locale, browserView.getHtml(BrowserView.Menu.ALL_STATISTICS, content, locale, account));

					// Display page.
					response.setContentType("application/xhtml+xml; charset=UTF-8");
					response.getWriter().println(html);
				}
			}
		}
		catch (Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
