/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.browser;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Contributor;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.kidarep.Datasets;
import org.kinsources.kiwa.utils.KiwaUtils;
import org.kinsources.kiwa.website.browser.BrowserView.Menu;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.charter.KiwaCharterView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Contributor_xhtml extends HttpServlet
{
	private static final long serialVersionUID = 3523141827401383261L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Contributor_xhtml.class);
	private static BrowserView browserView = new BrowserView();
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/browser/contributor.html");
	private static final Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		logger.debug("doGet starting...");

		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.browser.contributor", request);

			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);
			Long accountId = Account.getId(account);

			// Get parameters.
			// ===============
			String contributorId = request.getParameter("contributor_id");
			logger.info("contributorId=[{}]", contributorId);

			// Use parameters.
			// ===============
			//
			Contributor contributor = kiwa.kidarep().getContributorById(contributorId);
			if (contributor == null)
			{
				contributor = kiwa.kidarep().getContributorById(accountId);
			}

			// Send response.
			// ==============
			if (account == null)
			{
				Redirector.redirect(response, request.getContextPath() + "/accounts/login.xhtml");
			}
			else if (contributor == null)
			{
				Redirector.redirect(response, request.getContextPath() + "/browser/browse.xhtml");
			}
			else
			{
				//
				TagDataManager data = new TagDataManager();

				data.setContent("contributor_id_label", "ID:");
				data.setContent("contributor_fullname_label", "Name:");
				data.setContent("contributor_organization_label", "Organization:");
				data.setContent("contributor_country_label", "Country:");
				data.setContent("contributor_email_label", "Email:");
				data.setContent("contributor_business_card_label", "About me:");

				//
				data.setContent("title_contributor_name", XMLTools.escapeXmlBlank(contributor.getFullName()));
				data.setContent("contributor_id", contributor.getId());
				if (StringUtils.isBlank(contributor.account().getOrganization()))
				{
					//
					data.setContent("contributor_organization", "");

				}
				else
				{
					//
					data.setContent("contributor_organization_link", XMLTools.escapeXmlBlank(contributor.account().getOrganization()));
					data.setAttribute("contributor_organization_link", "href", "organization.xhtml?name=" + XMLTools.escapeXmlBlank(contributor.account().getOrganization()));
				}
				data.setContent("contributor_country", XMLTools.escapeXmlBlank(contributor.account().getCountry()));

				if (contributor.account().getEmailScope() == Account.EmailScope.PUBLIC)
				{
					data.setContent("contributor_email_value", XMLTools.escapeXmlBlank(contributor.account().getEmail()));
				}
				else
				{
					data.setContent("contributor_email", "");
				}

				data.setContent("contributor_business_card_value", KiwaUtils.htmlize(XMLTools.escapeXmlBlank(contributor.account().getBusinessCard())));

				//
				Datasets datasets;
				Menu menu;
				if ((accountId != null) && (contributor.getId() == accountId))
				{
					datasets = contributor.personalDatasets().find(Dataset.Status.VALIDATED);
					menu = BrowserView.Menu.MY_DATASETS;
				}
				else
				{
					datasets = contributor.personalDatasets().find(Dataset.Status.VALIDATED);
					menu = BrowserView.Menu.ALL_CONTRIBUTORS;
				}

				//
				datasets.sortByName();

				//
				if (datasets.isEmpty())
				{
					data.setContent("dataset_count", "0");
					data.setContent("dataset", "<td colspan=\"10\">No dataset.</td>");
				}
				else
				{
					//
					data.setContent("dataset_count", datasets.size());

					//
					int lineIndex = 0;
					for (Dataset dataset : datasets)
					{
						//
						data.setContent("dataset", lineIndex, "dataset_index", lineIndex + 1);
						data.setContent("dataset", lineIndex, "dataset_id", dataset.getId());

						if (dataset.isGenealogy())
						{
							data.setAttribute("dataset", lineIndex, "dataset_type_icon", "src", "/charter/commons/dataset-genealogy.png");
							data.setAttribute("dataset", lineIndex, "dataset_type_icon", "title", "Genealogy");
							data.setAttribute("dataset", lineIndex, "dataset_type_icon", "alt", "Genealogy Type");
							data.setAttribute("dataset", lineIndex, "dataset_type", "sorttable_customkey", "Genealogy");
						}
						else
						{
							data.setAttribute("dataset", lineIndex, "dataset_type_icon", "src", "/charter/commons/dataset-terminology.png");
							data.setAttribute("dataset", lineIndex, "dataset_type_icon", "title", "Terminology");
							data.setAttribute("dataset", lineIndex, "dataset_type_icon", "alt", "Terminology Type");
							data.setAttribute("dataset", lineIndex, "dataset_type", "sorttable_customkey", "Terminology");
						}

						data.setContent("dataset", lineIndex, "dataset_name", XMLTools.escapeXmlBlank(dataset.getName()));
						data.setAttribute("dataset", lineIndex, "dataset_name", "href", kiwa.permanentURI(dataset));

						if (StringUtils.isBlank(dataset.getAuthor()))
						{
							data.setContent("dataset", lineIndex, "dataset_author", "");
						}
						else
						{
							data.setContent("dataset", lineIndex, "dataset_author_link", XMLTools.escapeXmlBlank(dataset.getAuthor()));
							data.setAttribute("dataset", lineIndex, "dataset_author_link", "href", "field_value.xhtml?name=AUTHOR&amp;value=" + XMLTools.escapeXmlBlank(dataset.getAuthor()));
						}

						if (StringUtils.isBlank(dataset.getRegion()))
						{
							data.setContent("dataset", lineIndex, "dataset_region", "");
						}
						else
						{
							data.setContent("dataset", lineIndex, "dataset_region_link", XMLTools.escapeXmlBlank(dataset.getRegion()));
							data.setAttribute("dataset", lineIndex, "dataset_region_link", "href", "field_value.xhtml?name=REGION&amp;value=" + XMLTools.escapeXmlBlank(dataset.getRegion()));
						}

						if (StringUtils.isBlank(dataset.getCountry()))
						{
							data.setContent("dataset", lineIndex, "dataset_country", "");
						}
						else
						{
							data.setContent("dataset", lineIndex, "dataset_country_link", XMLTools.escapeXmlBlank(dataset.getCountry()));
							data.setAttribute("dataset", lineIndex, "dataset_country_link", "href", "field_value.xhtml?name=COUNTRY&amp;value=" + XMLTools.escapeXmlBlank(dataset.getCountry()));
						}

						data.setContent("dataset", lineIndex, "dataset_individual_count", dataset.countOfIndividuals());
						data.setContent("dataset", lineIndex, "dataset_union_count", dataset.countOfUnions());
						data.setContent("dataset", lineIndex, "dataset_relation_count", dataset.countOfRelations());
						data.setContent("dataset", lineIndex, "dataset_generation_count", dataset.countOfGenerations());

						//
						lineIndex += 1;
					}
				}

				//
				StringBuffer content = xidyn.dynamize(data);

				//
				StringBuffer html = kiwa.getCharterView().getHtml(KiwaCharterView.Menu.BROWSER, accountId, locale, browserView.getHtml(menu, content, locale, account));

				// Display page.
				response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.getWriter().println(html);
			}

		}
		catch (Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
