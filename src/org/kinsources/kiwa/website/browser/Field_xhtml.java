/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.browser;

import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.kidarep.Datasets;
import org.kinsources.kiwa.utils.WeightedString;
import org.kinsources.kiwa.utils.WeightedStrings;
import org.kinsources.kiwa.website.browser.Fields_xhtml.BrowsableDatasetField;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.charter.KiwaCharterView;

import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Field_xhtml extends HttpServlet
{
	private static final long serialVersionUID = 3523141827401383261L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Field_xhtml.class);
	private static BrowserView browserView = new BrowserView();
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/browser/field.html");
	private static final Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		logger.debug("doGet starting...");

		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.browser.field", request);

			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);
			Long accountId = Account.getId(account);

			// Get parameters.
			// ===============
			String fieldName = request.getParameter("name");
			logger.info("fieldName=[{}]", fieldName);

			// Use parameters.
			// ===============
			BrowsableDatasetField field = EnumUtils.getEnum(BrowsableDatasetField.class, fieldName);
			if (field == null)
			{
				throw new IllegalArgumentException("Invalid name.");
			}
			else
			{
				//
				Datasets source = kiwa.kidarep().findPublishedDatasets();

				//
				HashMap<String, Double> list = new HashMap<String, Double>();
				for (Dataset dataset : source)
				{
					String value;
					switch (field)
					{
						case ATLAS_CODE:
							value = dataset.getAtlasCode();
						break;
						case AUTHOR:
							value = dataset.getAuthor();
						break;
						case CODER:
							value = dataset.getCoder();
						break;
						case CONTINENT:
							value = dataset.getContinent();
						break;
						case COUNTRY:
							value = dataset.getCountry();
						break;
						case ETHNIC_OR_CULTURAL_GROUP:
							value = dataset.getEthnicOrCulturalGroup();
						break;
						case LANGUAGE_GROUP:
							value = dataset.getLanguageGroup();
						break;
						case LICENSE:
							value = dataset.getLicense();
						break;
						case LOCATION:
							value = dataset.getLocation();
						break;
						case PUBLICATION_DATE:
							if (dataset.getPublicationDate() == null)
							{
								value = "n/a";
							}
							else
							{
								value = dataset.getPublicationDate().toString("YYYY-MM");
							}
						break;
						case REGION:
							value = dataset.getRegion();
						break;
						default:
							value = null;
					}

					//
					if (value != null)
					{
						Double count = list.get(value);
						if (count == null)
						{
							count = 0.0;
						}
						count += 1;
						list.put(value, count);
					}
				}

				//
				WeightedStrings target = new WeightedStrings(list.size());
				for (String key : list.keySet())
				{
					target.add(new WeightedString(key, list.get(key)));
				}

				//
				target.sortByName();

				// Send response.
				// ==============
				TagDataManager data = new TagDataManager();

				//
				data.setContent("title_field_name", fieldName);

				//
				if (list.isEmpty())
				{
					data.setContent("field", "<td colspan=\"10\">No data.</td>");
				}
				else
				{
					data.setContent("count", target.size());

					int lineIndex = 0;
					for (WeightedString item : target)
					{
						//
						data.setContent("field", lineIndex, "field_index", lineIndex + 1);
						data.setContent("field", lineIndex, "field_name", XMLTools.escapeXmlBlank(item.getString()));
						data.setAttribute("field", lineIndex, "field_name", "href", "field_value.xhtml?name=" + fieldName + "&amp;value=" + XMLTools.escapeXmlBlank(item.getString()));

						data.setContent("field", lineIndex, "field_dataset_count", String.valueOf((long) (double) item.getWeight()));
						data.setAttribute("field", lineIndex, "field_dataset_count", "href", "field_value.xhtml?name=" + fieldName + "&amp;value=" + XMLTools.escapeXmlBlank(item.getString()));

						//
						lineIndex += 1;
					}
				}

				//
				StringBuffer content = xidyn.dynamize(data);

				//
				BrowserView.Menu menu;
				if (StringUtils.equals(fieldName, "AUTHOR"))
				{
					menu = BrowserView.Menu.ALL_AUTHORS;
				}
				else
				{
					menu = BrowserView.Menu.ALL_FIELDS;
				}

				//
				StringBuffer html = kiwa.getCharterView().getHtml(KiwaCharterView.Menu.BROWSER, accountId, locale, browserView.getHtml(menu, content, locale, account));

				// Display page.
				response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.getWriter().println(html);
			}
		}
		catch (Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
