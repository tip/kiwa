/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.myspace;

import java.util.Locale;

import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.TranslatorPresenter;
import fr.devinsy.xidyn.utils.XidynUtils;

/**
 *
 */
public class MySpaceView
{
	public enum Menu
	{
		NONE,
		MY_SPACE,
		MY_ACCOUNT,
		MY_STARTED_DATASETS,
		MY_SUBMITTED_DATASETS,
		MY_PUBLISHED_DATASETS,
		MY_REQUESTS,
		MY_COLLABORATIONS,
		MY_COLLABORATORS,

		HOW_TO_USE_KINSOURCES,
		HOW_TO_SUBMIT_A_DATASET,
		HOW_TO_CHOOSE_A_LICENSE,
		DATASET_FORMATS,
		TOOLS
	}

	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(MySpaceView.class);

	private static TranslatorPresenter page = new TranslatorPresenter("/org/kinsources/kiwa/website/myspace/my_space_view.html");

	/**
	 * 
	 */
	public StringBuffer getHtml(final Menu menu, final CharSequence content, final Locale locale) throws Exception
	{
		StringBuffer result;

		//
		TagDataManager data = new TagDataManager();

		//
		logger.debug("locale=[{}]", locale);

		data.setContent("my_space", "My status");
		data.setContent("my_account", "My account");
		data.setContent("my_started_datasets", "My started datasets");
		data.setContent("my_submitted_datasets", "My submitted datasets");
		data.setContent("my_published_datasets", "My published datasets");
		data.setContent("my_requests", "My sciboard requests");
		data.setContent("my_collaborators", "My collaborators");
		data.setContent("my_collaborations", "My collaborations");

		data.setContent("how_to_use_kinsources", "How to use Kinsources");
		data.setContent("how_to_submit_a_dataset", "How to submit a dataset");
		data.setContent("how_to_choose_a_license", "How to choose a license");
		data.setContent("dataset_formats", "Dataset formats");
		data.setContent("tools", "Tools");

		//
		switch (menu)
		{
			case MY_SPACE:
				data.setAttribute("my_space", "class", "active");
			break;
			case MY_ACCOUNT:
				data.setAttribute("my_account", "class", "active");
			break;
			case MY_STARTED_DATASETS:
				data.setAttribute("my_started_datasets", "class", "active");
			break;
			case MY_SUBMITTED_DATASETS:
				data.setAttribute("my_submitted_datasets", "class", "active");
			break;
			case MY_PUBLISHED_DATASETS:
				data.setAttribute("my_published_datasets", "class", "active");
			break;
			case MY_REQUESTS:
				data.setAttribute("my_requests", "class", "active");
			break;
			case MY_COLLABORATORS:
				data.setAttribute("my_collaborators", "class", "active");
			break;
			case MY_COLLABORATIONS:
				data.setAttribute("my_collaborations", "class", "active");
			break;
			default:
		}

		//
		data.setContent("browser_zone", XidynUtils.extractBodyContent(content));

		//
		result = page.dynamize(data);

		//
		return result;
	}
}

// ////////////////////////////////////////////////////////////////////////
