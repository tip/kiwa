/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.search;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kidarep.Dataset;
import org.kinsources.kiwa.kidarep.Datasets;
import org.kinsources.kiwa.seligo.Interval;
import org.kinsources.kiwa.seligo.SearchCriteria;
import org.kinsources.kiwa.seligo.SearchCriteria.DatasetFileScope;
import org.kinsources.kiwa.seligo.Seligo;
import org.kinsources.kiwa.stag.StatisticTag;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.charter.KiwaCharterView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.util.StringList;
import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Search_xhtml extends HttpServlet
{
	private static final long serialVersionUID = 3523141827401383261L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Search_xhtml.class);
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/search/result.html");
	private static final Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		logger.debug("doGet starting...");

		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.search.search", request);

			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);
			Long accountId = Account.getId(account);

			// Get parameters.
			// ===============
			String atlasCode = request.getParameter("atlas_code");
			logger.info("atlasCode=[{}]", XMLTools.unescapeXmlBlank(atlasCode));

			String author = request.getParameter("author");
			logger.info("author=[{}]", XMLTools.unescapeXmlBlank(author));

			String coder = request.getParameter("coder");
			logger.info("coder=[{}]", XMLTools.unescapeXmlBlank(coder));

			String continent = request.getParameter("continent");
			logger.info("continent=[{}]", XMLTools.unescapeXmlBlank(continent));

			String contributor = request.getParameter("contributor");
			logger.info("contributor=[{}]", XMLTools.unescapeXmlBlank(contributor));

			String datasetName = request.getParameter("dataset_name");
			logger.info("datasetName=[{}]", XMLTools.unescapeXmlBlank(datasetName));

			String country = request.getParameter("country");
			logger.info("country=[{}]", XMLTools.unescapeXmlBlank(country));

			String location = request.getParameter("location");
			logger.info("location=[{}]", XMLTools.unescapeXmlBlank(location));

			String organization = request.getParameter("organization");
			logger.info("organization=[{}]", XMLTools.unescapeXmlBlank(organization));

			String owner = request.getParameter("owner");
			logger.info("owner=[{}]", XMLTools.unescapeXmlBlank(owner));

			String region = request.getParameter("region");
			logger.info("region=[{}]", XMLTools.unescapeXmlBlank(region));

			String sortCriteria = request.getParameter("sort_by");
			logger.info("sortCriteria=[{}]", XMLTools.unescapeXmlBlank(sortCriteria));

			String datasetFileScopeParameter = request.getParameter("dataset_file");
			DatasetFileScope datasetFileScope = EnumUtils.getEnum(DatasetFileScope.class, datasetFileScopeParameter);
			logger.info("datasetFileScope=[{}]", datasetFileScope);

			String datasetTypeParameter = request.getParameter("dataset_type");
			Dataset.Type datasetType = EnumUtils.getEnum(Dataset.Type.class, datasetTypeParameter);
			logger.info("datasetType=[{}]", datasetType);

			String attributeDescriptorParameter = request.getParameter("attributes_descriptors");
			SearchCriteria.AttributeDescriptorFilter attributeDescriptorFilter = EnumUtils.getEnum(SearchCriteria.AttributeDescriptorFilter.class, attributeDescriptorParameter);
			logger.info("attributeDescriptorFilter=[{}]", attributeDescriptorFilter);

			String termsSelfTerm = request.getParameter("self_name");
			logger.info("termsSelfTerm=[{}]", XMLTools.unescapeXmlBlank(termsSelfTerm));

			String termsRecursive = request.getParameter("recursive_terms");
			logger.info("termRecursive=[{}]", XMLTools.unescapeXmlBlank(termsRecursive));

			String termsAscendantClassification = request.getParameter("terms_ascendant_classification");
			logger.info("termsAscendantClassification=[{}]", XMLTools.unescapeXmlBlank(termsAscendantClassification));

			String termsCousinClassification = request.getParameter("terms_cousin_classification");
			logger.info("termsCousinClassification=[{}]", XMLTools.unescapeXmlBlank(termsCousinClassification));

			// Use parameters.
			// ===============
			//
			SearchCriteria criteria = new SearchCriteria();
			criteria.setAtlasCode(atlasCode);
			criteria.setAuthor(author);
			criteria.setCoder(coder);
			criteria.setContinent(continent);
			criteria.setContributor(contributor);
			criteria.setDatasetName(datasetName);
			criteria.setCountry(country);
			criteria.setLocation(location);
			criteria.setOrganization(organization);
			criteria.setRegion(region);
			criteria.setDatasetFileScope(datasetFileScope);
			criteria.setAttributeDescriptor(attributeDescriptorFilter);
			criteria.setDatasetType(datasetType);

			for (StatisticTag stat : StatisticTag.values())
			{
				//
				String name = stat.name().toLowerCase();
				String min = XMLTools.unescapeXmlBlank(request.getParameter(name + "_min"));
				String max = XMLTools.unescapeXmlBlank(request.getParameter(name + "_max"));

				//
				if ((min != null) || (max != null))
				{
					logger.info("{}=[min={}][max={}]", name, min, max);
					criteria.intervals().add(name, min, max);
				}
			}

			criteria.setTermsSelfTerm(termsSelfTerm);
			criteria.setTermsRecursive(termsRecursive);
			criteria.setTermsAscendantClassification(termsAscendantClassification);
			criteria.setTermsCousinClassification(termsCousinClassification);

			//
			Datasets target = Seligo.search(accountId, criteria);

			// Make default sort.
			if (StringUtils.isBlank(sortCriteria))
			{
				target.sortByName();
			}
			else
			{
				target.sortBy(sortCriteria).reverse();
			}

			// Send response.
			// ==============
			TagDataManager data = new TagDataManager();

			//
			data.setContent("count", target.countOfDatasets());
			data.setContent("query", criteria.toString());

			if (criteria.isEmpty())
			{
				data.setContent("sort", "");
			}
			else
			{
				//
				int searchCriteriaIndex = 0;
				int sortCriteriaIndex = 0;
				if (StringUtils.isNotBlank(criteria.getAtlasCode()))
				{
					//
					data.setAttribute("sort_search_criterium", searchCriteriaIndex, "name", "atlas_code");
					data.setAttribute("sort_search_criterium", searchCriteriaIndex, "value", XMLTools.escapeXmlBlank(criteria.getAtlasCode()));
					searchCriteriaIndex += 1;

					//
					data.setContent("sort_criteria_option", sortCriteriaIndex, "Atlas code");
					data.setAttribute("sort_criteria_option", sortCriteriaIndex, "value", "atlas_code");
					if (StringUtils.equals(sortCriteria, "atlas_code"))
					{
						data.setAttribute("sort_criteria_option", sortCriteriaIndex, "selected", "selected");
					}
					sortCriteriaIndex += 1;
				}
				if (StringUtils.isNotBlank(criteria.getAuthor()))
				{
					//
					data.setAttribute("sort_search_criterium", searchCriteriaIndex, "name", "author");
					data.setAttribute("sort_search_criterium", searchCriteriaIndex, "value", XMLTools.unescapeXmlBlank(criteria.getAuthor()));
					searchCriteriaIndex += 1;

					//
					data.setContent("sort_criteria_option", sortCriteriaIndex, "Author");
					data.setAttribute("sort_criteria_option", sortCriteriaIndex, "value", "author");
					if (StringUtils.equals(sortCriteria, "author"))
					{
						data.setAttribute("sort_criteria_option", sortCriteriaIndex, "selected", "selected");
					}
					sortCriteriaIndex += 1;
				}
				if (StringUtils.isNotBlank(criteria.getCoder()))
				{
					//
					data.setAttribute("sort_search_criterium", searchCriteriaIndex, "name", "coder");
					data.setAttribute("sort_search_criterium", searchCriteriaIndex, "value", XMLTools.unescapeXmlBlank(criteria.getCoder()));
					searchCriteriaIndex += 1;

					//
					data.setContent("sort_criteria_option", sortCriteriaIndex, "Coder");
					data.setAttribute("sort_criteria_option", sortCriteriaIndex, "value", "coder");
					if (StringUtils.equals(sortCriteria, "coder"))
					{
						data.setAttribute("sort_criteria_option", sortCriteriaIndex, "selected", "selected");
					}
					sortCriteriaIndex += 1;
				}
				if (StringUtils.isNotBlank(criteria.getContinent()))
				{
					//
					data.setAttribute("sort_search_criterium", searchCriteriaIndex, "name", "continent");
					data.setAttribute("sort_search_criterium", searchCriteriaIndex, "value", XMLTools.unescapeXmlBlank(criteria.getContinent()));
					searchCriteriaIndex += 1;

					//
					data.setContent("sort_criteria_option", sortCriteriaIndex, "Continent");
					data.setAttribute("sort_criteria_option", sortCriteriaIndex, "value", "continent");
					if (StringUtils.equals(sortCriteria, "continent"))
					{
						data.setAttribute("sort_criteria_option", sortCriteriaIndex, "selected", "selected");
					}
					sortCriteriaIndex += 1;
				}
				if (criteria.getDatasetFileScope() != null)
				{
					data.setAttribute("sort_search_criterium", searchCriteriaIndex, "name", "dataset_file");
					data.setAttribute("sort_search_criterium", searchCriteriaIndex, "value", XMLTools.unescapeXmlBlank(criteria.getDatasetFileScope().toString()));
					searchCriteriaIndex += 1;
				}
				if (criteria.getDatasetType() != null)
				{
					data.setAttribute("sort_search_criterium", searchCriteriaIndex, "name", "dataset_type");
					data.setAttribute("sort_search_criterium", searchCriteriaIndex, "value", criteria.getDatasetType().name());
					searchCriteriaIndex += 1;
				}
				if (criteria.getAttributeDescriptorFilter() != null)
				{
					data.setAttribute("sort_search_criterium", searchCriteriaIndex, "name", "attributes_descriptors");
					data.setAttribute("sort_search_criterium", searchCriteriaIndex, "value", criteria.getAttributeDescriptorFilter().name());
					searchCriteriaIndex += 1;
				}
				if (StringUtils.isNotBlank(criteria.getContributor()))
				{
					//
					data.setAttribute("sort_search_criterium", searchCriteriaIndex, "name", "contributor");
					data.setAttribute("sort_search_criterium", searchCriteriaIndex, "value", XMLTools.unescapeXmlBlank(criteria.getContributor()));
					searchCriteriaIndex += 1;

					//
					data.setContent("sort_criteria_option", sortCriteriaIndex, "Contributor");
					data.setAttribute("sort_criteria_option", sortCriteriaIndex, "value", "contributor");
					if (StringUtils.equals(sortCriteria, "contributor"))
					{
						data.setAttribute("sort_criteria_option", sortCriteriaIndex, "selected", "selected");
					}
					sortCriteriaIndex += 1;
				}
				if (StringUtils.isNotBlank(criteria.getDatasetName()))
				{
					data.setAttribute("sort_search_criterium", searchCriteriaIndex, "name", "dataset_name");
					data.setAttribute("sort_search_criterium", searchCriteriaIndex, "value", XMLTools.unescapeXmlBlank(criteria.getDatasetName()));
					searchCriteriaIndex += 1;
				}
				{
					data.setContent("sort_criteria_option", sortCriteriaIndex, "Dataset name");
					data.setAttribute("sort_criteria_option", sortCriteriaIndex, "value", "dataset_name");
					if (StringUtils.equals(sortCriteria, "dataset_name"))
					{
						data.setAttribute("sort_criteria_option", sortCriteriaIndex, "selected", "selected");
					}
					sortCriteriaIndex += 1;
				}
				if (StringUtils.isNotBlank(criteria.getCountry()))
				{
					//
					data.setAttribute("sort_search_criterium", searchCriteriaIndex, "name", "country");
					data.setAttribute("sort_search_criterium", searchCriteriaIndex, "value", XMLTools.unescapeXmlBlank(criteria.getCountry()));
					searchCriteriaIndex += 1;

					//
					data.setContent("sort_criteria_option", sortCriteriaIndex, "Country");
					data.setAttribute("sort_criteria_option", sortCriteriaIndex, "value", "country");
					if (StringUtils.equals(sortCriteria, "country"))
					{
						data.setAttribute("sort_criteria_option", sortCriteriaIndex, "selected", "selected");
					}
					sortCriteriaIndex += 1;
				}
				if (StringUtils.isNotBlank(criteria.getLocation()))
				{
					//
					data.setAttribute("sort_search_criterium", searchCriteriaIndex, "name", "location");
					data.setAttribute("sort_search_criterium", searchCriteriaIndex, "value", XMLTools.unescapeXmlBlank(criteria.getLocation()));
					searchCriteriaIndex += 1;

					//
					data.setContent("sort_criteria_option", sortCriteriaIndex, "Location");
					data.setAttribute("sort_criteria_option", sortCriteriaIndex, "value", "location");
					if (StringUtils.equals(sortCriteria, "location"))
					{
						data.setAttribute("sort_criteria_option", sortCriteriaIndex, "selected", "selected");
					}
					sortCriteriaIndex += 1;
				}
				if (StringUtils.isNotBlank(criteria.getOrganization()))
				{
					//
					data.setAttribute("sort_search_criterium", searchCriteriaIndex, "name", "organization");
					data.setAttribute("sort_search_criterium", searchCriteriaIndex, "value", XMLTools.unescapeXmlBlank(criteria.getOrganization()));
					searchCriteriaIndex += 1;

					//
					data.setContent("sort_criteria_option", sortCriteriaIndex, "Organization");
					data.setAttribute("sort_criteria_option", sortCriteriaIndex, "value", "organization");
					if (StringUtils.equals(sortCriteria, "organization"))
					{
						data.setAttribute("sort_criteria_option", sortCriteriaIndex, "selected", "selected");
					}
					sortCriteriaIndex += 1;
				}
				if (StringUtils.isNotBlank(criteria.getRegion()))
				{
					//
					data.setAttribute("sort_search_criterium", searchCriteriaIndex, "name", "region");
					data.setAttribute("sort_search_criterium", searchCriteriaIndex, "value", XMLTools.unescapeXmlBlank(criteria.getRegion()));
					searchCriteriaIndex += 1;

					//
					data.setContent("sort_criteria_option", sortCriteriaIndex, "Region");
					data.setAttribute("sort_criteria_option", sortCriteriaIndex, "value", "region");
					if (StringUtils.equals(sortCriteria, "region"))
					{
						data.setAttribute("sort_criteria_option", sortCriteriaIndex, "selected", "selected");
					}
					sortCriteriaIndex += 1;
				}

				//
				for (Interval interval : criteria.intervals())
				{
					//
					if (interval.getMin() != Double.NEGATIVE_INFINITY)
					{
						data.setAttribute("sort_search_criterium", searchCriteriaIndex, "name", interval.getName() + "_min");
						data.setAttribute("sort_search_criterium", searchCriteriaIndex, "value", String.valueOf(interval.getMin()));
						searchCriteriaIndex += 1;
					}

					//
					if (interval.getMax() != Double.POSITIVE_INFINITY)
					{
						data.setAttribute("sort_search_criterium", searchCriteriaIndex, "name", interval.getName() + "_max");
						data.setAttribute("sort_search_criterium", searchCriteriaIndex, "value", String.valueOf(interval.getMax()));
						searchCriteriaIndex += 1;
					}

					//
					data.setContent("sort_criteria_option", sortCriteriaIndex, interval.getName());
					data.setAttribute("sort_criteria_option", sortCriteriaIndex, "value", interval.getName());
					if (StringUtils.equals(sortCriteria, interval.getName()))
					{
						data.setAttribute("sort_criteria_option", sortCriteriaIndex, "selected", "selected");
					}
					sortCriteriaIndex += 1;
				}
			}

			//
			if (target.isEmpty())
			{
				data.setContent("result", "No result found.");
			}
			else
			{
				int lineIndex = 0;
				for (Dataset dataset : target)
				{
					data.setContent("result", lineIndex, "dataset_id", dataset.getId());
					data.setContent("result", lineIndex, "dataset_name", XMLTools.escapeXmlBlank(dataset.getName()));
					data.setAttribute("result", lineIndex, "dataset_name", "href", kiwa.permanentURI(dataset));

					if (dataset.isGenealogy())
					{
						data.setAttribute("result", lineIndex, "dataset_type_icon", "src", "/charter/commons/dataset-genealogy.png");
						data.setAttribute("result", lineIndex, "dataset_type_icon", "title", "Genealogy");
					}
					else
					{
						data.setAttribute("result", lineIndex, "dataset_type_icon", "src", "/charter/commons/dataset-terminology.png");
						data.setAttribute("result", lineIndex, "dataset_type_icon", "title", "Terminology");
					}

					if (dataset.getOriginFile() == null)
					{
						data.setContent("result", lineIndex, "dataset_file_flag", "no");
					}
					else
					{
						data.setContent("result", lineIndex, "dataset_file_flag", "yes");
					}

					data.setContent("result", lineIndex, "dataset_attachment_count", dataset.attachments().size());

					if (dataset.getStatus() == Dataset.Status.VALIDATED)
					{
						data.setContent("result", lineIndex, "dataset_unpublished", "");
					}

					if (StringUtils.isBlank(dataset.getCoder()))
					{
						data.setContent("result", lineIndex, "dataset_coder", "");
					}
					else
					{
						data.setContent("result", lineIndex, "dataset_coder_link", XMLTools.escapeXmlBlank(dataset.getCoder()));
						data.setAttribute("result", lineIndex, "dataset_coder_link", "href", "/browser/field_value.xhtml?name=CODER&amp;value=" + XMLTools.escapeXmlBlank(dataset.getCoder()));
					}

					if (StringUtils.isBlank(dataset.getAuthor()))
					{
						data.setContent("result", lineIndex, "dataset_author", "");
					}
					else
					{
						data.setContent("result", lineIndex, "dataset_author_link", XMLTools.escapeXmlBlank(dataset.getAuthor()));
						data.setAttribute("result", lineIndex, "dataset_author_link", "href", "/browser/field_value.xhtml?name=AUTHOR&amp;value=" + XMLTools.escapeXmlBlank(dataset.getAuthor()));
					}

					if (dataset.isGenealogy())
					{
						data.setAttribute("result", lineIndex, "dataset_terminology_basics", "class", "xid:nodisplay");

						if (dataset.getOriginFile() == null)
						{
							data.setContent("result", lineIndex, "dataset_genealogy_basics_values", "n/a");
						}
						else
						{
							data.setContent("result", lineIndex, "dataset_individual_count", dataset.countOfIndividuals());
							data.setContent("result", lineIndex, "dataset_union_count", dataset.countOfUnions());
							data.setContent("result", lineIndex, "dataset_relation_count", dataset.countOfRelations());
							data.setContent("result", lineIndex, "dataset_generation_count", dataset.countOfGenerations());
						}
					}
					else
					{
						data.setAttribute("result", lineIndex, "dataset_genealogy_basics", "class", "xid:nodisplay");
						if (dataset.getOriginFile() == null)
						{
							data.setContent("result", lineIndex, "dataset_terminology_basics_values", "n/a");
						}
						else
						{
							data.setContent("result", lineIndex, "dataset_term_count", dataset.countOfTerms());
						}
					}

					if (StringUtils.isBlank(dataset.getAtlasCode()))
					{
						data.setContent("result", lineIndex, "dataset_atlas_code", "");
					}
					else
					{
						data.setContent("result", lineIndex, "dataset_atlas_code_link", XMLTools.escapeXmlBlank(dataset.getAtlasCode()));
						data.setAttribute("result", lineIndex, "dataset_atlas_code_link", "href",
								"/browser/field_value.xhtml?name=ATLAS_CODE&amp;value=" + XMLTools.escapeXmlBlank(dataset.getAtlasCode()));
					}

					if (StringUtils.isBlank(dataset.getLocation()))
					{
						data.setContent("result", lineIndex, "dataset_location", "");
					}
					else
					{
						data.setContent("result", lineIndex, "dataset_location_link", XMLTools.escapeXmlBlank(dataset.getLocation()));
						data.setAttribute("result", lineIndex, "dataset_location_link", "href", "/browser/field_value.xhtml?name=LOCATION&amp;value=" + XMLTools.escapeXmlBlank(dataset.getLocation()));
					}

					if (StringUtils.isBlank(dataset.getCountry()))
					{
						data.setContent("result", lineIndex, "dataset_country", "");
					}
					else
					{
						data.setContent("result", lineIndex, "dataset_country_link", XMLTools.escapeXmlBlank(dataset.getCountry()));
						data.setAttribute("result", lineIndex, "dataset_country_link", "href", "/browser/field_value.xhtml?name=COUNTRY&amp;value=" + XMLTools.escapeXmlBlank(dataset.getCountry()));
					}

					if (StringUtils.isBlank(dataset.getRegion()))
					{
						data.setContent("result", lineIndex, "dataset_region", "");
					}
					else
					{
						data.setContent("result", lineIndex, "dataset_region_link", XMLTools.escapeXmlBlank(dataset.getRegion()));
						data.setAttribute("result", lineIndex, "dataset_region_link", "href", "/browser/field_value.xhtml?name=REGION&amp;value=" + XMLTools.escapeXmlBlank(dataset.getRegion()));
					}

					data.setContent("result", lineIndex, "dataset_license", XMLTools.escapeXmlBlank(dataset.getLicense()));
					data.setContent("result", lineIndex, "dataset_short_description", XMLTools.escapeXmlBlank(dataset.getShortDescription()));

					data.setContent("result", lineIndex, "dataset_matching", Seligo.matching(criteria, dataset));

					//
					lineIndex += 1;
				}
			}

			//
			StringBuffer content = xidyn.dynamize(data);

			//
			StringBuffer html = kiwa.getCharterView().getHtml(KiwaCharterView.Menu.SEARCH, accountId, locale, content);

			// Display page.
			response.setContentType("application/xhtml+xml; charset=UTF-8");
			response.getWriter().println(html);
		}
		catch (Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		logger.debug("doPost starting...");

		//
		StringList target = new StringList(100);

		Enumeration<String> names = request.getParameterNames();
		while (names.hasMoreElements())
		{
			//
			String name = names.nextElement();

			//
			String value = request.getParameter(name);

			//
			if (StringUtils.isBlank(value))
			{
				value = null;
			}
			else if ((name.startsWith("rate_of")) || (name.startsWith("density_of")))
			{
				if ((value.equals("0")) && (name.endsWith("_min")))
				{
					value = null;
				}
				else if ((value.equals("100")) && (name.endsWith("_max")))
				{
					value = null;
				}
			}

			//
			if (value != null)
			{
				target.append("&").append(name).append("=").append(value);
			}
		}

		//
		if (!target.isEmpty())
		{
			target.set(0, "?");
		}

		logger.info("target1=[{}]", target.toString());
		logger.info("target2=[{}{}]", request.getRequestURI(), target.toString());

		//
		Redirector.redirect(response, (request.getRequestURI() + target.toString()).replace("#", "%23"));
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
