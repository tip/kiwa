/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.hico;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.hico.Article;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kernel.KiwaRoles;
import org.kinsources.kiwa.website.charter.ErrorView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Article_edition_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -1078407178204539829L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Article_edition_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/hico/article_edition.html");

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.hico.article_edition", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Account userAccount = kiwa.getAuthentifiedAccount(request, response);

			if ((userAccount == null) || (!userAccount.isRole(KiwaRoles.REDACTOR)))
			{
				Redirector.redirect(response, "/");
			}
			else
			{
				// Get parameters.
				// ===============
				String targetArticleIdParameter = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("target_id")));
				logger.info("targetArticleId=[{}]", targetArticleIdParameter);
				long targetArticleId = Long.parseLong(targetArticleIdParameter);

				// Use parameters.
				// ===============
				Article article = kiwa.hico().getArticleById(targetArticleId);

				if (article == null)
				{
					throw new IllegalArgumentException("Permission denied.");
				}
				else
				{

					TagDataManager data = new TagDataManager();

					//
					data.setContent("label_id", "ID:");
					data.setContent("id", article.getId());
					data.setAttribute("target_id", "value", article.getId());
					data.setContent("label_name", "Name:");
					data.setAttribute("name", "value", StringEscapeUtils.escapeXml(article.getName()));
					data.setContent("label_author", "Author:");
					if (article.getAuthor() != null)
					{
						data.setAttribute("author", "value", StringEscapeUtils.escapeXml(article.getAuthor()));
					}
					data.setContent("label_creation_date", "Creation date:");
					if (article.getCreationDate() != null)
					{
						data.setContent("creation_date", article.getCreationDate().toString("dd/MM/yyyy HH'h'mm"));
					}
					data.setContent("label_edition_date", "Edition date:");
					if (article.getEditionDate() != null)
					{
						data.setContent("edition_date", article.getEditionDate().toString("dd/MM/yyyy HH'h'mm"));
					}
					data.setContent("label_visibility", "Visibility:");
					if (article.isVisible())
					{
						data.setAttribute("visibility", "checked", "checked");
					}
					data.setContent("label_language", "Language:");
					data.setContent("label_text", "Text:");
					if (article.getText() != null)
					{
						data.setContent("text", StringEscapeUtils.escapeXml(article.getText()));
					}

					//
					int lineIndex = 0;
					for (Locale localeItem : kiwa.hico().locales())
					{
						//
						data.setContent("language_option", lineIndex, localeItem.getDisplayLanguage(locale));
						data.setAttribute("language_option", lineIndex, "value", localeItem.getLanguage());

						if (localeItem.getLanguage().equals(article.getLocale().getLanguage()))
						{
							data.setAttribute("language_option", lineIndex, "selected", "selected");
						}

						//
						lineIndex += 1;
					}

					//
					data.setAttribute("copy_id", "value", article.getId());
					data.setAttribute("remove_id", "value", article.getId());

					// Send response.
					// ==============
					StringBuffer content = xidyn.dynamize(data);

					//
					StringBuffer page = kiwa.getCharterView().getHtml(userAccount.getId(), locale, content);

					//
					// response.setContentType("application/xhtml+xml; charset=UTF-8");
					response.setContentType("text/html; charset=UTF-8");
					response.getWriter().println(page);

				}
			}
		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
