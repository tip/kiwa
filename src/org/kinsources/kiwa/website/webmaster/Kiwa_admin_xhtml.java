/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.webmaster;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kernel.Kiwa.Status;
import org.kinsources.kiwa.kernel.KiwaRoles;
import org.kinsources.kiwa.website.charter.ErrorView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Kiwa_admin_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8903536311059605390L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Kiwa_admin_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/webmaster/kiwa_admin.html");
	private static WebmasterView view = new WebmasterView();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.webmaster.kiwa_admin", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);

			if ((account == null) || (!account.isRole(KiwaRoles.WEBMASTER)))
			{
				Redirector.redirect(response, "/");
			}
			else
			{
				// Get parameters
				// ==============

				// Use parameters
				// ==============
				//
				TagDataManager data = new TagDataManager();

				//
				data.setContent("actalog_count", kiwa.actalog().eventLogs().size());
				data.setContent("actalog_last_id", kiwa.actalog().lastId());
				data.setContent("actilog_count", kiwa.actilog().logs().size());
				data.setContent("actilog_last_id", kiwa.actilog().lastId());
				data.setContent("actulog_count", kiwa.actulog().wires().size());
				data.setContent("actulog_last_id", kiwa.actulog().lastId());
				data.setContent("accounts_count", kiwa.accountManager().accounts().size());
				data.setContent("accounts_last_id", kiwa.accountManager().lastAccountId());

				data.setContent("agora_forum_count", kiwa.agora().countOfForums());
				data.setContent("agora_forum_last_id", kiwa.agora().lastForumId());
				data.setContent("agora_topic_count", kiwa.agora().countOfTopics());
				data.setContent("agora_topic_last_id", kiwa.agora().lastTopicId());
				data.setContent("agora_message_count", kiwa.agora().countOfMessages());
				data.setContent("agora_message_last_id", kiwa.agora().lastMessageId());

				data.setContent("eucles_mail_count", kiwa.eucles().countOfMails());
				data.setContent("eucles_waiting_count", kiwa.eucles().countOfWaitingMails());

				data.setContent("hico_article_count", kiwa.hico().countOfArticles());
				data.setContent("hico_article_last_id", kiwa.hico().lastArticleId());
				data.setContent("hico_bubble_count", kiwa.hico().bubbleManager().countOfBubbles());
				data.setContent("hico_bubble_last_id", kiwa.hico().bubbleManager().lastBubbleId());

				data.setContent("kidarep_contributor_count", kiwa.kidarep().countOfContributors());
				data.setContent("kidarep_contributor_last_id", kiwa.kidarep().lastContibutorId());
				data.setContent("kidarep_dataset_count", kiwa.kidarep().countOfDatasets());
				data.setContent("kidarep_dataset_last_id", kiwa.kidarep().lastDatasetId());
				data.setContent("kidarep_collection_count", kiwa.kidarep().countOfCollections());
				data.setContent("kidarep_collection_last_id", kiwa.kidarep().lastCollectionId());
				data.setContent("kidarep_collaborator_count", kiwa.kidarep().countOfCollaborators());
				data.setContent("kidarep_file_count", kiwa.kidarep().countOfFiles());
				data.setContent("kidarep_file_last_id", kiwa.kidarep().lastFileId());
				data.setContent("kidarep_dataset_file_count", kiwa.kidarep().countOfDatasetFiles());
				data.setContent("kidarep_dataset_file_last_id", kiwa.kidarep().lastFileId());
				data.setContent("kidarep_attachment_file_count", kiwa.kidarep().countOfAttachmentFiles());
				data.setContent("kidarep_attachment_file_last_id", kiwa.kidarep().lastFileId());
				data.setContent("kidarep_format_issue_count", kiwa.kidarep().countOfFormatIssues());
				data.setContent("kidarep_format_issue_last_id", kiwa.kidarep().lastFormatIssueId());
				data.setContent("kidarep_format_issue_ready_count", kiwa.kidarep().countOfReadyFormatIssues());

				data.setContent("sciboard_request_count", kiwa.sciboard().countOfRequests());
				data.setContent("sciboard_request_last_id", kiwa.sciboard().lastRequestId());
				data.setContent("sciboard_comment_count", kiwa.sciboard().countOfComments());
				data.setContent("sciboard_vote_count", kiwa.sciboard().countOfVotes());

				//
				int lineIndex = 0;
				for (Status status : Kiwa.Status.values())
				{
					//
					data.setContent("kiwa_status_option", lineIndex, status.toString());
					data.setAttribute("kiwa_status_option", lineIndex, "value", status.toString());

					//
					if (status == kiwa.getStatus())
					{
						data.setAttribute("kiwa_status_option", lineIndex, "selected", "selected");
					}

					//
					lineIndex += 1;
				}

				//
				String content = xidyn.dynamize(data).toString();

				// Send response
				// ============
				StringBuffer html = kiwa.getCharterView().getHtml(account.getId(), locale, view.getHtml(content));

				//
				response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.getWriter().println(html);
			}
		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		//
		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}
}

// ////////////////////////////////////////////////////////////////////////
