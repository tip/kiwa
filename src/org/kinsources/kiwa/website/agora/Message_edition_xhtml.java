/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.agora;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.agora.Message;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kernel.KiwaRoles;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.charter.KiwaCharterView;

import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Message_edition_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8988256363597317696L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Message_edition_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/agora/message_edition.html");

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.agora.message_edition", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Account userAccount = kiwa.getAuthentifiedAccount(request, response);

			if (userAccount == null)
			{
				throw new IllegalArgumentException("Permission denied.");
			}
			else
			{
				// Get parameters.
				// ===============
				String messageIdParameter = request.getParameter("message_id");
				logger.info("messageId=[{}]", messageIdParameter);
				long messageId = Long.parseLong(messageIdParameter);

				// Use parameters.
				// ===============
				//
				Message message = kiwa.agora().getMessageById(messageId);
				if (message == null)
				{
					throw new IllegalArgumentException("Unknown forum [" + messageId + "].");
				}
				else if ((Account.isNotRole(userAccount, KiwaRoles.MODERATOR)) && (Account.isNotRole(userAccount, KiwaRoles.WEBMASTER)) && (userAccount.getId() != message.getAuthorId()))
				{
					throw new IllegalArgumentException("Permission denied.");
				}
				else
				{
					//
					TagDataManager data = new TagDataManager();

					//
					data.setContent("path_forum_title", StringEscapeUtils.escapeXml(message.getTopic().getForum().getTitle()));
					data.setAttribute("path_forum_title", "href", "forum.xhtml?forum_id=" + message.getTopic().getForum().getId());
					data.setContent("path_topic_title", StringEscapeUtils.escapeXml(message.getTopic().getTitle()));
					data.setAttribute("path_topic_title", "value", "topic.xhtml?forum_id=" + message.getTopic().getId());
					data.setContent("path_message_title", "#" + message.getId());

					//
					data.setAttribute("message_id", "value", message.getId());

					data.setContent("label_id", "ID:");
					data.setContent("id", message.getId());

					data.setContent("label_author_name", "Author:");
					data.setContent("author_name", StringEscapeUtils.escapeXml(message.getAuthorName()));

					data.setContent("label_creation_date", "Creation date:");
					data.setContent("creation_date", message.getCreationDate().toString("dd/MM/yyyy HH:mm"));
					data.setContent("label_edition_date", "Edition date:");
					data.setContent("edition_date", message.getEditionDate().toString("dd/MM/yyyy HH:mm"));

					//
					data.setContent("label_text", "Text:");
					data.setContent("tips", "(5000 car.)");
					data.setContent("text", StringEscapeUtils.escapeXml(message.getText()));

					//
					data.setAttribute("back", "href", "topic.xhtml?topic_id=" + message.getTopic().getId());

					// Send response.
					// ==============
					StringBuffer content = xidyn.dynamize(data);

					//
					StringBuffer page = kiwa.getCharterView().getHtml(KiwaCharterView.Menu.FORUM, userAccount.getId(), locale, content);

					//
					// response.setContentType("application/xhtml+xml; charset=UTF-8");
					response.setContentType("text/html; charset=UTF-8");
					response.getWriter().println(page);
				}
			}
		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
