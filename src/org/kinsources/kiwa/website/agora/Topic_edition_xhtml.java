/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.agora;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringEscapeUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.agora.Forum;
import org.kinsources.kiwa.agora.Topic;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.charter.KiwaCharterView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Topic_edition_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8903536311059605390L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Topic_edition_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/agora/topic_edition.html");

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.agora.topic_edition", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Account userAccount = kiwa.getAuthentifiedAccount(request, response);

			if (Account.isNotActivated(userAccount))
			{
				Redirector.redirect(response, "/");
			}
			else
			{
				// Get parameters.
				// ===============
				String topicIdParameter = request.getParameter("topic_id");
				logger.info("topicId=[{}]", topicIdParameter);
				long topicId = Long.parseLong(topicIdParameter);

				// Use parameters.
				// ===============
				TagDataManager data = new TagDataManager();

				//
				Topic topic = kiwa.agora().getTopicById(topicId);
				if (topic == null)
				{
					throw new IllegalArgumentException("Unknown topic [" + topicId + "].");
				}
				else
				{
					//
					data.setContent("path_forum_title", StringEscapeUtils.escapeXml(topic.getForum().getTitle()));
					data.setAttribute("path_forum_title", "href", "forum.xhtml?forum_id=" + topic.getForum().getId());
					data.setContent("path_topic_title", StringEscapeUtils.escapeXml(topic.getTitle()));
					data.setAttribute("path_topic_title", "value", "topic.xhtml?forum_id=" + topicId);

					//
					data.setAttribute("topic_id", "value", topic.getId());

					//
					data.setContent("label_title", "Title:");
					data.setAttribute("title", "value", StringEscapeUtils.escapeXml(topic.getTitle()));

					//
					data.setContent("label_sticky", "Sticky:");

					//
					if (topic.isSticky())
					{
						data.setAttribute("sticky", "checked", "checked");
					}

					//
					data.setContent("label_status", "Status:");

					//
					if (topic.isOpened())
					{
						data.setAttribute("status_option_opened", "selected", "selected");
					}
					else
					{
						data.setAttribute("status_option_locked", "selected", "selected");
					}

					//
					data.setContent("label_forum", "Forum:");

					//
					int index = 0;
					for (Forum forum : kiwa.agora().forums())
					{
						//
						data.setContent("forum_option", index, StringEscapeUtils.escapeXml(forum.getTitle()));
						data.setAttribute("forum_option", index, "value", forum.getId());

						//
						if (forum.getId() == topic.getForum().getId())
						{
							data.setAttribute("forum_option", index, "selected", "selected");
						}

						//
						index += 1;
					}

					//
					data.setAttribute("back", "href", "topic.xhtml?topic_id=" + topicId);

					// Send response.
					// ==============
					StringBuffer content = xidyn.dynamize(data);

					//
					StringBuffer page = kiwa.getCharterView().getHtml(KiwaCharterView.Menu.FORUM, userAccount.getId(), locale, content);

					//
					response.setContentType("application/xhtml+xml; charset=UTF-8");
					response.getWriter().println(page);
				}
			}
		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
