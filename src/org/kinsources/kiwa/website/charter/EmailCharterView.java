/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.charter;

import java.util.Locale;
import java.util.ResourceBundle;

import org.kinsources.kiwa.kernel.Kiwa;

import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;
import fr.devinsy.xidyn.utils.XidynUtils;
import fr.devinsy.xidyn.views.CharterView;

/**
 *
 */
public class EmailCharterView implements CharterView
{
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(EmailCharterView.class);
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/charter/email_charter.html");
	private static Kiwa kiwa = Kiwa.instance();

	// static private FilePresenter xidyn = new
	// FilePresenter(Kiwa.instance().getWebContentPath() +
	// "/charter/charter.html");

	/**
	 * {@inheritDoc}
	 */
	@Override
	public StringBuffer getHtml() throws Exception
	{
		return xidyn.dynamize();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public StringBuffer getHtml(final Long accountId, final Locale locale, final CharSequence content) throws Exception
	{
		StringBuffer result;

		//
		TagDataManager data = new TagDataManager();

		// Extract start of content.
		String contentStart;
		if (content == null)
		{
			contentStart = null;
		}
		else if (content.length() < 7)
		{
			contentStart = content.toString();
		}
		else
		{
			contentStart = content.subSequence(0, 6).toString();
		}

		// Set content.
		String fixedContent;
		if ((contentStart != null) && ((contentStart.startsWith("<?")) || (contentStart.startsWith("<!")) || (contentStart.startsWith("<html>"))))
		{
			fixedContent = XidynUtils.extractBodyContent(content);
		}
		else
		{
			fixedContent = content.toString();
		}

		data.setContent("content_zone", fixedContent);

		//
		logger.debug("locale=[{}]", locale);

		ResourceBundle writer = ResourceBundle.getBundle("org.kinsources.kiwa.website.charter.email_charter", locale);

		//
		data.setAttribute("logo_link", "href", kiwa.getWebsiteUrl());
		data.setAttribute("logo", "src", kiwa.getWebsiteUrl() + "charter/kinsources-logo.png");

		//
		data.setContent("charter_about_kinsources", writer.getString("about_kinsources"));
		data.setAttribute("charter_about_kinsources", "href", kiwa.getWebsiteUrl() + "editorial/about_Kinsources.xhtml");
		data.setContent("charter_partners", writer.getString("partners"));
		data.setAttribute("charter_partners", "href", kiwa.getWebsiteUrl() + "editorial/partners.xhtml");
		data.setContent("charter_contact_us", writer.getString("contact_us"));
		data.setAttribute("charter_contact_us", "href", kiwa.getWebsiteUrl() + "editorial/contact_us.xhtml");
		data.setContent("charter_legal_information", writer.getString("legal_information"));
		data.setAttribute("charter_legal_information", "href", kiwa.getWebsiteUrl() + "editorial/legal.xhtml");
		data.setContent("charter_copyright", writer.getString("copyright"));

		//
		result = xidyn.dynamize(data);

		//
		return result;
	}
}

// ////////////////////////////////////////////////////////////////////////
