/**
 * Copyright 2013-2017 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.charter;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.kinsources.kiwa.kernel.Kiwa;

import fr.devinsy.util.StacktraceWriter;
import fr.devinsy.util.StringList;
import fr.devinsy.xidyn.utils.XidynUtils;

/**
 * 
 * @author Christian P. Momon
 */
public class ErrorView
{
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ErrorView.class);
	private static Kiwa kiwa = Kiwa.instance();

	/**
	 * 
	 * @throws IOException
	 */
	public static void show(final HttpServletRequest request, final HttpServletResponse response, final Exception source) throws IOException
	{
		try
		{
			logger.error("ERROR DETECTED", source);

			//
			Locale locale = kiwa.getUserLocale(request);
			Long accountId = kiwa.getAuthentifiedAccountId(request, response);

			//
			StringList content = new StringList(10);
			content.appendln("An error occured:<br/>");
			content.appendln("<pre>");
			content.appendln(source.getMessage());
			content.appendln("</pre>");
			content.appendln("<hr />");

			if (source.getMessage() == null)
			{
				content.appendln("<pre>No message</pre>");
			}
			else
			{
				content.appendln("<pre>" + XidynUtils.restoreEntities(new StringBuffer(source.getMessage())) + "</pre>");
			}
			content.appendln("<hr />");

			//
			StringBuffer page = kiwa.getCharterView().getHtml(accountId, locale, content.toString());

			//
			response.reset();
			response.setContentType("application/xhtml+xml; charset=UTF-8");
			response.getWriter().println(page);

		}
		catch (Exception exception)
		{
			logger.error("Exception detected during Error management.", exception);
			FatalView.show(request, response, source);
		}
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @param title
	 * @param message
	 * @throws IOException
	 */
	public static void show(final HttpServletRequest request, final HttpServletResponse response, final String title, final CharSequence message) throws IOException
	{
		try
		{
			//
			Locale locale = kiwa.getUserLocale(request);
			Long accountId = kiwa.getAuthentifiedAccountId(request, response);

			//
			StringList content = new StringList();
			content.append("<h1>").append(title).appendln("</h1>");
			content.append("<p>").append(message.toString()).appendln("</p>");
			content.append("<a class=\"button\" href=\"javascript: window.history.back()\">Back</a>");

			//
			StringBuffer page = Kiwa.instance().getCharterView().getHtml(accountId, locale, content.toString());

			//
			response.reset();
			response.setContentType("application/xhtml+xml; charset=UTF-8");
			response.getWriter().println(page);

		}
		catch (Exception exception)
		{
			logger.error(StacktraceWriter.toString(exception));
			FatalView.show(request, response, exception);
		}
	}

	/**
	 * 
	 * @param request
	 * @param response
	 * @param title
	 * @param message
	 * @param referer
	 * @throws IOException
	 */
	public static void show(final HttpServletRequest request, final HttpServletResponse response, final String title, final CharSequence message, final String referer) throws IOException
	{
		try
		{
			logger.error("ErrorView called: {}", title);

			//
			Locale locale = kiwa.getUserLocale(request);
			Long accountId = kiwa.getAuthentifiedAccountId(request, response);

			//
			StringList content = new StringList();
			content.append("<h1>").append(title).appendln("</h1>");
			content.append("<p>").append(message.toString()).appendln("</p>");
			content.append("<a class=\"button\" href=\"").append(referer.replace("&", "&amp;")).append("\" onclick=\"if (document.referrer != '') {window.history.back(); return false; };\">Back</a>");

			//
			StringBuffer page = Kiwa.instance().getCharterView().getHtml(accountId, locale, content.toString());

			//
			response.reset();
			response.setContentType("application/xhtml+xml; charset=UTF-8");
			response.getWriter().println(page);

		}
		catch (Exception exception)
		{
			//
			logger.error(StacktraceWriter.toString(exception));
			FatalView.show(request, response, exception);
		}
	}
}

// ////////////////////////////////////////////////////////////////////////
