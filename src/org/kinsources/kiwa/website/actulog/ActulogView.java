/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.actulog;

import java.util.Locale;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.kinsources.kiwa.actulog.Wire;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.utils.KiwaUtils;

import fr.devinsy.kiss4web.SimpleServletDispatcher;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class ActulogView
{
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(ActulogView.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static URLPresenter view = new URLPresenter("/org/kinsources/kiwa/website/actulog/actulog_view.html");

	/**
	 * 
	 */
	public ActulogView()
	{
	}

	/**
	 * {@inheritDoc}
	 */
	public StringBuffer getHtml(final Locale locale) throws Exception
	{
		StringBuffer result;

		//
		TagDataManager data = new TagDataManager();

		//
		logger.debug("locale=[{}]", locale);

		//
		data.setContent("actulog_view_title", "Latest Kinsources news");

		//
		Wire wire = kiwa.actulog().wires().latestPublishedWire(locale);

		//
		if (wire == null)
		{
			data.setContent("actulog_view_wire", "No news");
		}
		else
		{
			String link = SimpleServletDispatcher.rewriteShortUrl("/actulog/wire", "xhtml", String.valueOf(wire.getId()), StringEscapeUtils.escapeXml(wire.getTitle()));

			data.setContent("actulog_view_title", StringEscapeUtils.escapeXml(wire.getTitle()));
			data.setAttribute("actulog_view_title", "href", link);
			data.setContent("actulog_view_date", wire.getPublicationDate().toString("dd/MM/yyyy"));

			//
			String author;
			if (StringUtils.isBlank(wire.getAuthor()))
			{
				author = "";
			}
			else
			{
				author = StringEscapeUtils.escapeXml(wire.getAuthor());
			}
			data.setContent("actulog_view_author", author);

			//
			String begin;
			if (StringUtils.isBlank(wire.getLeadParagraph()))
			{
				if (StringUtils.isBlank(wire.getBody()))
				{
					//
					begin = "";
				}
				else
				{
					//
					begin = wire.getBody();
				}
			}
			else
			{
				begin = wire.getLeadParagraph();
			}

			//
			begin = begin.replaceAll("\\<.*?\\>", "");

			//
			if (begin.length() > 120)
			{
				//
				begin = begin.substring(0, 120) + "…";
			}
			data.setContent("actulog_view_begin", KiwaUtils.htmlizeWithoutBR(begin));

			//
			data.setContent("actulog_view_button", "READ MORE");
			data.setContent("actulog_wires_button", "MORE NEWS");
			data.setAttribute("actulog_view_button", "href", link);
		}

		//
		result = view.dynamize(data);

		//
		return result;
	}
}

// ////////////////////////////////////////////////////////////////////////
