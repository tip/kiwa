/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.kinsources.kiwa.actilog.Log.Level;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.welcome.Welcome_xhtml;

import fr.devinsy.util.StringList;

/**
 *
 */
public class Gone_page_xhtml extends HttpServlet
{
	private static final long serialVersionUID = 3716074540818963001L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Gone_page_xhtml.class);
	private static Welcome_xhtml welcome = new Welcome_xhtml();
	private static Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		//
		try
		{
			//
			logger.debug("doGet starting...");
			logger.info("Gone page for [{}]", request.getRequestURI());
			kiwa.logPageHit("pages.gone_page", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Long accountId = kiwa.getAuthentifiedAccountId(request, response);

			// Get parameters.
			// ===============

			// Use parameters.
			// ===============
			String referer = request.getHeader("referer");
			String userAgent = request.getHeader("User-Agent");
			String uri = request.getRequestURI();

			//
			kiwa.log(Level.INFO, "events.gone_page", "[accountId={}][uri={}][userAgent={}][referer={}]", accountId, uri, userAgent, referer);

			// Send response.
			// ==============
			StringList message = new StringList();

			message.appendln("<br/>");
			message.appendln("<br/>");
			message.appendln("<br/>");
			message.appendln("<br/>");
			message.appendln("<p>Dear visitor,</p>");
			message.appendln("<p>The Kinsources.net web site has been redesigned and the page you requested is no longer available.</p>");
			message.appendln("<p>Sorry for the inconvenience.</p>");
			message.appendln("<p>Please, visit our new website: <a href=\"/\" class=\"button\">Kinsources.net</a></p>");

			//
			StringBuffer html = kiwa.getCharterView().getHtml(accountId, locale, message);

			// Display page.
			response.setStatus(HttpServletResponse.SC_GONE);
			response.setContentType("application/xhtml+xml; charset=UTF-8");
			response.getWriter().println(html);
		}
		catch (Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
		super.init();
	}
}

// ////////////////////////////////////////////////////////////////////////
