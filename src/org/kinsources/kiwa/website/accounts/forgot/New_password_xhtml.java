/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.accounts.forgot;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.accounts.Account.Status;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.website.charter.ErrorView;

import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class New_password_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8903536311059605390L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(New_password_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/accounts/forgot/new_password.html");

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.accounts.forgot.new_password", request);

			// Get parameters.
			// ===============
			Locale locale = kiwa.getUserLocale(request);

			String accountIdParameter = StringUtils.trim(request.getParameter("account_id"));
			logger.info("[accountId={}]", accountIdParameter);

			String resetTimeParameter = StringUtils.trim(request.getParameter("reset_time"));
			logger.info("[resetTime={}]", resetTimeParameter);

			String resetKey = StringUtils.trim(request.getParameter("reset_key"));
			logger.info("[resetKey={}]", resetKey);

			// Use parameters.
			// ===============
			if ((StringUtils.isBlank(accountIdParameter)) || (StringUtils.isBlank(resetTimeParameter)) || (StringUtils.isBlank(resetKey)))
			{
				//
				throw new IllegalArgumentException("Bad parameters (nu).");

			}
			else
			{
				//
				String confirmKey = kiwa.securityAgent().computeAuth(accountIdParameter, resetTimeParameter);

				if (!StringUtils.equals(confirmKey, resetKey))
				{
					throw new IllegalArgumentException("Compromised parameters (rk).");
				}
				else
				{
					//
					long accountId = Long.parseLong(accountIdParameter);
					Account account = kiwa.accountManager().getAccountById(accountId);

					if (account == null)
					{
						throw new IllegalArgumentException("Bad parameter (au).");
					}
					else if (account.getStatus() != Status.ACTIVATED)
					{
						throw new IllegalArgumentException("Unuseful request.");
					}
					else
					{
						//
						long resetTime = Long.parseLong(resetTimeParameter);

						if (resetTime < account.password().editionDate().getMillis())
						{
							throw new IllegalArgumentException("Outdated resquest.");
						}
						else if (DateTime.now().isAfter(new DateTime(resetTime).plusMinutes(kiwa.getResetPasswordValidity())))
						{
							throw new IllegalArgumentException("Expired request.");
						}
						else
						{
							//
							TagDataManager data = new TagDataManager();

							//
							data.setAttribute("account_id", "value", accountIdParameter);
							data.setAttribute("reset_time", "value", resetTimeParameter);
							data.setAttribute("reset_key", "value", resetKey);
							data.setContent("label_password", "Password:");
							data.setAttribute("password", "pattern", Account.PASSWORD_PATTERN);
							data.setContent("label_password_confirmation", "Confirm password:");
							data.setAttribute("password_confirmation", "pattern", Account.PASSWORD_PATTERN);
							data.setAttribute("button_save", "value", "Save new password");

							// TODO set default timezone from
							// request.getLocale().getCountry().

							// Send response.
							// ==============
							StringBuffer content = xidyn.dynamize(data);

							//
							StringBuffer page = kiwa.getCharterView().getHtml(accountId, locale, content);

							//
							response.setContentType("application/xhtml+xml; charset=UTF-8");
							response.getWriter().println(page);
						}
					}
				}
			}

		}
		catch (IllegalArgumentException exception)
		{
			ErrorView.show(request, response, "Password reset issue", exception.getMessage(), "/");
		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
