/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.accounts.forgot;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.accounts.Account.Status;
import org.kinsources.kiwa.actilog.Log.Level;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.website.charter.ErrorView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.util.StringList;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.TranslatorPresenter;

/**
 *
 */
public class Reset_password_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8988256363597317696L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Reset_password_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static TranslatorPresenter mailPresenter = new TranslatorPresenter("/org/kinsources/kiwa/website/accounts/forgot/password_reset_mail.html");

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			kiwa.logPageHit("pages.accounts.forgot.reset_password", request);

			// Get parameters.
			// ===============
			Locale locale = kiwa.getUserLocale(request);

			String email = StringUtils.trim(request.getParameter("email"));
			logger.info("[email={}]", email);

			// Use parameters.
			// ===============
			kiwa.log(Level.INFO, "events.reset_password_resquest", "[email={}]", email);

			if (StringUtils.isBlank(email))
			{
				throw new IllegalArgumentException("Your email is mandatory.");
			}
			else if (!email.matches(Account.EMAIL_PATTERN))
			{
				throw new IllegalArgumentException("Incorrect email format.");
			}
			else
			{
				//
				Account account = kiwa.accountManager().getAccountByEmail(email);
				if ((account == null) || (account.getStatus() != Status.ACTIVATED))
				{
					throw new IllegalArgumentException("Incorrect email.");
				}
				else
				{
					//
					kiwa.log(Level.INFO, "events.reset_password_email", "[email={}]", email);

					//
					sendPasswordResetMail(request, locale, account);

					//
					Redirector.redirect(response, "password_reseting.xhtml");
				}
			}

		}
		catch (final IllegalArgumentException exception)
		{
			StringList message = new StringList();

			message.appendln("<p>" + exception.getMessage() + "</p>");
			message.appendln("<p>Please, try again.</p>");

			ErrorView.show(request, response, "Registration issue", message, "/accounts/forgot/forgotten_password.xhtml");

		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}

	/**
	 * 
	 * @param account
	 * @throws Exception
	 */
	private void sendPasswordResetMail(final HttpServletRequest request, final Locale locale, final Account account) throws Exception
	{
		// Build a key.
		String resetTime = String.valueOf(DateTime.now().getMillis());
		String resetKey = kiwa.securityAgent().computeAuth(String.valueOf(account.getId()), resetTime);

		// Build the mail content.
		TagDataManager data = new TagDataManager();

		//
		StringList resetLinkBuffer = new StringList();
		resetLinkBuffer.append(kiwa.getWebsiteUrl());
		resetLinkBuffer.append("accounts/forgot/new_password.xhtml");
		resetLinkBuffer.append("?account_id=").append(account.getId());
		resetLinkBuffer.append("&reset_time=").append(resetTime);
		resetLinkBuffer.append("&reset_key=").append(resetKey);

		String resetLink = resetLinkBuffer.toString();
		logger.debug("activationLink=[{}]", resetLink);

		//
		data.setAttribute("reset_link", "href", resetLink);

		//
		StringBuffer content = mailPresenter.dynamize(data);
		StringBuffer html = kiwa.getEmailCharterView().getHtml(account.getId(), locale, content);

		// Send activation email.
		kiwa.sendEmail(account.getEmail(), "[Kinsources] Password reset", html);
	}
}

// ////////////////////////////////////////////////////////////////////////
