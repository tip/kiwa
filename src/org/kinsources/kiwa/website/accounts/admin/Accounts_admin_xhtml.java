/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.accounts.admin;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.EnumUtils;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.accounts.Account.Status;
import org.kinsources.kiwa.accounts.Accounts;
import org.kinsources.kiwa.accounts.Role;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.kernel.KiwaRoles;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.webmaster.WebmasterView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.util.StringList;
import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.URLPresenter;

/**
 *
 */
public class Accounts_admin_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8903536311059605390L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Accounts_admin_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static URLPresenter xidyn = new URLPresenter("/org/kinsources/kiwa/website/accounts/admin/accounts_admin.html");
	private static WebmasterView view = new WebmasterView();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			logger.debug("doGet starting...");
			kiwa.logPageHit("pages.accounts.admin.accounts_admin", request);

			//
			Locale locale = kiwa.getUserLocale(request);
			Account userAccount = kiwa.getAuthentifiedAccount(request, response);

			if ((userAccount == null) || (!userAccount.isRole(KiwaRoles.WEBMASTER)))
			{
				Redirector.redirect(response, "/");
			}
			else
			{
				// Get parameters
				// ==============
				String roleNameFilter = request.getParameter("role_filter");
				logger.info("roleNameFilter=[{}]", roleNameFilter);

				String roleStatusFilter = request.getParameter("status_filter");
				logger.info("roleStatusFilter=[{}]", roleStatusFilter);

				// Use parameters
				// ==============
				TagDataManager data = new TagDataManager();

				//
				{
					data.setContent("count_status_created", kiwa.accountManager().accounts().count(Account.Status.CREATED));
					data.setContent("count_status_activated", kiwa.accountManager().accounts().count(Account.Status.ACTIVATED));
					data.setContent("count_status_suspended", kiwa.accountManager().accounts().count(Account.Status.SUSPENDED));
					data.setContent("count_status_removed", kiwa.accountManager().accounts().count(Account.Status.REMOVED));
					data.setContent("count_status_total", kiwa.accountManager().accounts().size());
				}

				//
				{
					int roleCount = 0;
					for (Role role : kiwa.accountManager().roles())
					{
						data.setContent("count_roles_row", roleCount, "count_roles_label", role.getName());
						data.setContent("count_roles_row", roleCount, "count_roles_value", kiwa.accountManager().accounts().findAccountsByRole(role).count(Account.Status.ACTIVATED));

						roleCount += 1;
					}
				}

				//
				Role roleFilter = kiwa.accountManager().roles().getByName(roleNameFilter);
				Status statusFilter = EnumUtils.getEnum(Account.Status.class, roleStatusFilter);

				Accounts accounts;
				if (roleFilter == null)
				{
					accounts = kiwa.accountManager().accounts().sortByFullName();
				}
				else
				{
					accounts = kiwa.accountManager().accounts().findAccountsByRole(roleFilter).sortByFullName();
				}
				if (statusFilter != null)
				{
					accounts = accounts.findAccountByStatus(statusFilter);
				}

				//
				data.setContent("role_filter", 0, "All");
				data.setAttribute("role_filter", 0, "value", "All");

				int roleCount = 1;
				for (Role role : kiwa.accountManager().roles().sortedList())
				{
					data.setContent("role_filter", roleCount, role.getName());
					data.setAttribute("role_filter", roleCount, "value", role.getName());

					if (role == roleFilter)
					{
						data.setAttribute("role_filter", roleCount, "selected", "selected");
					}

					roleCount += 1;
				}

				//
				data.setContent("status_filter", 0, "All");
				data.setAttribute("status_filter", 0, "value", "All");

				int statusCount = 1;
				for (Status status : Status.values())
				{
					if (status != Status.NOT_CREATED)
					{
						data.setContent("status_filter", statusCount, status.name());
						data.setAttribute("status_filter", statusCount, "value", status.name());

						if (status == statusFilter)
						{
							data.setAttribute("status_filter", statusCount, "selected", "selected");
						}

						statusCount += 1;
					}
				}

				//
				data.setContent("account_count", accounts.size());
				if (accounts.isEmpty())
				{
					data.setContent("account", "<td colspan=\"10\">No account with these filters.</td>");
				}
				else
				{
					int lineIndex = 0;
					for (Account account : accounts.sortByFullNameReversed())
					{
						//
						data.setContent("account", lineIndex, "account_id", account.getId());
						data.setContent("account", lineIndex, "account_firstnames", XMLTools.escapeXmlBlank(account.getFirstNames()));
						data.setAttribute("account", lineIndex, "account_firstnames", "href", "account_edition.xhtml?target_id=" + account.getId());
						data.setContent("account", lineIndex, "account_lastname", XMLTools.escapeXmlBlank(account.getLastName()));
						data.setAttribute("account", lineIndex, "account_lastname", "href", "account_edition.xhtml?target_id=" + account.getId());
						data.setContent("account", lineIndex, "account_email", XMLTools.escapeXmlBlank(account.getEmail()));
						data.setAttribute("account", lineIndex, "account_email", "href", "account_edition.xhtml?target_id=" + account.getId());
						data.setContent("account", lineIndex, "account_status", account.getStatus().toString());
						if (account.getCreationDate() != null)
						{
							data.setContent("account", lineIndex, "account_creation_date", account.getCreationDate().toString("dd/MM/yyyy"));
						}
						if (account.getLastConnection() != null)
						{
							data.setContent("account", lineIndex, "account_last_connection", account.getLastConnection().toString("dd/MM/yyyy"));
						}

						data.setContent("account", lineIndex, "account_roles", buildRoleString(account).toStringSeparatedBy("<br/>"));

						//
						lineIndex += 1;
					}
				}

				//
				String content = xidyn.dynamize(data).toString();

				// Send response
				// ============
				StringBuffer html = kiwa.getCharterView().getHtml(userAccount.getId(), locale, view.getHtml(content));

				//
				response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.getWriter().println(html);
			}
		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}

		//
		logger.debug("doGet done.");
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 * 
	 * @param source
	 * @return
	 */
	public static StringList buildRoleString(final Account source)
	{
		StringList result;

		//
		result = new StringList();

		if (Account.isRole(source, KiwaRoles.CONTRIBUTOR))
		{
			result.append("Contributor");
		}

		if (Account.isRole(source, KiwaRoles.SCIBOARDER))
		{
			result.append("Sciboarder");
		}

		if (Account.isRole(source, KiwaRoles.SCIBOARD_CHIEF))
		{
			result.append("Sci-chief");
		}

		if (Account.isRole(source, KiwaRoles.MODERATOR))
		{
			result.append("Moderator");
		}

		if (Account.isRole(source, KiwaRoles.REDACTOR))
		{
			result.append("Redactor");
		}

		if (Account.isRole(source, KiwaRoles.FORMAT_INSPECTOR))
		{
			result.append("Format&#160;inspector");
		}

		if (Account.isRole(source, KiwaRoles.WEBMASTER))
		{
			result.append("Webmaster");
		}

		//
		return result;
	}
}

// ////////////////////////////////////////////////////////////////////////
