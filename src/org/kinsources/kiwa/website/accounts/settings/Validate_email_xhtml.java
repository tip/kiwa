/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.accounts.settings;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.joda.time.DateTime;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.accounts.Account.Status;
import org.kinsources.kiwa.actilog.Log.Level;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.website.charter.ErrorView;

import fr.devinsy.kiss4web.Redirector;

/**
 *
 */
public class Validate_email_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8988256363597317696L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Validate_email_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			// Get parameters.
			// ===============
			Locale locale = kiwa.getUserLocale(request);

			//
			kiwa.logPageHit("pages.accounts.settings.validate_email", request);

			String idParameter = StringUtils.trim(request.getParameter("id"));
			logger.info("id=[{}]", idParameter);

			String newEmail = StringUtils.trim(request.getParameter("newEmail"));
			logger.info("newEmail=[{}]", newEmail);

			String activationTimeParameter = StringUtils.trim(request.getParameter("activation_time"));
			logger.info("activationTime=[{}]", activationTimeParameter);

			String activationKey = StringUtils.trim(request.getParameter("activation_key"));
			logger.info("emailConfirmation=[{}]", activationKey);

			// Use parameters.
			// ===============
			logger.info("CONFIRM_REGISTRATION for [id={}][newEmail={}][activationTime={}][activationKey={}]", idParameter, newEmail, activationTimeParameter, activationKey);

			//
			if ((StringUtils.isBlank(idParameter)) || (!NumberUtils.isNumber(idParameter)))
			{
				throw new IllegalArgumentException("Bad parameter (id).");
			}
			else if (StringUtils.isBlank(newEmail))
			{
				throw new IllegalArgumentException("Bad parameter (ne).");
			}
			else if ((StringUtils.isBlank(activationTimeParameter)) || (!NumberUtils.isNumber(activationTimeParameter)))
			{
				throw new IllegalArgumentException("Bad parameter (at).");
			}
			else if (StringUtils.isBlank(activationKey))
			{
				throw new IllegalArgumentException("Bad parameter (ak).");
			}
			else
			{
				//
				String confirmKey = kiwa.securityAgent().computeAuth(idParameter, newEmail, activationTimeParameter);

				//
				if (!StringUtils.equals(confirmKey, activationKey))
				{
					throw new IllegalArgumentException("Compromised parameters (ak).");
				}
				else
				{
					//
					int accountId = Integer.parseInt(idParameter);
					Account account = kiwa.accountManager().getAccountById(accountId);

					if (account == null)
					{
						throw new IllegalArgumentException("Bad parameter (an).");
					}
					else if (account.getStatus() != Status.ACTIVATED)
					{
						throw new IllegalArgumentException("Request previously submited. Your email is already activated.");
					}
					else
					{
						//
						long activationTime = Long.parseLong(activationTimeParameter);

						if (activationTime < account.getEditionDate().getMillis())
						{
							throw new IllegalArgumentException("Outdated resquest.");
						}
						else if (DateTime.now().isAfter(new DateTime(activationTime).plusMinutes(10)))
						{
							throw new IllegalArgumentException("Expired request.");
						}
						else if (kiwa.accountManager().getAccountByEmail(newEmail) != null)
						{
							throw new IllegalArgumentException("Email already in use.");
						}
						else
						{
							//
							kiwa.log(Level.INFO, "events.accounts.settings.new_email_validated", "[id={}][fullName={}][previousEmail={}][newEmail={}]", accountId, account.getFullName(),
									account.getEmail(), newEmail);

							//
							kiwa.updateAccountEmail(account, newEmail);

							//
							Redirector.redirect(response, "email_updated.xhtml");
						}
					}
				}
			}
		}
		catch (IllegalArgumentException exception)
		{
			ErrorView.show(request, response, "Registration issue", exception.getMessage(), "/");
		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}
}

// ////////////////////////////////////////////////////////////////////////
