/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.accounts.settings;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTimeZone;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.accounts.Account.EmailScope;
import org.kinsources.kiwa.actilog.Log.Level;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.website.charter.ErrorView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.util.StringList;
import fr.devinsy.util.xml.XMLTools;

/**
 *
 */
public class Update_account_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8988256363597317696L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Update_account_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			kiwa.logPageHit("pages.accounts.settings.update_account", request);

			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);

			if (account == null)
			{
				logger.debug("Invalid account.");
				Redirector.redirect(response, "/");
			}
			else
			{
				// Get parameters.
				// ===============
				String firstNames = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("first_names")));
				logger.info("firstNames=[{}]", firstNames);

				String lastName = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("last_name")));
				logger.info("lastName=[{}]", lastName);

				String password = StringUtils.trim(request.getParameter("password"));
				// Do not log the password.

				String passwordConfirmation = StringUtils.trim(request.getParameter("password_confirmation"));
				// Do not log the password.

				String organization = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("organization")));
				logger.info("organization=[{}]", organization);

				String aboutMe = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("about_me")));
				logger.info("aboutMe=[{}]", aboutMe);

				String website = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("website")));
				logger.info("website=[{}]", website);

				String country = XMLTools.unescapeXmlBlank(StringUtils.trim(request.getParameter("country")));
				logger.info("country=[{}]", country);

				String timeZoneParameter = StringUtils.trim(request.getParameter("timezone"));
				DateTimeZone timeZone = DateTimeZone.forID(timeZoneParameter);
				logger.info("timezone=[{}][{}]", timeZoneParameter, timeZone);

				String emailNotificationParameter = StringUtils.trim(request.getParameter("email_notification"));
				boolean emailNotification = Boolean.valueOf(emailNotificationParameter);
				logger.info("emailNotification=[{}][{}]", emailNotificationParameter, emailNotification);

				String emailScopeParameter = StringUtils.trim(request.getParameter("email_scope"));
				EmailScope emailScope;
				if (StringUtils.equalsIgnoreCase(emailScopeParameter, "public"))
				{
					emailScope = EmailScope.PUBLIC;
				}
				else
				{
					emailScope = EmailScope.PRIVATE;
				}
				logger.info("emailScope=[{}][{}]", emailScopeParameter, emailScope);

				// Use parameters.
				// ===============
				if ((StringUtils.isNotBlank(password)) && (!StringUtils.equals(password, passwordConfirmation)))
				{
					throw new IllegalArgumentException("Password and confirmation are different.");
				}
				else
				{
					logger.info("UPDATE ACCOUNT for login [{}]", account.getEmail());
					kiwa.log(Level.INFO, "events.accounts.settings.update_account", "[accountId={}]", account.getId());

					//
					kiwa.updateAccount(account, firstNames, lastName, passwordConfirmation, emailScope, organization, aboutMe, country, website, timeZone, emailNotification);

					//
					Redirector.redirect(response, "my_account.xhtml");
				}
			}
		}
		catch (final IllegalArgumentException exception)
		{
			StringList message = new StringList();

			message.appendln("<p>Your account edition is incomplete.</p>");
			message.appendln("<p>Please, fix the following items:</p>");
			message.appendln("<pre>" + XMLTools.escapeXmlBlank(exception.getMessage()) + "</pre>");

			ErrorView.show(request, response, "Account edition issue", message, "/accounts/registering/account_edition.xhtml");
		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}
}

// ////////////////////////////////////////////////////////////////////////
