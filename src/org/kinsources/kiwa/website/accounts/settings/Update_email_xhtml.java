/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.website.accounts.settings;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.actilog.Log.Level;
import org.kinsources.kiwa.kernel.Kiwa;
import org.kinsources.kiwa.website.charter.ErrorView;
import org.kinsources.kiwa.website.charter.FatalView;
import org.kinsources.kiwa.website.myspace.MySpaceView;

import fr.devinsy.kiss4web.Redirector;
import fr.devinsy.util.StacktraceWriter;
import fr.devinsy.util.StringList;
import fr.devinsy.util.xml.XMLTools;
import fr.devinsy.xidyn.data.TagDataManager;
import fr.devinsy.xidyn.presenters.TranslatorPresenter;

/**
 *
 */
public class Update_email_xhtml extends HttpServlet
{
	private static final long serialVersionUID = -8988256363597317696L;
	private static org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(Update_email_xhtml.class);
	private static Kiwa kiwa = Kiwa.instance();
	private static TranslatorPresenter mailPresenter = new TranslatorPresenter("/org/kinsources/kiwa/website/accounts/settings/email_validation_mail.html");
	private static MySpaceView mySpaceView = new MySpaceView();

	/**
	 *
	 */
	@Override
	public void doGet(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException
	{
		try
		{
			//
			kiwa.logPageHit("pages.accounts.settings.update_email", request);

			Locale locale = kiwa.getUserLocale(request);
			Account account = kiwa.getAuthentifiedAccount(request, response);

			if (account == null)
			{
				logger.debug("Invalid account.");
				Redirector.redirect(response, "/");
			}
			else
			{
				// Get parameters.
				// ===============
				String email = XMLTools.unescapeXmlBlank(StringUtils.lowerCase(StringUtils.trim(request.getParameter("email"))));
				logger.info("email=[{}]", email);

				String emailConfirmation = XMLTools.unescapeXmlBlank(StringUtils.lowerCase(StringUtils.trim(request.getParameter("email_confirmation"))));
				logger.info("emailConfirmation=[{}]", emailConfirmation);

				// Use parameters.
				// ===============
				if (StringUtils.isBlank(email))
				{
					throw new IllegalArgumentException("Invalid blank email.");
				}
				else if (!email.matches(Account.EMAIL_PATTERN))
				{
					throw new IllegalArgumentException("Invalid email format.");
				}
				else if (!StringUtils.equals(email, emailConfirmation))
				{
					throw new IllegalArgumentException("Your email and confirmation are differents.");
				}
				else if (kiwa.accountManager().getAccountByEmail(email) != null)
				{
					throw new IllegalArgumentException("Email already in use.");
				}
				else
				{
					logger.info("UPDATE EMAIL REQUEST for [accountId={}]", account.getId());

					//
					sendValidationMail(request, locale, account, email);

					kiwa.log(Level.INFO, "events.accounts.settings.send_validation_email", "[id={}][fullName={}][previousEmail={}][newEmail={}]", account.getId(), account.getFullName(),
							account.getEmail(), email);

					//
					Redirector.redirect(response, "email_updating.xhtml");
				}
			}
		}
		catch (final IllegalArgumentException exception)
		{
			try
			{
				//
				StringList content = new StringList();

				content.append("<html><body>");
				content.append("<h1>Account edition issue</h1>");
				content.appendln("<p>Your email edition is incomplete.</p>");
				content.appendln("<p>Please, fix the following items:</p>");
				content.appendln("<pre>" + XMLTools.escapeXmlBlank(exception.getMessage()) + "</pre>");
				content.append("<a class=\"button\" href=\"").append("/accounts/registering/email_edition.xhtml".replace("&", "&amp;"))
						.append("\" onclick=\"if (document.referrer != '') {window.history.back(); return false; };\">Back</a>");
				content.append("</body></html>");
				System.out.println("content=" + content.toString());

				Long accountId = kiwa.getAuthentifiedAccountId(request, response);
				Locale locale = kiwa.getUserLocale(request);
				StringBuffer page = Kiwa.instance().getCharterView().getHtml(accountId, locale, mySpaceView.getHtml(MySpaceView.Menu.MY_ACCOUNT, content.toString(), locale));

				//
				response.reset();
				response.setContentType("application/xhtml+xml; charset=UTF-8");
				response.getWriter().println(page);

			}
			catch (Exception subException)
			{
				logger.error(StacktraceWriter.toString(subException));
				FatalView.show(request, response, subException);
			}
		}
		catch (final Exception exception)
		{
			ErrorView.show(request, response, exception);
		}
	}

	/**
	 *
	 */
	@Override
	public void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException
	{
		doGet(request, response);
	}

	/**
	 *
	 */
	@Override
	public void init() throws ServletException
	{
	}

	/**
	 * 
	 * @param account
	 * @throws Exception
	 */
	private void sendValidationMail(final HttpServletRequest request, final Locale locale, final Account account, final String newEmail) throws Exception
	{
		// Build a key.
		String activationTime = String.valueOf(DateTime.now().getMillis());
		String activationKey = kiwa.securityAgent().computeAuth(String.valueOf(account.getId()), newEmail, activationTime);

		//
		StringList activationLinkBuffer = new StringList();
		activationLinkBuffer.append(kiwa.getWebsiteUrl());
		activationLinkBuffer.append("accounts/settings/validate_email.xhtml");
		activationLinkBuffer.append("?id=").append(account.getId());
		activationLinkBuffer.append("&newEmail=").append(newEmail);
		activationLinkBuffer.append("&activation_time=").append(activationTime);
		activationLinkBuffer.append("&activation_key=").append(activationKey);

		String validationLink = activationLinkBuffer.toString();
		logger.debug("emailValidationLink=[{}]", validationLink);

		// Build the mail content.
		TagDataManager data = new TagDataManager();

		//
		data.setAttribute("validation_link", "href", validationLink);
		data.setContent("full_name", XMLTools.escapeXmlBlank(account.getFullName()));
		data.setContent("current_email", XMLTools.escapeXmlBlank(account.getEmail()));
		data.setContent("new_email", XMLTools.escapeXmlBlank(newEmail));

		//
		StringBuffer content = mailPresenter.dynamize(data);
		StringBuffer html = kiwa.getEmailCharterView().getHtml(account.getId(), locale, content);

		// Send activation email.
		kiwa.sendEmail(newEmail, "[Kinsources] Validation of your new email.", html);
	}
}

// ////////////////////////////////////////////////////////////////////////
