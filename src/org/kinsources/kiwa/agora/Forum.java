/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.agora;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class Forum
{
	private long id;
	private String title;
	private String subtitle;
	private DateTime creationDate;
	private DateTime editionDate;
	private Topics topics;

	/**
	 * 
	 * 
	 * @param id
	 * @param title
	 * @param subtitle
	 */
	public Forum(final long id, final String title, final String subtitle)
	{
		if (StringUtils.isBlank(title))
		{
			throw new IllegalArgumentException("title is blank.");
		}
		else
		{
			this.id = id;
			this.title = title;
			this.subtitle = subtitle;
			this.creationDate = DateTime.now();
			this.editionDate = this.creationDate;
			this.topics = new Topics();
		}
	}

	/**
	 * 
	 * 
	 * @param id
	 * @param title
	 * @param subtitle
	 */
	public Forum(final long id, final String title, final String subtitle, final DateTime creationDate, final DateTime editionDate)
	{
		if (StringUtils.isBlank(title))
		{
			throw new IllegalArgumentException("title is blank.");
		}
		else if (creationDate == null)
		{
			throw new IllegalArgumentException("creationDate is null.");
		}
		else if (editionDate == null)
		{
			throw new IllegalArgumentException("editionDate is null.");
		}
		else
		{
			this.id = id;
			this.title = title;
			this.subtitle = subtitle;
			this.creationDate = creationDate;
			this.editionDate = editionDate;
			this.topics = new Topics();
		}
	}

	/**
	 * 
	 * @return
	 */
	public long countOfMessages()
	{
		long result;

		result = 0;
		for (Topic topic : this.topics)
		{
			result += topic.countOfMessages();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfTopics()
	{
		long result;

		result = this.topics.size();

		//
		return result;
	}

	public DateTime getCreationDate()
	{
		return this.creationDate;
	}

	public DateTime getEditionDate()
	{
		return this.editionDate;
	}

	public long getId()
	{
		return this.id;
	}

	public String getSubtitle()
	{
		return this.subtitle;
	}

	public String getTitle()
	{
		return this.title;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty()
	{
		boolean result;

		result = this.topics.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty()
	{
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Message latestMessage()
	{
		Message result;

		result = null;
		for (Topic topic : this.topics)
		{
			Message latest = topic.latestMessage();

			if (latest != null)
			{
				if ((result == null) || (result.getEditionDate().isBefore(latest.getEditionDate())))
				{
					result = latest;
				}
			}
		}

		//
		return result;
	}

	public void setCreationDate(final DateTime creationDate)
	{
		this.creationDate = creationDate;
	}

	public void setEditionDate(final DateTime editionDate)
	{
		this.editionDate = editionDate;
	}

	public void setId(final long id)
	{
		this.id = id;
	}

	public void setSubtitle(final String subtitle)
	{
		this.subtitle = subtitle;
	}

	public void setTitle(final String title)
	{
		this.title = title;
	}

	public Topics topics()
	{
		return this.topics;
	}
}
