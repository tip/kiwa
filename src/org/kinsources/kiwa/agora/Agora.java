/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.agora;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class Agora
{
	private long lastForumId;
	private Forums forums;
	private ForumIdIndex forumIdIndex;
	private long lastTopicId;
	private TopicIdIndex topicIdIndex;
	private long lastMessageId;
	private MessageIdIndex messageIdIndex;

	/**
	 * 
	 */
	public Agora()
	{
		this.lastForumId = 0;
		this.lastTopicId = 0;
		this.lastMessageId = 0;
		this.forums = new Forums();
		this.forumIdIndex = new ForumIdIndex();
		this.topicIdIndex = new TopicIdIndex();
		this.messageIdIndex = new MessageIdIndex();
	}

	/**
	 * 
	 */
	public Agora(final long lastForumId, final long lastTopicId, final long lastMessageId)
	{
		this.lastForumId = lastForumId;
		this.lastMessageId = lastMessageId;
		this.lastTopicId = lastTopicId;
		this.forums = new Forums();
		this.forumIdIndex = new ForumIdIndex();
		this.topicIdIndex = new TopicIdIndex();
		this.messageIdIndex = new MessageIdIndex();
	}

	/**
	 * 
	 */
	public void clear()
	{
		//
		this.lastForumId = 0;
		this.lastMessageId = 0;
		this.lastTopicId = 0;
		this.forums.clear();
		this.forumIdIndex.clear();
		this.topicIdIndex.clear();
		this.messageIdIndex.clear();
	}

	/**
	 * 
	 * @return
	 */
	public long countOfForums()
	{
		long result;

		result = this.forums().size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfMessages()
	{
		int result;

		result = 0;
		for (Forum forum : this.forums)
		{
			result += forum.countOfMessages();
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long countOfTopics()
	{
		long result;

		result = 0;
		for (Forum forum : this.forums)
		{
			result += forum.countOfTopics();
		}

		//
		return result;
	}

	/**
	 * 
	 * @param title
	 * @param subtitle
	 * @return
	 */
	synchronized public Forum createForum(final String title, final String subtitle)
	{
		Forum result;

		result = new Forum(nextForumId(), title, subtitle);
		this.forums.add(result);
		this.forumIdIndex.put(result);

		//
		return result;
	}

	/**
	 * 
	 */
	synchronized public Message createMessage(final Topic topic, final long authorId, final String authorName, final String text)
	{
		Message result;

		//
		result = new Message(nextMessageId(), topic, authorId, authorName, text);

		//
		topic.messages().add(result);

		//
		this.messageIdIndex.put(result);

		//
		return result;
	}

	/**
	 * 
	 * @param title
	 * @param subtitle
	 * @return
	 */
	synchronized public Topic createTopic(final Forum parent, final String title)
	{
		Topic result;

		//
		result = new Topic(nextTopicId(), parent, title);

		//
		parent.topics().add(result);

		//
		this.topicIdIndex.put(result);

		//
		return result;
	}

	/**
	 * 
	 * @param forumId
	 */
	synchronized public void downForum(final Forum forum)
	{
		//
		this.forums.down(forum);
	}

	public Forums forums()
	{
		return this.forums;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Forum getForumById(final long id)
	{
		Forum result;

		result = this.forumIdIndex.getById(id);

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Message getMessageById(final long id)
	{
		Message result;

		result = this.messageIdIndex.getById(id);

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 * @return
	 */
	public Topic getTopicById(final long id)
	{
		Topic result;

		result = this.topicIdIndex.getById(id);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long lastForumId()
	{
		long result;

		result = this.lastForumId;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long lastMessageId()
	{
		return this.lastMessageId;
	}

	/**
	 * 
	 * @return
	 */
	public long lastTopicId()
	{
		return this.lastTopicId;
	}

	/**
	 * 
	 * @return
	 */
	synchronized public long nextForumId()
	{
		long result;

		this.lastForumId += 1;

		result = this.lastForumId;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	synchronized public long nextMessageId()
	{
		long result;

		this.lastMessageId += 1;

		result = this.lastMessageId;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	synchronized public long nextTopicId()
	{
		long result;

		this.lastTopicId += 1;

		result = this.lastTopicId;

		//
		return result;
	}

	/**
	 * 
	 */
	public void rebuildForumIndexes()
	{
		this.forumIdIndex.rebuild(this.forums);
	}

	/**
	 * 
	 */
	public void rebuildIndexes()
	{
		rebuildForumIndexes();
		rebuildTopicIndexes();
		rebuildMessageIndexes();
	}

	/**
	 * 
	 */
	public void rebuildMessageIndexes()
	{
		for (Forum forum : this.forums)
		{
			for (Topic topic : forum.topics())
			{
				this.messageIdIndex.put(topic.messages());
			}
		}
	}

	/**
	 * 
	 */
	public void rebuildTopicIndexes()
	{
		for (Forum forum : this.forums)
		{
			this.topicIdIndex.put(forum.topics());
		}
	}

	/**
	 * 
	 * @param topic
	 */
	public void removeForum(final Forum forum)
	{
		//
		this.forums().remove(forum);

		//
		this.forumIdIndex.remove(forum);
	}

	/**
	 * 
	 * @param topic
	 */
	public void removeMessage(final Message message)
	{
		//
		message.getTopic().messages().remove(message);

		//
		this.messageIdIndex.remove(message);
	}

	/**
	 * 
	 * @param topic
	 */
	public void removeTopic(final Topic topic)
	{
		//
		topic.getForum().topics().remove(topic);

		//
		this.topicIdIndex.remove(topic);
	}

	/**
	 * 
	 */
	public void resetLastIds()
	{
		this.lastForumId = this.forums.lastId();
		this.lastTopicId = this.forums.lastTopicId();
		this.lastMessageId = this.forums.lastMessageId();
	}

	/**
	 * 
	 * @param forumId
	 */
	synchronized public void upForum(final Forum forum)
	{
		//
		this.forums.up(forum);
	}
}
