/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.agora;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class Forums implements Iterable<Forum>
{
	private ArrayList<Forum> forums;

	/**
	 * 
	 */
	public Forums()
	{
		this.forums = new ArrayList<Forum>();
	}

	/**
	 * 
	 */
	public Forums(final int initialCapacity)
	{
		this.forums = new ArrayList<Forum>(initialCapacity);
	}

	/**
	 * 
	 * @param log
	 */
	public void add(final Forum forum)
	{
		//
		if (forum == null)
		{
			throw new IllegalArgumentException("forum is null.");
		}
		else if (forum.getTitle() == null)
		{
			throw new IllegalArgumentException("Title is null.");
		}
		else if (forum.getCreationDate() == null)
		{
			throw new IllegalArgumentException("Creation date is null.");
		}
		else if (forum.getEditionDate() == null)
		{
			throw new IllegalArgumentException("Edition date is null.");
		}
		else
		{
			this.forums.add(forum);
		}
	}

	/**
	 * 
	 */
	public void clear()
	{
		this.forums.clear();
	}

	/**
	 * This methods returns a shallow copy of the current object.
	 * 
	 * @return a shallow copy of the current object.
	 */
	public Forums copy()
	{
		Forums result;

		//
		result = new Forums(this.forums.size());

		//
		for (Forum forum : this.forums)
		{
			result.add(forum);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param forumId
	 */
	public void down(final Forum forum)
	{

		if (forum == null)
		{
			throw new IllegalArgumentException("forum is null.");
		}
		else
		{
			int index = this.forums.indexOf(forum);
			if (index == -1)
			{
				throw new IllegalArgumentException("Unknown forum [" + forum.getId() + "]");
			}
			else if (index < this.forums.size() - 1)
			{
				swap(index, index + 1);
			}
		}
	}

	/**
	 * 
	 * @param targetCount
	 * @return
	 */
	public Forums first(final int targetCount)
	{
		Forums result;

		//
		result = new Forums(targetCount);

		//
		boolean ended = false;
		Iterator<Forum> iterator = iterator();
		int count = 0;
		while (!ended)
		{
			if ((count > targetCount) || (!iterator.hasNext()))
			{
				ended = true;
			}
			else
			{
				result.add(iterator.next());
				count += 1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param index
	 * @return
	 */
	public Forum getByIndex(final int index)
	{
		Forum result;

		result = this.forums.get(index);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty()
	{
		boolean result;

		result = this.forums.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty()
	{
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Iterator<Forum> iterator()
	{
		Iterator<Forum> result;

		result = this.forums.iterator();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	synchronized public long lastId()
	{
		long result;

		//
		result = 0;
		for (Forum forum : this.forums)
		{
			if (forum.getId() > result)
			{
				result = forum.getId();
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	synchronized public long lastMessageId()
	{
		long result;

		//
		result = 0;
		for (Forum forum : this.forums)
		{
			for (Topic topic : forum.topics())
			{
				for (Message message : topic.messages())
				{
					if (message.getId() > result)
					{
						result = message.getId();
					}
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	synchronized public long lastTopicId()
	{
		long result;

		//
		result = 0;
		for (Forum forum : this.forums)
		{
			for (Topic topic : forum.topics())
			{
				if (topic.getId() > result)
				{
					result = topic.getId();
				}
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 */
	synchronized public void remove(final Forum forum)
	{
		this.forums.remove(forum);
	}

	/**
	 * 
	 * @return
	 */
	public Forums reverse()
	{
		Forums result;

		//
		Collections.reverse(this.forums);

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int size()
	{
		int result;

		result = this.forums.size();

		//
		return result;
	}

	/**
	 * 
	 * @param forumId
	 */
	public void swap(final int alpha, final int bravo)
	{

		Forum pivot = this.forums.get(alpha);
		this.forums.set(alpha, this.forums.get(bravo));
		this.forums.set(bravo, pivot);
	}

	/**
	 * 
	 * @param forumId
	 */
	public void up(final Forum forum)
	{

		if (forum == null)
		{
			throw new IllegalArgumentException("forum is null.");
		}
		else
		{
			int index = this.forums.indexOf(forum);
			if (index == -1)
			{
				throw new IllegalArgumentException("Unknown forum [" + forum.getId() + "]");
			}
			else if (index > 0)
			{
				swap(index, index - 1);
			}
		}
	}
}
