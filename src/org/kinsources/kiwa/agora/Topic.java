/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.agora;

import org.joda.time.DateTime;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class Topic
{
	public enum Status
	{
		OPENED,
		LOCKED
	}

	private long id;
	private Forum forum;
	private String title;
	private Messages messages;
	private boolean sticky;
	private Status status;

	/**
	 * 
	 */
	public Topic(final long id, final Forum parent, final String title)
	{
		this.id = id;
		this.forum = parent;
		this.title = title;
		this.status = Status.OPENED;
		this.sticky = false;
		this.messages = new Messages();
	}

	/**
	 * 
	 */
	public Topic(final long id, final Forum parent, final String title, final Status status, final boolean sticky)
	{
		this.id = id;
		this.forum = parent;
		this.title = title;
		this.status = status;
		this.sticky = sticky;
		this.messages = new Messages();
	}

	/**
	 * 
	 * @return
	 */
	public long countOfMessages()
	{
		long result;

		result = this.messages.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public long getAuthorId()
	{
		long result;

		result = this.messages.getByIndex(0).getAuthorId();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public String getAuthorName()
	{
		String result;

		result = this.messages.getByIndex(0).getAuthorName();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public DateTime getCreationDate()
	{
		DateTime result;

		result = this.messages.getByIndex(0).getCreationDate();

		//
		return result;
	}

	public Forum getForum()
	{
		return this.forum;
	}

	public long getId()
	{
		return this.id;
	}

	public Status getStatus()
	{
		return this.status;
	}

	public String getTitle()
	{
		return this.title;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isLocked()
	{
		boolean result;

		if (this.status == Status.LOCKED)
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotSticky()
	{
		boolean result;

		result = !isSticky();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isOpened()
	{
		boolean result;

		if (this.status == Status.OPENED)
		{
			result = true;
		}
		else
		{
			result = false;
		}

		//
		return result;
	}

	public boolean isSticky()
	{
		return this.sticky;
	}

	/**
	 * 
	 * @return
	 */
	public Message latestMessage()
	{
		Message result;

		result = this.messages.last();

		//
		return result;
	}

	public Messages messages()
	{
		return this.messages;
	}

	public void setForum(final Forum forum)
	{
		this.forum = forum;
	}

	public void setId(final long id)
	{
		this.id = id;
	}

	public void setStatus(final Status status)
	{
		this.status = status;
	}

	public void setSticky(final boolean sticky)
	{
		this.sticky = sticky;
	}

	public void setTitle(final String title)
	{
		this.title = title;
	}
}
