/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.agora;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

import org.kinsources.kiwa.agora.MessageComparator.Criteria;

/**
 * 
 * @author christian.momon@devinsy.fr
 */
public class Messages implements Iterable<Message>
{
	private ArrayList<Message> messages;

	/**
	 * 
	 */
	public Messages()
	{
		this.messages = new ArrayList<Message>();
	}

	/**
	 * 
	 */
	public Messages(final int initialCapacity)
	{
		this.messages = new ArrayList<Message>(initialCapacity);
	}

	/**
	 * 
	 * @param log
	 */
	public void add(final Message message)
	{
		//
		if (message == null)
		{
			throw new IllegalArgumentException("Message is null.");
		}
		else if (message.getAuthorName() == null)
		{
			throw new IllegalArgumentException("author is null.");
		}
		else
		{
			this.messages.add(message);
		}
	}

	/**
	 * 
	 */
	public void clear()
	{
		this.messages.clear();
	}

	/**
	 * This methods returns a shallow copy of the current object.
	 * 
	 * @return a shallow copy of the current object.
	 */
	public Messages copy()
	{
		Messages result;

		//
		result = new Messages(this.messages.size());

		//
		for (Message Message : this.messages)
		{
			result.add(Message);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param targetCount
	 * @return
	 */
	public Messages first(final int targetCount)
	{
		Messages result;

		//
		result = new Messages(targetCount);

		//
		boolean ended = false;
		Iterator<Message> iterator = iterator();
		int count = 0;
		while (!ended)
		{
			if ((count > targetCount) || (!iterator.hasNext()))
			{
				ended = true;
			}
			else
			{
				result.add(iterator.next());
				count += 1;
			}
		}

		//
		return result;
	}

	/**
	 * 
	 * @param index
	 * @return
	 */
	public Message getByIndex(final int index)
	{
		Message result;

		result = this.messages.get(index);

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isEmpty()
	{
		boolean result;

		result = this.messages.isEmpty();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public boolean isNotEmpty()
	{
		boolean result;

		result = !isEmpty();

		//
		return result;
	}

	/**
	 * 
	 */
	@Override
	public Iterator<Message> iterator()
	{
		Iterator<Message> result;

		result = this.messages.iterator();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Message last()
	{
		Message result;

		if (this.messages.isEmpty())
		{
			result = null;
		}
		else
		{
			result = this.messages.get(this.messages.size() - 1);
		}

		//
		return result;
	}

	/**
	 * 
	 * @param id
	 */
	synchronized public void remove(final Message message)
	{
		this.messages.remove(message);
	}

	/**
	 * 
	 * @return
	 */
	public Messages reverse()
	{
		Messages result;

		//
		Collections.reverse(this.messages);

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public int size()
	{
		int result;

		result = this.messages.size();

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Messages sortByDate()
	{
		Messages result;

		//
		Collections.sort(this.messages, new MessageComparator(Criteria.EDITION_DATE));

		//
		result = this;

		//
		return result;
	}

	/**
	 * 
	 * @return
	 */
	public Messages sortById()
	{
		Messages result;

		//
		Collections.sort(this.messages, new MessageComparator(Criteria.ID));

		//
		result = this;

		//
		return result;
	}
}
