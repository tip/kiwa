/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.utils;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author Christian P. Momon
 */
public class PlafUtilsTest
{
	protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(PlafUtilsTest.class);

	/**
	 * 
	 */
	@Before
	public void before()
	{
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(org.apache.log4j.Level.INFO);
	}

	/**
	 * 
	 */
	@Test
	public void testSplit01()
	{
		//
		logger.debug("===== test starting...");

		//
		String[] source = new String[12];
		source[0] = "alpha";
		source[1] = "bravo";
		source[2] = "charlie";
		source[3] = "delta";
		source[4] = "echo";
		source[5] = "fox";
		source[6] = "golf";
		source[7] = "hotel";
		source[8] = "india";
		source[9] = "juliet";
		source[10] = "kilo";
		source[11] = "mike";

		//
		String[][] target = KiwaUtils.split(source, 5);

		//
		Assert.assertEquals(3, target.length);
		Assert.assertEquals(5, target[0].length);
		Assert.assertEquals(5, target[1].length);
		Assert.assertEquals(2, target[2].length);

		//
		logger.debug("===== test done.");
	}
}
