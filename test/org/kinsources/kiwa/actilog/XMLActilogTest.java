/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.actilog;

import java.io.UnsupportedEncodingException;

import javax.xml.stream.XMLStreamException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.kinsources.kiwa.actilog.Log.Level;

import fr.devinsy.util.xml.XMLBadFormatException;

/**
 * 
 * @author Christian P. Momon
 */
public class XMLActilogTest
{
	protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(XMLActilogTest.class);

	/**
	 * 
	 */
	@Before
	public void before()
	{
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(org.apache.log4j.Level.INFO);
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testActilog01() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Actilog source = new Actilog();

		//
		String xml = XMLActilog.toXMLString(source);

		logger.debug("xml=[" + xml + "]");

		//
		Actilog target = XMLActilog.readActilog(xml);

		//
		Assert.assertEquals(source.getLogLevel(), target.getLogLevel());
		Assert.assertEquals(source.logs().size(), target.logs().size());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testActilog02() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Actilog source = new Actilog();

		source.log(Level.DEBUG, "test.alpha", null);
		source.log(Level.DEBUG, "test.bravo", "detail from bravo");

		//
		String xml = XMLActilog.toXMLString(source);

		logger.debug("xml=[" + xml + "]");

		//
		Actilog target = XMLActilog.readActilog(xml);

		//
		Assert.assertEquals(source.getLogLevel(), target.getLogLevel());
		Assert.assertEquals(source.logs().size(), target.logs().size());

		Assert.assertEquals(source.logs().getByIndex(0).getTime().toString(), target.logs().getByIndex(0).getTime().toString());
		Assert.assertEquals(source.logs().getByIndex(0).getLevel(), target.logs().getByIndex(0).getLevel());
		Assert.assertEquals(source.logs().getByIndex(0).getEvent(), target.logs().getByIndex(0).getEvent());
		Assert.assertEquals(source.logs().getByIndex(0).getDetails(), target.logs().getByIndex(0).getDetails());

		Assert.assertEquals(source.logs().getByIndex(1).getTime().toString(), target.logs().getByIndex(1).getTime().toString());
		Assert.assertEquals(source.logs().getByIndex(1).getLevel(), target.logs().getByIndex(1).getLevel());
		Assert.assertEquals(source.logs().getByIndex(1).getEvent(), target.logs().getByIndex(1).getEvent());
		Assert.assertEquals(source.logs().getByIndex(1).getDetails(), target.logs().getByIndex(1).getDetails());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testLog01() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Log source = new Log(17, DateTime.now(), Level.INFO, "test.alpha", (String) null);

		//
		String xml = XMLActilog.toXMLString(source);

		logger.debug("xml=[" + xml + "]");

		Log target = XMLActilog.readLog(xml);

		//
		Assert.assertEquals(source.getId(), target.getId());
		Assert.assertEquals(source.getTime().toString(), target.getTime().toString());
		Assert.assertEquals(source.getLevel(), target.getLevel());
		Assert.assertEquals(source.getEvent(), target.getEvent());
		Assert.assertEquals(source.getDetails(), target.getDetails());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testLog02() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Log source = new Log(12, DateTime.now(), Level.INFO, "test.alpha", "bla bla bla");

		//
		String xml = XMLActilog.toXMLString(source);

		logger.debug("xml=[" + xml + "]");

		Log target = XMLActilog.readLog(xml);

		//
		Assert.assertEquals(source.getId(), target.getId());
		Assert.assertEquals(source.getTime().toString(), target.getTime().toString());
		Assert.assertEquals(source.getLevel(), target.getLevel());
		Assert.assertEquals(source.getEvent(), target.getEvent());
		Assert.assertEquals(source.getDetails(), target.getDetails());

		//
		logger.debug("===== test done.");
	}

}
