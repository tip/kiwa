/**
 * Copyright 2013-2015 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.hico;

import java.io.UnsupportedEncodingException;
import java.util.Locale;

import javax.xml.stream.XMLStreamException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import fr.devinsy.util.xml.XMLBadFormatException;

/**
 * 
 * @author Christian P. Momon
 */
public class XMLHicoTest
{
	protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(XMLHicoTest.class);

	/**
	 * 
	 */
	@Before
	public void before()
	{
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(org.apache.log4j.Level.INFO);
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testArticle01() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Article source = new Article(12, "article1", Locale.FRENCH);
		source.setAuthor("testor");
		source.setVisible(true);
		source.setText("A text.<br/>aaaaaa<b>aaaaa</b>aaaaa");

		//
		String xml = XMLHico.toXMLString(source);

		logger.debug("xml=[" + xml + "]");

		//
		Article target = XMLHico.readArticle(xml);

		//
		Assert.assertEquals(source.getId(), target.getId());
		Assert.assertEquals(source.getAuthor(), target.getAuthor());
		Assert.assertEquals(source.getName(), target.getName());
		Assert.assertEquals(source.isVisible(), target.isVisible());
		Assert.assertEquals(source.getCreationDate().toString(), target.getCreationDate().toString());
		Assert.assertEquals(source.getEditionDate().toString(), target.getEditionDate().toString());
		Assert.assertEquals(source.getLocale().getLanguage(), target.getLocale().getLanguage());
		Assert.assertEquals(source.getText(), target.getText());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testArticle02() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Article source = new Article(12, "article1", Locale.FRENCH);
		source.setAuthor("testor");
		source.setVisible(true);
		source.setText("A text.<br/>aaaaaa<b>aaaaa</b>aaaaa");

		//
		String xml = XMLHico.toXMLString(source);

		logger.debug("xml=[" + xml + "]");

		//
		Article target = XMLHico.readArticle(xml);

		//
		Assert.assertEquals(source.getId(), target.getId());
		Assert.assertEquals(source.getAuthor(), target.getAuthor());
		Assert.assertEquals(source.getName(), target.getName());
		Assert.assertEquals(source.isVisible(), target.isVisible());
		Assert.assertEquals(source.getCreationDate().toString(), target.getCreationDate().toString());
		Assert.assertEquals(source.getEditionDate().toString(), target.getEditionDate().toString());
		Assert.assertEquals(source.getLocale().getLanguage(), target.getLocale().getLanguage());
		Assert.assertEquals(source.getText(), target.getText());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testHico01() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Hico source = new Hico();

		//
		String xml = XMLHico.toXMLString(source);

		logger.debug("xml=[" + xml + "]");

		//
		Hico target = XMLHico.readHico(xml);

		//
		Assert.assertEquals(source.lastArticleId(), target.lastArticleId());
		Assert.assertEquals(source.articles().size(), target.articles().size());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testHico02() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Hico source = new Hico();

		source.createArticle("article1", Locale.FRENCH);
		source.createArticle("article2", Locale.ENGLISH);
		source.createArticle("article3", Locale.FRENCH);

		//
		String xml = XMLHico.toXMLString(source);

		logger.debug("xml=[" + xml + "]");

		//
		Hico target = XMLHico.readHico(xml);

		//
		Assert.assertEquals(source.lastArticleId(), target.lastArticleId());
		Assert.assertEquals(source.articles().size(), target.articles().size());

		Assert.assertEquals(source.articles().getByIndex(0).getId(), target.articles().getByIndex(0).getId());
		Assert.assertEquals(source.articles().getByIndex(0).getAuthor(), target.articles().getByIndex(0).getAuthor());
		Assert.assertEquals(source.articles().getByIndex(0).isVisible(), target.articles().getByIndex(0).isVisible());
		Assert.assertEquals(source.articles().getByIndex(0).getName(), target.articles().getByIndex(0).getName());
		Assert.assertEquals(source.articles().getByIndex(0).getCreationDate().toString(), target.articles().getByIndex(0).getCreationDate().toString());
		Assert.assertEquals(source.articles().getByIndex(0).getEditionDate().toString(), target.articles().getByIndex(0).getEditionDate().toString());
		Assert.assertEquals(source.articles().getByIndex(0).getLocale().getLanguage(), target.articles().getByIndex(0).getLocale().getLanguage());
		Assert.assertEquals(source.articles().getByIndex(0).getText(), target.articles().getByIndex(0).getText());

		Assert.assertEquals(source.articles().getByIndex(1).getId(), target.articles().getByIndex(1).getId());
		Assert.assertEquals(source.articles().getByIndex(1).getAuthor(), target.articles().getByIndex(1).getAuthor());
		Assert.assertEquals(source.articles().getByIndex(1).isVisible(), target.articles().getByIndex(1).isVisible());
		Assert.assertEquals(source.articles().getByIndex(1).getName(), target.articles().getByIndex(1).getName());
		Assert.assertEquals(source.articles().getByIndex(1).getCreationDate().toString(), target.articles().getByIndex(1).getCreationDate().toString());
		Assert.assertEquals(source.articles().getByIndex(1).getEditionDate().toString(), target.articles().getByIndex(1).getEditionDate().toString());
		Assert.assertEquals(source.articles().getByIndex(1).getLocale().getLanguage(), target.articles().getByIndex(1).getLocale().getLanguage());
		Assert.assertEquals(source.articles().getByIndex(1).getText(), target.articles().getByIndex(1).getText());

		Assert.assertEquals(source.articles().getByIndex(2).getId(), target.articles().getByIndex(2).getId());
		Assert.assertEquals(source.articles().getByIndex(2).getAuthor(), target.articles().getByIndex(2).getAuthor());
		Assert.assertEquals(source.articles().getByIndex(2).isVisible(), target.articles().getByIndex(2).isVisible());
		Assert.assertEquals(source.articles().getByIndex(2).getName(), target.articles().getByIndex(2).getName());
		Assert.assertEquals(source.articles().getByIndex(2).getCreationDate().toString(), target.articles().getByIndex(2).getCreationDate().toString());
		Assert.assertEquals(source.articles().getByIndex(2).getEditionDate().toString(), target.articles().getByIndex(2).getEditionDate().toString());
		Assert.assertEquals(source.articles().getByIndex(2).getLocale().getLanguage(), target.articles().getByIndex(2).getLocale().getLanguage());
		Assert.assertEquals(source.articles().getByIndex(2).getText(), target.articles().getByIndex(2).getText());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testHico03() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Hico source = new Hico();

		Article article1 = source.createArticle("article1", Locale.FRENCH);
		article1.setAuthor("testor1");
		article1.setVisible(true);
		article1.setText("A text.<br/>aaqqqqaaaa<b>aaaaa</b>aaaaa");

		Article article2 = source.createArticle("article2", Locale.FRENCH);
		article2.setAuthor("testor2");
		article2.setVisible(true);
		article2.setText("A text.<br/>aaaaffffaa<b>aaaaa</b>aaaaa");

		Article article3 = source.createArticle("article3", Locale.FRENCH);
		article3.setAuthor("testor3");
		article3.setVisible(true);
		article3.setText("A text.<br/>aaaccccaa<b>aaaaa</b>aaaaa");
		//
		String xml = XMLHico.toXMLString(source);

		logger.debug("xml=[" + xml + "]");

		//
		Hico target = XMLHico.readHico(xml);

		//
		Assert.assertEquals(source.lastArticleId(), target.lastArticleId());
		Assert.assertEquals(source.articles().size(), target.articles().size());

		Assert.assertEquals(source.articles().getByIndex(0).getId(), target.articles().getByIndex(0).getId());
		Assert.assertEquals(source.articles().getByIndex(0).getAuthor(), target.articles().getByIndex(0).getAuthor());
		Assert.assertEquals(source.articles().getByIndex(0).isVisible(), target.articles().getByIndex(0).isVisible());
		Assert.assertEquals(source.articles().getByIndex(0).getName(), target.articles().getByIndex(0).getName());
		Assert.assertEquals(source.articles().getByIndex(0).getCreationDate().toString(), target.articles().getByIndex(0).getCreationDate().toString());
		Assert.assertEquals(source.articles().getByIndex(0).getEditionDate().toString(), target.articles().getByIndex(0).getEditionDate().toString());
		Assert.assertEquals(source.articles().getByIndex(0).getLocale().getLanguage(), target.articles().getByIndex(0).getLocale().getLanguage());
		Assert.assertEquals(source.articles().getByIndex(0).getText(), target.articles().getByIndex(0).getText());

		Assert.assertEquals(source.articles().getByIndex(1).getId(), target.articles().getByIndex(1).getId());
		Assert.assertEquals(source.articles().getByIndex(1).getAuthor(), target.articles().getByIndex(1).getAuthor());
		Assert.assertEquals(source.articles().getByIndex(1).isVisible(), target.articles().getByIndex(1).isVisible());
		Assert.assertEquals(source.articles().getByIndex(1).getName(), target.articles().getByIndex(1).getName());
		Assert.assertEquals(source.articles().getByIndex(1).getCreationDate().toString(), target.articles().getByIndex(1).getCreationDate().toString());
		Assert.assertEquals(source.articles().getByIndex(1).getEditionDate().toString(), target.articles().getByIndex(1).getEditionDate().toString());
		Assert.assertEquals(source.articles().getByIndex(1).getLocale().getLanguage(), target.articles().getByIndex(1).getLocale().getLanguage());
		Assert.assertEquals(source.articles().getByIndex(1).getText(), target.articles().getByIndex(1).getText());

		Assert.assertEquals(source.articles().getByIndex(2).getId(), target.articles().getByIndex(2).getId());
		Assert.assertEquals(source.articles().getByIndex(2).getAuthor(), target.articles().getByIndex(2).getAuthor());
		Assert.assertEquals(source.articles().getByIndex(2).isVisible(), target.articles().getByIndex(2).isVisible());
		Assert.assertEquals(source.articles().getByIndex(2).getName(), target.articles().getByIndex(2).getName());
		Assert.assertEquals(source.articles().getByIndex(2).getCreationDate().toString(), target.articles().getByIndex(2).getCreationDate().toString());
		Assert.assertEquals(source.articles().getByIndex(2).getEditionDate().toString(), target.articles().getByIndex(2).getEditionDate().toString());
		Assert.assertEquals(source.articles().getByIndex(2).getLocale().getLanguage(), target.articles().getByIndex(2).getLocale().getLanguage());
		Assert.assertEquals(source.articles().getByIndex(2).getText(), target.articles().getByIndex(2).getText());

		//
		logger.debug("===== test done.");
	}
}
