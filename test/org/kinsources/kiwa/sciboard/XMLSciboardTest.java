/**
 * Copyright 2013-2016 Christian Pierre MOMON, DEVINSY, UMR 7186 LESC.
 * 
 * christian.momon@devinsy.fr
 * 
 * This file is part of Kiwa. This software (Kiwa) is a computer program whose
 * purpose is to be the Kinsources Web Application, an open interactive platform
 * for archiving, sharing, analyzing and comparing kinship data used in
 * scientific inquiry.
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/ or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.kinsources.kiwa.sciboard;

import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.Properties;

import javax.xml.stream.XMLStreamException;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.kinsources.kiwa.accounts.Account;
import org.kinsources.kiwa.kidarep.Contributor;

import fr.devinsy.util.xml.XMLBadFormatException;
import fr.devinsy.util.xml.XMLReader;
import fr.devinsy.util.xml.XMLWriter;

/**
 * 
 * @author Christian P. Momon
 */
public class XMLSciboardTest
{
	protected static org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(XMLSciboardTest.class);

	/**
	 *  
	 */
	@Before
	public void before()
	{
		BasicConfigurator.configure();
		Logger.getRootLogger().setLevel(Level.INFO);
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testSciboard01() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Sciboard source = new Sciboard();

		//
		StringWriter xmlTarget = new StringWriter();
		XMLWriter out = new XMLWriter(xmlTarget);
		out.writeXMLHeader();
		XMLSciboard.write(out, source);
		String xml = xmlTarget.toString();

		logger.debug("xml=[" + xml + "]");

		//
		XMLReader xmlSource = new XMLReader(new StringReader(xml));
		xmlSource.readXMLHeader();
		Sciboard target = XMLSciboard.readSciboard(xmlSource);

		//
		Assert.assertEquals(0, source.countOfRequests());
		Assert.assertEquals(0, source.countOfComments());
		Assert.assertEquals(0, source.countOfVotes());

		//
		Assert.assertEquals(source.countOfRequests(), target.countOfRequests());
		Assert.assertEquals(source.countOfComments(), target.countOfComments());
		Assert.assertEquals(source.countOfVotes(), target.countOfVotes());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testSciboard02() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Sciboard source = new Sciboard();

		Properties properties10 = new Properties();
		properties10.setProperty("corpus_id", "11");
		source.createRequest(new Contributor(new Account(10, "10@foo.org", "FirstNameTen", "LastNameTen", "secret10")), "My motivation", Request.Type.DATASET_PUBLISH, properties10);

		Properties properties20 = new Properties();
		properties20.setProperty("corpus_id", "21");
		source.createRequest(new Contributor(new Account(20, "20@foo.org", "FirstNameTwenty", "LastNameTwenty", "secret20")), "My motivation", Request.Type.DATASET_PUBLISH, properties10);

		//
		StringWriter xmlTarget = new StringWriter();
		XMLWriter out = new XMLWriter(xmlTarget);
		out.writeXMLHeader();
		XMLSciboard.write(out, source);
		String xml = xmlTarget.toString();

		logger.debug("xml=[" + xml + "]");

		//
		XMLReader xmlSource = new XMLReader(new StringReader(xml));
		xmlSource.readXMLHeader();
		Sciboard target = XMLSciboard.readSciboard(xmlSource);

		//
		Assert.assertEquals(2, source.countOfRequests());
		Assert.assertEquals(0, source.countOfComments());
		Assert.assertEquals(0, source.countOfVotes());

		//
		Assert.assertEquals(source.countOfRequests(), target.countOfRequests());
		Assert.assertEquals(source.countOfComments(), target.countOfComments());
		Assert.assertEquals(source.countOfVotes(), target.countOfVotes());

		//
		//
		Request sourceRequest = source.requests().getByIndex(0);
		Request targetRequest = target.requests().getByIndex(0);

		Assert.assertEquals(sourceRequest.getId(), targetRequest.getId());
		Assert.assertEquals(sourceRequest.getContributorId(), targetRequest.getContributorId());
		Assert.assertEquals(sourceRequest.getContributorComment(), targetRequest.getContributorComment());
		Assert.assertEquals(sourceRequest.getCreationDate().toString(), targetRequest.getCreationDate().toString());
		Assert.assertEquals(sourceRequest.getDecisionMakerId(), targetRequest.getDecisionMakerId());
		Assert.assertEquals(sourceRequest.getDecisionComment(), targetRequest.getDecisionComment());
		Assert.assertEquals(sourceRequest.getDecisionDate(), targetRequest.getDecisionDate());
		Assert.assertEquals(sourceRequest.getStatus().toString(), targetRequest.getStatus().toString());
		Assert.assertEquals(sourceRequest.getType().toString(), targetRequest.getType().toString());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testSciboard03() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Sciboard source = new Sciboard();

		Properties properties10 = new Properties();
		properties10.setProperty("corpus_id", "11");
		Request request10 = source
				.createRequest(new Contributor(new Account(10, "10@foo.org", "FirstNameTen", "LastNameTen", "secret10")), "My motivation", Request.Type.DATASET_PUBLISH, properties10);

		Properties properties20 = new Properties();
		properties20.setProperty("corpus_id", "21");
		source.createRequest(new Contributor(new Account(20, "20@foo.org", "FirsNameTwenty", "LastNameTwenty", "secret20")), "My motivation", Request.Type.DATASET_PUBLISH, properties10);

		request10.setAccepted(100, "yeeeeeees");

		//
		StringWriter xmlTarget = new StringWriter();
		XMLWriter out = new XMLWriter(xmlTarget);
		out.writeXMLHeader();
		XMLSciboard.write(out, source);
		String xml = xmlTarget.toString();

		logger.debug("xml=[" + xml + "]");

		//
		XMLReader xmlSource = new XMLReader(new StringReader(xml));
		xmlSource.readXMLHeader();
		Sciboard target = XMLSciboard.readSciboard(xmlSource);

		//
		Assert.assertEquals(2, source.countOfRequests());
		Assert.assertEquals(0, source.countOfComments());
		Assert.assertEquals(0, source.countOfVotes());

		//
		Assert.assertEquals(source.countOfRequests(), target.countOfRequests());
		Assert.assertEquals(source.countOfComments(), target.countOfComments());
		Assert.assertEquals(source.countOfVotes(), target.countOfVotes());

		//
		//
		Request sourceRequest = source.requests().getByIndex(0);
		Request targetRequest = target.requests().getByIndex(0);

		Assert.assertEquals(sourceRequest.getId(), targetRequest.getId());
		Assert.assertEquals(sourceRequest.getContributorId(), targetRequest.getContributorId());
		Assert.assertEquals(sourceRequest.getContributorComment(), targetRequest.getContributorComment());
		Assert.assertEquals(sourceRequest.getCreationDate().toString(), targetRequest.getCreationDate().toString());
		Assert.assertEquals(sourceRequest.getDecisionMakerId(), targetRequest.getDecisionMakerId());
		Assert.assertEquals(sourceRequest.getDecisionComment(), targetRequest.getDecisionComment());
		Assert.assertEquals(sourceRequest.getDecisionDate().toString(), targetRequest.getDecisionDate().toString());
		Assert.assertEquals(sourceRequest.getStatus().toString(), targetRequest.getStatus().toString());
		Assert.assertEquals(sourceRequest.getType().toString(), targetRequest.getType().toString());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testSciboard04() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Sciboard source = new Sciboard();

		Properties properties10 = new Properties();
		properties10.setProperty("corpus_id", "11");
		Request request10 = source.createRequest(new Contributor(new Account(10, "10@foo.org", "FirstNameTen", "Ten", "secret10")), "My motivation", Request.Type.DATASET_PUBLISH, properties10);
		source.addComment(request10, new Account(40, "40@foo.org", "FirstNameFourty", "Fourty", "secret40"), "my comment 40");
		source.addComment(request10, new Account(50, "50@foo.org", "FirstNameFivety", "Fivety", "secret50"), "my comment 50");
		source.addComment(request10, new Account(40, "40@foo.org", "FirstNameFourty", "Fourty", "secret40"), "my comment 40b");

		Properties properties20 = new Properties();
		properties20.setProperty("corpus_id", "21");
		source.createRequest(new Contributor(new Account(20, "20@foo.org", "FirstNameTwenty", "Twenty", "secret20")), "My motivation", Request.Type.DATASET_PUBLISH, properties10);

		//
		StringWriter xmlTarget = new StringWriter();
		XMLWriter out = new XMLWriter(xmlTarget);
		out.writeXMLHeader();
		XMLSciboard.write(out, source);
		String xml = xmlTarget.toString();

		logger.debug("xml=[" + xml + "]");

		//
		XMLReader xmlSource = new XMLReader(new StringReader(xml));
		xmlSource.readXMLHeader();
		Sciboard target = XMLSciboard.readSciboard(xmlSource);

		//
		Assert.assertEquals(2, source.countOfRequests());
		Assert.assertEquals(3, source.countOfComments());
		Assert.assertEquals(0, source.countOfVotes());

		//
		Assert.assertEquals(source.countOfRequests(), target.countOfRequests());
		Assert.assertEquals(source.countOfComments(), target.countOfComments());
		Assert.assertEquals(source.countOfVotes(), target.countOfVotes());

		//
		Comment sourceComment = source.requests().getByIndex(0).comments().getByIndex(1);
		Comment targetComment = target.requests().getByIndex(0).comments().getByIndex(1);

		Assert.assertEquals(sourceComment.getId(), targetComment.getId());
		Assert.assertEquals(sourceComment.getText(), targetComment.getText());
		Assert.assertEquals(sourceComment.getAuthorId(), targetComment.getAuthorId());
		Assert.assertEquals(sourceComment.getCreationDate().toString(), targetComment.getCreationDate().toString());
		Assert.assertEquals(sourceComment.getEditionDate().toString(), targetComment.getEditionDate().toString());

		//
		logger.debug("===== test done.");
	}

	/**
	 * @throws UnsupportedEncodingException
	 * @throws XMLBadFormatException
	 * @throws XMLStreamException
	 * 
	 */
	@Test
	public void testSciboard05() throws UnsupportedEncodingException, XMLStreamException, XMLBadFormatException
	{
		//
		logger.debug("===== test starting...");

		//
		Sciboard source = new Sciboard();

		Properties properties10 = new Properties();
		properties10.setProperty("corpus_id", "11");
		Request request10 = source.createRequest(new Contributor(new Account(10, "10@foo.org", "FirstNameTen", "Ten", "secret10")), "My motivation", Request.Type.DATASET_PUBLISH, properties10);
		source.addVote(request10, new Account(40, "40@foo.org", "FirstNameFourty", "Fourty", "secret40"), Vote.Value.AGREE);
		source.addVote(request10, new Account(50, "50@foo.org", "FirstNameFourty", "Fourty", "secret50"), Vote.Value.DUBIOUS);
		source.addVote(request10, new Account(40, "40@foo.org", "FirstNameFourty", "Fourty", "secret40"), Vote.Value.DISAGREE);

		Properties properties20 = new Properties();
		properties20.setProperty("corpus_id", "21");
		Request request20 = source.createRequest(new Contributor(new Account(20, "20@foo.org", "FirstNameTwenty", "Twenty", "secret20")), "My motivation", Request.Type.DATASET_PUBLISH, properties10);
		source.addVote(request20, new Account(240, "240@foo.org", "FirstNameFourty2", "Fourty2", "secret402"), Vote.Value.AGREE);
		source.addVote(request20, new Account(250, "250@foo.org", "FirstNameFourty2", "Fourty2", "secret502"), Vote.Value.DUBIOUS);
		source.addVote(request20, new Account(240, "240@foo.org", "FirstNameFourty2", "Fourty2", "secret402"), Vote.Value.DISAGREE);

		//
		StringWriter xmlTarget = new StringWriter();
		XMLWriter out = new XMLWriter(xmlTarget);
		out.writeXMLHeader();
		XMLSciboard.write(out, source);
		String xml = xmlTarget.toString();

		logger.debug("xml=[" + xml + "]");

		//
		XMLReader xmlSource = new XMLReader(new StringReader(xml));
		xmlSource.readXMLHeader();
		Sciboard target = XMLSciboard.readSciboard(xmlSource);

		//
		Assert.assertEquals(2, source.countOfRequests());
		Assert.assertEquals(0, source.countOfComments());
		Assert.assertEquals(4, source.countOfVotes());

		//
		Assert.assertEquals(source.countOfRequests(), target.countOfRequests());
		Assert.assertEquals(source.countOfComments(), target.countOfComments());
		Assert.assertEquals(source.countOfVotes(), target.countOfVotes());

		//
		Vote sourceVeto = source.requests().getByIndex(0).votes().getByOwnerId(40);
		Vote targetVeto = target.requests().getByIndex(0).votes().getByOwnerId(40);

		Assert.assertEquals(sourceVeto.getValue().toString(), targetVeto.getValue().toString());
		Assert.assertEquals(sourceVeto.getOwnerId(), targetVeto.getOwnerId());
		Assert.assertEquals(sourceVeto.getCreationDate().toString(), targetVeto.getCreationDate().toString());
		Assert.assertEquals(sourceVeto.getEditionDate().toString(), targetVeto.getEditionDate().toString());

		//
		logger.debug("===== test done.");
	}
}
